# Pathfinder

This is the code of the paper "PATHFINDER: Storage and Indexing of Massive Trajectory Sets" by Stefan Funke, André Nusser, Tobias Rupp, and Sabine Storandt. The paper will be presented at [SSTD 2019](http://sstd2019.org/) in Vienna.

It is a project to build a fast and flexible data structure for the storage and retrieval of paths in a road network graph. The core idea is that the paths are compressed using a Contraction Hierarchy (CH) and then retrieved using the R-tree like structure of the CH.

## How to build

### Build Commands

To *build the easy way*, enter the `code` directory and execute:

```
./build.sh
```

To *manually build*, enter the `code` directory and execute the following commands:

```
git submodule init
git submodule update
mkdir build
cd build
cmake ..
make -j
```

### Dependencies

 * A compiler supporting C++11
 * CMake
 * stxxl (as submodule)

### Build Options

To change build options you can either edit the `build/CMakeCache.txt` file directly or use a GUI interface like `ccmake`.

There are three main options that you might want to change in the build process:

##### Verbosity

The `VERBOSE` variable is a bool and can thus be set to `ON` or `OFF`. If `VERBOSE` is set to `ON`, the output of the Print macro is shown.

##### Build Type

There are four different build types: Debug, Release, RelWithDebInfo, and MinSizeRel. You can use them by setting the `CMAKE_BUILD_TYPE` variable. If `CMAKE_BUILD_TYPE` is set to `ON`, the output of the Debug macro is shown and the debug asserts are checked.

##### Other Build an Link Flags

Additional flags are best set by using the variables with the EXTRA prefix.

## How to use

### Tests

There are some tests to check the basic functionality. When building, a `run_tests` binary is created which executes the tests. As randomized testing is used, a seed is created for each test run which is printed at the beginning of the output. To make tests reproducible, you can also set the seed of the tests by providing it as an argument to `run_tests`.

### Example Binary and scripts

An example usage of the pathfinder algorithm is shown in the `src/main.cpp` file.
It is also used to build the `pathfinder` executable.
There are also some scripts which demonstrate the various use cases of pathfinder.
The easiest way to start is by executing the script 'parseAndFindPaths.sh'.
In the following the usage of the `pathfinder` executable is explained.

##### Parameters

The executable can be called in the following ways:

```
./pathfinder -i <graph_filename> -e <export_filename> - p <paths_filename>
./pathfinder -i <graph_filename> -e <export_filename> - p <paths_filename> -r <min_coordinate>;<max_coordinate>
```

where `<min_coordinate>` and `<max_coordinate>` give the
coordinates of the query bounding box and have to be
of the form:

`<lat>,<lon>` (e.g., `64.244740,17.385652`)

The first way to call `pathfinder` asks for CLI input of the query box coordinates and does so repeatedly until one enters "quit". This is the best way to interactively test the pathfinder algorithm. After every query you can choose to export the result of this query to the filename provided via the `export_filename` CLI parameter.
Pathfinder can also answer temporal queries. To obtain more information about its usage, call:

```
./pathfinder --help
```

##### Graph format

The graph file has to be in the following form:

```
# <optional comment>
.
.
# <optional comment>

<number_of_nodes>
<number_of_edges>
<first_node>
.
.
<last_node>
<first_edge>
.
.
<last_edge>
```

Note the empty line between the comments and the `<number_of_nodes>` entry.

The node entries are of the form:
```
<node id> <osm id> <latitude> <longitude> <height> <contraction_hierarchy_level>
```

The edge entries are of the form:
```
<source_node_id> <target_node_id> <edge_length> <edge_type> <edge_speed> <child_edge_id_1> <child_edge_id_2>
```

For an example, see the `data/test.graph` file. Those file are best created using the [OsmGraphCreator](https://github.com/fmi-alg/OsmGraphCreator) to export the graph from an OSM pbf file and the [ch_constructor](https://github.com/chaot4/ch_constructor) to create the contraction hierarchy. 

##### Paths format

The paths file contains the matched traces. It is best created by using the [MapMatcher](https://theosvn.informatik.uni-stuttgart.de/SVN/ALGORITHMIK/PerMa). You can also download data of germany from [here](http://www.fmi.uni-stuttgart.de/files/alg/data/SPP/trajectory_data.tar.gz).

##### Output format

The output format is the input format of the [simplestGraphRendering](https://github.com/invor/simplestGraphRendering) viewer. So I highly recommend using it for viewing the output.

### Performance Test

The `performance_test` binary was created to measure the speed of the pathfinder algorithm over several random queries. It is called via

```
./performance_test -i <graph_filename> -g <number_of_generated_paths> -q <number_of_queries>
```

where the graph format is the same as described earlier. The parameter `number_of_generated_paths` gives the number of random paths that are created and inserted into the query data structure. The parameter `number_of_queries` states how many queries per measurement are executed.
There are also options which allow you to export and import generated paths and queries to repeat experiments.
Moreover, you can also supply real-world paths as input.
Finally you can also test simple baseline algorithms and specifiy the number of threads used.
You get information about the usage of these options by calling

```
./performance_test --help
```


### Compressor

The `compressor` binary was created to use the partition functionality to compress trajectories. It is called via

```
./compressor -c -i <graph_filename> -p <paths_filename> -e <export_filename>
```
By adding the paramter -b you can get a more dense binary output.

## Nice picture

Here you can see the result of an example query. The result is shown using the [simplestGraphRendering](https://github.com/invor/simplestGraphRendering) viewer.

![There should be a screenshot here.](data/screenshot.png)

## License

The source code is licensed under the MIT license (see `code/LICENSE`). The test data we provide in the data directory is based on OpenStreeMap data and is therefore licensed under CC-BY-SA (see `data/LICENSE`).
