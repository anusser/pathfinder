gitDir=~/git/

export GLIBCXX_FORCE_NEW

nof_queries=100
nof_gen_paths=1000000
graph="../data/saarland-180101-time-all-ch.ftxt"




out="saarland_foundPaths.sg"
valgrind --tool=massif --log-file="logfile_massif.txt" ./build/performance_test -i $graph -a  $nof_gen_paths -w $nof_queries
# --leak-check=full --show-leak-kinds=all
#--track-origins=yes
