resultname=$1
nof_paths=10_5
script=benchmark_import_generated_data.sh
time=/usr/bin/time

path=stxxl-log/$nof_paths
time_path=$path/time
space_path=$path/space

mkdir -p $time_path
mkdir -p $space_path

eval $time -v ./$script 1>$time_path/$resultname 2>$space_path/$resultname
