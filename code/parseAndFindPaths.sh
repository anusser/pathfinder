executable="build/pathfinder"
dataDir="../data"
graphIn=$dataDir"/saarland-180101-maxspeed-distance-dm-all-ch.ftxt"
pathsIn=$dataDir"/paths.0_2147483647.json"
saarlandBB="49.347612,6.661348:49.461421,6.762571"
saarlandTI="946684800,1546300800" #2000-2018
saarlandTS="1111111111111111111111111111111100000000000000000000000000000000" #first half of the week
out="saarland_parsed_found_paths.sg"

eval $executable -i $graphIn -e $out -p $pathsIn -r $saarlandBB -t $saarlandTI -s $saarlandTS