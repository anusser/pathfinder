#include "exporter.h"

#include "defs.h"
#include "paths_ds.h"

#include <fstream>

namespace pf
{

Exporter::Exporter(Graph const& graph) : graph(graph), geometry(graph)
{
}

void Exporter::exportSimplestGraphRendererFile(ExportData const& data) const
{
	auto node_map = createNodeMap(data);

	std::ofstream f(data.filename);
	if (!f.is_open()) {
		throw Exception("The file " + data.filename + " could not be opened.");
	}

	writeSimplestHeader(node_map, data, f);
	writeSimplestNodes(node_map, f);
	writeSimplestEdges(node_map, data, f);
}

auto Exporter::createNodeMap(ExportData const& data) const -> NodeMap
{
	NodeMap node_map;

	// Insert coordinates of boxes
	for (auto const& box : data.boxes) {
		auto vertices = geometry.getVertices(box);
		for (auto const& vertex : vertices) {
			node_map.emplace(vertex.coordinate, c::NO_NID);
		}
	}

	// Insert coordinates of paths
	for (PathID path_id = 0; path_id < data.paths_ds.getNrOfPaths();
	     path_id++) {
		Path path = data.paths_ds.getPath(path_id);
		for (auto node_id : path.getNodeRange(graph)) {
			auto const& node = graph.getNode(node_id);
			node_map.emplace(node.coordinate, c::NO_NID);
		}
	}

	return node_map;
}

void Exporter::writeSimplestHeader(
    NodeMap const& node_map, ExportData const& data, std::ofstream& f) const
{
	auto number_of_nodes = node_map.size();
	auto number_of_edges = 4 * data.boxes.size();
	for (PathID path_id = 0; path_id < data.paths_ds.getNrOfPaths();
	     path_id++) {
		number_of_edges += data.paths_ds.getPathLength(path_id);
	}
	auto number_of_triangles = 0;

	f << number_of_nodes << "\n";
	f << number_of_edges << "\n";
	f << number_of_triangles << "\n";
}

void Exporter::writeSimplestNodes(NodeMap& node_map, std::ofstream& f) const
{
	// Assign IDs and write nodes at the same time
	ExportNodeID current_id = 0;
	for (auto& pair : node_map) {
		pair.second = current_id;
		++current_id;

		// "0 0 0 255" assigns black color to the node
		auto const& coordinate = pair.first;
		f << coordinate.lat << " " << coordinate.lon << " 0 0 0 255\n";
	}
}

void Exporter::writeSimplestEdges(
    NodeMap const& node_map, ExportData const& data, std::ofstream& f) const
{
	// Write edges of boxes
	assert(data.boxes.size() == data.box_colors.size());
	for (std::size_t i = 0; i < data.boxes.size(); ++i) {
		auto const& box = data.boxes[i];
		auto const& color = data.box_colors[i];

		auto segments = geometry.getSegments(box);
		for (auto const& segment : segments) {
			auto it1 = node_map.find(segment.vertex1.coordinate);
			auto it2 = node_map.find(segment.vertex2.coordinate);
			assert(it1 != node_map.end());
			assert(it2 != node_map.end());

			f << it1->second << " " << it2->second << " " << color << "\n";
		}
	}

	// Write edges of paths
	assert(data.paths_ds.getNrOfPaths() == (PathID)data.path_colors.size());
	for (PathID i = 0; i < data.paths_ds.getNrOfPaths(); ++i) {
		Path const path = data.paths_ds.getPath(i);
		auto const& color = data.path_colors[i];

		for (EdgeID edge_id : path.getEdges()) {
			auto const& edge = graph.getEdge(edge_id);
			auto const& coordinate1 = graph.getNode(edge.source).coordinate;
			auto const& coordinate2 = graph.getNode(edge.target).coordinate;

			auto it1 = node_map.find(coordinate1);
			auto it2 = node_map.find(coordinate2);
			assert(it1 != node_map.end());
			assert(it2 != node_map.end());

			f << it1->second << " " << it2->second << " " << color << "\n";
		}
	}
}

std::size_t Exporter::CoordinateHash::operator()(
    Coordinate const& coordinate) const
{
	auto lat_hash = std::hash<Coordinate::FloatType>{}(coordinate.lat);
	auto lon_hash = std::hash<Coordinate::FloatType>{}(coordinate.lon);
	return lat_hash + lon_hash;
}

} // namespace pf
