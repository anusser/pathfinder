#pragma once

#include <cstdint>
#include <vector>

namespace pf
{

using VertexID = int64_t;

namespace c
{
const VertexID NO_VID = -1;
} // namespace c

struct Vertex {
	Coordinate coordinate;
	VertexID id;

	// all-invalid constructor
	Vertex() : id(c::NO_VID)
	{
	}

	Vertex(Coordinate coordinate, VertexID id = c::NO_VID)
	    : coordinate(coordinate), id(id)
	{
	}

	Coordinate::FloatType lat() const
	{
		return coordinate.lat;
	}

	Coordinate::FloatType lon() const
	{
		return coordinate.lon;
	}
};
using Vertices = std::vector<Vertex>;

struct Segment {
	Vertex vertex1;
	Vertex vertex2;

	// all-invalid constructor
	Segment()
	{
	}

	Segment(Vertex const& vertex1, Vertex const& vertex2)
	    : vertex1(vertex1), vertex2(vertex2)
	{
	}
};
using Segments = std::vector<Segment>;

enum class Orientation { Above, On, Below };
using Orientations = std::vector<Orientation>;

} // namespace pf
