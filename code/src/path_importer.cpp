#include "path_importer.h"

#include "defs.h"
#include "time_tracker.h"

#include <fstream>

namespace pf
{

PathImporter::PathImporter(Graph const& graph, PathsDS& paths_ds)
    : graph(graph), paths_ds(paths_ds)
{
}

bool PathImporter::importPathsForPerformanceTest(
    const std::string graph_filename, uint number_of_paths, uint distance)
{
	std::string directory_name = graph_filename + "_resorted_" +
	                             std::to_string(number_of_paths) + "_paths";
	std::string file_name = directory_name + "/" + std::to_string(distance);
	return importPaths(file_name);
}

bool PathImporter::importPaths(std::string file_name)
{
	assert(!already_parsed);
	already_parsed = true;

	TimeTracker time_tracker;

	Print("Import paths ...");
	time_tracker.start();

	bool success = true;
	std::ifstream f(file_name);

	if (f.is_open()) {
		try {
			auto number_of_paths = readHeader(f);
			readPaths(f, number_of_paths);
		} catch (Exception e) {
			throw e;
			success = false;
		}

		f.close();
	} else {
		success = false;
	}

	time_tracker.stop();
	Print("Took " << time_tracker.getLastMeasurment() << " seconds");

	return success;
}

std::size_t PathImporter::readHeader(std::ifstream& f)
{
	std::string line;
	std::getline(f, line);
	uint number_of_paths = std::stoi(line);

	std::getline(f, line); // eat empty line
	return number_of_paths;
}

void PathImporter::readPaths(std::ifstream& f, std::size_t number_of_paths)
{
	for (std::size_t i = 0; i < number_of_paths; ++i) {
		Path path;
		TimeType minTime;
		TimeType maxTime;
		f >> minTime;
		f.ignore();

		while (f.peek() != '\n') {

			path.push(graph, *std::istream_iterator<EdgeID>(f));
			f.ignore();

			f >> maxTime;
			TimeIntervall time_intervall(minTime, maxTime);
			path.setTimeIntervallForEdge(
			    path.getEdges().size() - 1, time_intervall);
			f.ignore();
			// for next Edge
			minTime = maxTime;
		}
		f.ignore();

		paths_ds.addPath(path);
	}
}

bool PathImporter::importPathsBin(std::string file_name)
{
	assert(!already_parsed);
	already_parsed = true;

	TimeTracker time_tracker;

	Print("Import paths ...");
	time_tracker.start();

	bool success = true;
	std::ifstream f;
	f.open(file_name, std::ios::in | std::ios::binary);

	if (f.is_open()) {
		try {
			auto number_of_paths = readHeaderBin(f);
			readPathsBin(f, number_of_paths);
		} catch (Exception e) {
			throw e;
			success = false;
		}

		f.close();
	} else {
		success = false;
	}

	time_tracker.stop();
	Print("Took " << time_tracker.getLastMeasurment() << " seconds");

	return success;
}

std::size_t PathImporter::readHeaderBin(std::ifstream& f)
{
	long number_of_paths;
	f.read((char*)&number_of_paths, sizeof(long));
	Print(number_of_paths);
	return number_of_paths;
}

void PathImporter::readPathsBin(std::ifstream& f, long number_of_paths)
{
	for (long i = 0; i < number_of_paths; ++i) {
		Path path;
		TimeType minTime;
		TimeType maxTime;

		long path_size;
		f.read((char*)&path_size, sizeof(long));
		f.read((char*)&minTime, sizeof(TimeType));

		for (long edge_index = 0; edge_index < path_size; edge_index++) {
			EdgeID edge_id;
			f.read((char*)&edge_id, sizeof(EdgeID));
			path.push(graph, edge_id);

			f.read((char*)&maxTime, sizeof(TimeType));
			TimeIntervall time_intervall(minTime, maxTime);
			path.setTimeIntervallForEdge(
			    path.getEdges().size() - 1, time_intervall);

			minTime = maxTime;
		}
		paths_ds.addPath(path);
	}
}

} // namespace pf
