#include "geometry.h"

#include "defs.h"
#include <cmath>
#include <math.h>

namespace pf
{

Geometry::Geometry(Graph const& graph) : graph(graph)
{
}

Vertex Geometry::getVertex(NodeID node_id) const
{
	auto const& node = graph.getNode(node_id);
	return Vertex(node.coordinate);
}

Segment Geometry::getSegment(EdgeID edge_id) const
{
	auto const& edge = graph.getEdge(edge_id);
	auto const& source = graph.getNode(edge.source);
	auto const& target = graph.getNode(edge.target);

	Vertex vertex1(source.coordinate);
	Vertex vertex2(target.coordinate);

	return Segment(vertex1, vertex2);
}

bool Geometry::nodeInBox(Vertex const& vertex, BoundingBox const& box) const
{
	if (vertex.lon() >= box.min_coordinate.lon &&
	    vertex.lat() >= box.min_coordinate.lat &&
	    vertex.lon() <= box.max_coordinate.lon &&
	    vertex.lat() <= box.max_coordinate.lat) {

		return true;
	}

	return false;
}

Orientation Geometry::calcOrientation(
    Segment const& segment, Vertex const& vertex) const
{
	// is vertex one of the endpoints of segment?
	if (vertex.coordinate == segment.vertex1.coordinate ||
	    vertex.coordinate == segment.vertex2.coordinate) {

		return Orientation::On;
	}

	// calculate line parameters
	auto lat_diff = segment.vertex1.lat() - segment.vertex2.lat();
	auto lon_diff = segment.vertex1.lon() - segment.vertex2.lon();
	auto m = lat_diff / lon_diff;
	auto b = segment.vertex1.lat() - m * segment.vertex1.lon();

	// check for orientation
	auto lat_line = m * vertex.lon() + b;
	if (vertex.lat() > lat_line) {
		return Orientation::Above;
	} else if (vertex.lat() < lat_line) {
		return Orientation::Below;
	} else {
		return Orientation::On;
	}
}

Vertices Geometry::getVertices(BoundingBox const& box) const
{
	Vertices vertices;

	vertices.emplace_back(box.min_coordinate, 0);
	vertices.emplace_back(
	    Coordinate(box.min_coordinate.lat, box.max_coordinate.lon), 1);
	vertices.emplace_back(box.max_coordinate, 2);
	vertices.emplace_back(
	    Coordinate(box.max_coordinate.lat, box.min_coordinate.lon), 3);

	return vertices;
}

Segments Geometry::getSegments(BoundingBox const& box) const
{
	auto const& vertices = getVertices(box);
	assert(vertices.size() == 4);

	Segments segments;
	for (std::size_t i = 0; i < 4; ++i) {
		segments.emplace_back(vertices[i], vertices[(i + 1) % 4]);
	}

	return segments;
}

bool Geometry::intersect(BoundingBox const& query_box, EdgeID edge_id) const
{
	auto const& edge = graph.getEdge(edge_id);

	auto const source_vertex = getVertex(edge.source);
	auto const target_vertex = getVertex(edge.target);

	// Check if source or target are inside the query_box
	if (nodeInBox(source_vertex, query_box) ||
	    nodeInBox(target_vertex, query_box)) {

		return true;
	}

	auto const segment = getSegment(edge.id);
	auto const box_vertices = getVertices(query_box);
	auto const box_segments = getSegments(query_box);

	// Calculate orientation of the bounding box wrt the segment
	Orientations orientations;
	for (auto const& vertex : box_vertices) {
		auto orientation = calcOrientation(segment, vertex);
		orientations.push_back(orientation);
	}

	// Check for segments where the endpoints have different orientation if
	// they intersect with one side of the rectangle. To understand what is
	// happening here it's best to draw an example. :)
	for (auto box_segment : box_segments) {
		auto const& vertex1 = box_segment.vertex1;
		auto const& vertex2 = box_segment.vertex2;

		if (orientations[vertex1.id] != orientations[vertex2.id]) {
			// horizontal
			if (vertex1.lat() == vertex2.lat()) {
				auto lat = vertex1.lat();
				if ((source_vertex.lat() - lat) * (target_vertex.lat() - lat) <
				    0) {
					return true;
				}
			}
			// vertical
			else if (vertex1.lon() == vertex2.lon()) {
				auto lon = vertex1.lon();
				if ((source_vertex.lon() - lon) * (target_vertex.lon() - lon) <
				    0) {
					return true;
				}
			}
			// something is wrong...
			else {
				throw Exception(
				    "A box segment has to be horizontal or vertical.");
			}
		}
	}

	return false;
}

bool Geometry::intersect(BoundingBox const& query_box, Path const& path) const
{
	for (EdgeID edge_id : path.getEdges()) {
		Path edge_path = graph.unpack(edge_id);

		for (EdgeID base_edge_id : edge_path.getEdges()) {
			if (intersect(query_box, base_edge_id)) {
				return true;
			}
		}
	}

	return false;
}

double Geometry::square(double x) const
{
	return x * x;
}

double Geometry::pythagoras(double a, double b) const
{
	return sqrt(square(a) + square(b));
}

double Geometry::calcGeoDist(
    double lon1, double lat1, double lon2, double lat2) const
{
	double deltaLon = lon1 - lon2;
	double deltaLat = lat1 - lat2;

	// based on equirectangular projection
	double avgLat = (lat1 + lat2) / 2;

	double x = (deltaLon)*std::cos(avgLat * M_PI / 180.0);
	double y = (deltaLat);
	return pythagoras(x, y) * R * (M_PI / 180.0);
}

double Geometry::calcGeoDist(
    Coordinate coordinate1, Coordinate coordinate2) const
{
	return calcGeoDist(
	    coordinate1.lon, coordinate1.lat, coordinate2.lon, coordinate2.lat);
}

} // namespace pf
