#pragma once

#include "basic_types.h"
#include "defs.h"
#include "flags.h"
#include "graph.h"

#include <queue>

namespace pf
{

namespace unit_tests
{
void testCHTraversal();
} // namespace unit_tests

class CHTraversal
{
public:
	CHTraversal(Graph const& graph);

	enum class Result { Expand, Ignore, StopSearch };

	enum class ExitValue { Stopped, NotStopped };

	// The Function must be callable with arguments:
	// (Edge const& edge, Direction direction)
	template <typename Function>
	void up(Function&& function) const;

	// The Function must be callable with arguments:
	// (Edge const& edge, Direction direction)
	// and also return a value of type Result.
	template <typename Function>
	ExitValue up(
	    Function&& function, NodeID start_node_id, bool reset_seen = true);

	// Same as member function up just starting with multiple NodeIDs
	template <typename Function>
	ExitValue up(Function&& function, NodeIDs const& start_node_ids,
	    bool reset_seen = true);

	// The Function must be callable with arguments:
	// (Edge const& edge, Direction direction)
	template <typename Function>
	void down(Function&& function) const;

	// The Function must be callable with arguments:
	// (Edge const& edge, Direction direction)
	// and also return a value of type Result.
	// NOTE: Do not call this function recursively or in a parallelized
	// implementation.
	// Due to the static member this will trigger issues!
	template <typename Function>
	ExitValue down(
	    Function&& function, NodeID start_node_id, bool reset_seen = true);

	// Same as member function down just starting with multiple NodeIDs
	// NOTE: Do not call this function recursively or in a parallelized
	// implementation.
	// Due to the static member this will trigger issues!
	template <typename Function>
	ExitValue down(Function&& function, NodeIDs const& start_node_ids,
	    bool reset_seen = true);

	// The Function must be callable with arguments:
	// (Edge const& edge, Direction direction)
	// and also return a value of type Result.
	// NOTE: Do not call this function recursively or in a parallelized
	// implementation.
	// Due to the static member this will trigger issues!
	// This version of the down variant will ignore obsolete edges
	template <typename Function>
	ExitValue downWithoutObsolete(
	    Function&& function, NodeID start_node_id, bool reset_seen = true);

	// Same as member function down just starting with multiple NodeIDs
	// NOTE: Do not call this function recursively or in a parallelized
	// implementation.
	// Due to the static member this will trigger issues!
	// This version of the down variant will ignore obsolete edges
	template <typename Function>
	ExitValue downWithoutObsolete(Function&& function,
	    NodeIDs const& start_node_ids, bool reset_seen = true);

	// The Function must be callable with arguments:
	// (EdgeID parent_id, EdgeID edge_id)
	template <typename Function>
	void toParents(Function&& function) const;

	// The Function must be callable with arguments:
	// (EdgeID parent_id, EdgeID edge_id)
	// and also return a value of type Result.
	template <typename Function>
	ExitValue toParents(Function&& function, EdgeID start_edge_id) const;

	// Same as member function toParents just starting with multiple EdgeIDs
	template <typename Function>
	ExitValue toParents(
	    Function&& function, EdgeIDs const& start_edge_ids) const;

	// The Function must be callable with arguments:
	// (EdgeID child_id, EdgeID edge_id)
	template <typename Function>
	void toChildren(Function&& function) const;

	// The Function must be callable with arguments:
	// (EdgeID child_id, EdgeID edge_id)
	// and also return a value of type Result.
	template <typename Function>
	ExitValue toChildren(Function&& function, EdgeID start_edge_id) const;

	// Same as member function toChildren just starting with multiple EdgeIDs
	template <typename Function>
	ExitValue toChildren(
	    Function&& function, EdgeIDs const& start_edge_ids) const;

	// Resets the seen vector. This is helpful when using the reset_seen flag
	// in the up or down member function.
	void resetSeen();

	// Just returns the value of the seen vector for node_id
	bool wasSeen(NodeID node_id) const;

private:
	Graph const& graph;
	Flags<bool> seen;

	NodeIDs node_contraction_order;
	EdgeIDs edge_creation_order;

	friend void unit_tests::testCHTraversal();
};

template <typename Function>
void CHTraversal::up(Function&& function) const
{
	for (auto node_id : node_contraction_order) {
		for (auto direction : {Direction::Forward, Direction::Backward}) {
			for (auto const& edge : graph.getUpEdgesOf(node_id, direction)) {
				std::forward<Function>(function)(edge, direction);
			}
		}
	}
}

template <typename Function>
auto CHTraversal::up(Function&& function, NodeID start_node_id, bool reset_seen)
    -> ExitValue
{
	auto start_node_ids = {start_node_id};
	return up(std::forward<Function>(function), start_node_ids, reset_seen);
}

template <typename Function>
auto CHTraversal::up(Function&& function, NodeIDs const& start_node_ids,
    bool reset_seen) -> ExitValue
{
	using PQElement = std::pair<decltype(Node::level), NodeID>;
	using PQ = std::priority_queue<PQElement, std::vector<PQElement>,
	    std::greater<PQElement>>;

	PQ pq;
	if (reset_seen) {
		seen.reset();
	}

	for (auto start_node_id : start_node_ids) {
		auto start_node_level = graph.getNode(start_node_id).level;
		pq.emplace(start_node_level, start_node_id);
		seen.set(start_node_id, true);
	}

	while (!pq.empty()) {
		auto node_id = pq.top().second;
		pq.pop();

		for (auto direction : {Direction::Forward, Direction::Backward}) {
			for (auto const& edge : graph.getUpEdgesOf(node_id, direction)) {
				auto head_id = edge.getHead(direction);

				auto result = std::forward<Function>(function)(edge, direction);

				if (result == Result::StopSearch) {
					return ExitValue::Stopped;
				}

				if (result == Result::Expand && !seen[head_id]) {
					auto const& head = graph.getNode(head_id);
					pq.emplace(head.level, head.id);
					seen.set(head_id, true);
				}
			}
		}
	}

	return ExitValue::NotStopped;
}

template <typename Function>
void CHTraversal::down(Function&& function) const
{
	auto rbegin = node_contraction_order.rbegin();
	auto rend = node_contraction_order.rend();

	for (auto it = rbegin; it != rend; ++it) {
		auto node_id = *it;
		for (auto direction : {Direction::Forward, Direction::Backward}) {
			for (auto const& edge : graph.getDownEdgesOf(node_id, direction)) {
				std::forward<Function>(function)(edge, direction);
			}
		}
	}
}

template <typename Function>
auto CHTraversal::down(
    Function&& function, NodeID start_node_id, bool reset_seen) -> ExitValue
{
	auto start_node_ids = {start_node_id};
	return down(std::forward<Function>(function), start_node_ids, reset_seen);
}

// NOTE: Do not call this function recursively or in a parallelized
// implementation.
// Due to the static member this will trigger issues!
template <typename Function>
auto CHTraversal::down(Function&& function, NodeIDs const& start_node_ids,
    bool reset_seen) -> ExitValue
{
	// reusable stack used in the down search
	static NodeIDs node_stack;
	node_stack.clear();

	if (reset_seen) {
		seen.reset();
	}

	for (auto start_node_id : start_node_ids) {
		node_stack.emplace_back(start_node_id);
		seen.set(start_node_id, true);
	}

	while (!node_stack.empty()) {
		auto node_id = node_stack.back();
		node_stack.pop_back();

		for (auto direction : {Direction::Forward, Direction::Backward}) {
			for (auto const& edge : graph.getDownEdgesOf(node_id, direction)) {
				auto head_id = edge.getHead(direction);

				auto result = std::forward<Function>(function)(edge, direction);

				if (result == Result::StopSearch) {
					return ExitValue::Stopped;
				}

				if (result == Result::Expand && !seen[head_id]) {
					node_stack.emplace_back(head_id);
					seen.set(head_id, true);
				}
			}
		}
	}

	return ExitValue::NotStopped;
}

template <typename Function>
auto CHTraversal::downWithoutObsolete(
    Function&& function, NodeID start_node_id, bool reset_seen) -> ExitValue
{
	assert(graph.isSortedWRTEdgesStatus());
	auto start_node_ids = {start_node_id};
	return downWithoutObsolete(
	    std::forward<Function>(function), start_node_ids, reset_seen);
}

// NOTE: Do not call this function recursively or in a parallelized
// implementation.
// Due to the static member this will trigger issues!
template <typename Function>
auto CHTraversal::downWithoutObsolete(Function&& function,
    NodeIDs const& start_node_ids, bool reset_seen) -> ExitValue
{
	assert(graph.isSortedWRTEdgesStatus());
	// reusable stack used in the down search
	static NodeIDs node_stack;
	node_stack.clear();

	if (reset_seen) {
		seen.reset();
	}

	for (auto start_node_id : start_node_ids) {
		node_stack.emplace_back(start_node_id);
		seen.set(start_node_id, true);
	}

	while (!node_stack.empty()) {
		auto node_id = node_stack.back();
		node_stack.pop_back();

		for (auto direction : {Direction::Forward, Direction::Backward}) {
			for (auto const& edge :
			    graph.getNonObsoleteDownEdgesOf(node_id, direction)) {
				auto head_id = edge.getHead(direction);

				auto result = std::forward<Function>(function)(edge, direction);

				if (result == Result::StopSearch) {
					return ExitValue::Stopped;
				}

				if (result == Result::Expand && !seen[head_id]) {
					node_stack.emplace_back(head_id);
					seen.set(head_id, true);
				}
			}
		}
	}

	return ExitValue::NotStopped;
}

template <typename Function>
void CHTraversal::toParents(Function&& function) const
{
	for (auto edge_id : edge_creation_order) {
		for (auto parent_id : graph.getParents(edge_id)) {
			std::forward<Function>(function)(parent_id, edge_id);
		}
	}
}

template <typename Function>
auto CHTraversal::toParents(Function&& function, EdgeID start_edge_id) const
    -> ExitValue
{
	auto start_edge_ids = {start_edge_id};
	return toParents(std::forward<Function>(function), start_edge_ids);
}

template <typename Function>
auto CHTraversal::toParents(
    Function&& function, EdgeIDs const& start_edge_ids) const -> ExitValue
{
	using PQElement = std::pair<decltype(Node::level), EdgeID>;
	using PQ = std::priority_queue<PQElement, std::vector<PQElement>,
	    std::greater<PQElement>>;

	PQ pq;

	for (auto start_edge_id : start_edge_ids) {
		auto shortcut_node_id = graph.getShortcutNode(start_edge_id);
		if (shortcut_node_id == c::NO_NID) {
			pq.emplace(c::NO_LEVEL, start_edge_id);
		} else {
			pq.emplace(graph.getNode(shortcut_node_id).level, start_edge_id);
		}
	}

	while (!pq.empty()) {
		auto edge_id = pq.top().second;

		// pop edge and all duplicates
		while (!pq.empty() && pq.top().second == edge_id) {
			pq.pop();
		}

		for (auto parent_id : graph.getParents(edge_id)) {
			auto result = std::forward<Function>(function)(parent_id, edge_id);

			switch (result) {
			case Result::StopSearch:
				return ExitValue::Stopped;
			case Result::Expand:
				pq.emplace(graph.getShortcutLevel(parent_id), parent_id);
				break;
			case Result::Ignore:
				break;
			}
		}
	}

	return ExitValue::NotStopped;
}

template <typename Function>
void CHTraversal::toChildren(Function&& function) const
{
	auto const begin = edge_creation_order.rbegin();
	auto const end = edge_creation_order.rend();

	for (auto it = begin; it != end; ++it) {
		auto const& edge = graph.getEdge(*it);
		if (edge.isShortcut()) {
			std::forward<Function>(function)(edge.child_edge1, edge.id);
			std::forward<Function>(function)(edge.child_edge2, edge.id);
		}
	}
}

template <typename Function>
auto CHTraversal::toChildren(Function&& function, EdgeID start_edge_id) const
    -> ExitValue
{
	auto start_edge_ids = {start_edge_id};
	return toChildren(std::forward<Function>(function), start_edge_ids);
}

template <typename Function>
auto CHTraversal::toChildren(
    Function&& function, EdgeIDs const& start_edge_ids) const -> ExitValue
{
	using PQElement = std::pair<decltype(Node::level), EdgeID>;
	using PQ = std::priority_queue<PQElement>;

	PQ pq;

	for (auto start_edge_id : start_edge_ids) {
		auto shortcut_node_id = graph.getShortcutNode(start_edge_id);
		auto shortcut_node_level = graph.getNode(shortcut_node_id).level;
		pq.emplace(shortcut_node_level, start_edge_id);
	}

	while (!pq.empty()) {
		auto edge_id = pq.top().second;
		pq.pop();

		auto const& edge = graph.getEdge(edge_id);
		if (!edge.isShortcut()) {
			continue;
		}

		for (auto child_id : {edge.child_edge1, edge.child_edge2}) {
			auto result = std::forward<Function>(function)(child_id, edge.id);

			switch (result) {
			case Result::StopSearch:
				return ExitValue::Stopped;
			case Result::Expand:
				pq.emplace(graph.getShortcutLevel(child_id), child_id);
				break;
			case Result::Ignore:
				break;
			}
		}
	}

	return ExitValue::NotStopped;
}

} // namespace pf
