#pragma once

void initStxxl()
{
#if defined(USE_VALGRIND)
	Print("USE_VALGRIND ON");
#else
	Print("USE_VALGRIND OFF");
#endif

#if defined(PATHS_ON_DISK)
	Print("PATHS_ON_DISK ON");
#else
	Print("PATHS_ON_DISK OFF");
#endif

	stxxl::config* cfg = stxxl::config::get_instance();
	stxxl::disk_config disk("/tmp/stxxl.tmp", 1E10, "syscall unlink");
	disk.direct = stxxl::disk_config::DIRECT_ON; // force O_DIRECT
	cfg->add_disk(disk);
}
