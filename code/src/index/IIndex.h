#pragma once

#include "../basic_types.h"
#include "../defs.h"
#include "../path_intervall.h"
#include "../query.h"

namespace pf
{

/*
 * Interface of a index.
 */
class IIndex
{
public:
	virtual ~IIndex()
	{
	}

	virtual bool isContainedInPath(EdgeID edge_id) const = 0;

	virtual TimeIntervall getTimeIntervall(EdgeID edge_id) const = 0;

	virtual PathIntervalls getPathIntervalls(EdgeID edge_id) const = 0;

	virtual void getPathIDsOnlySpatial(EdgeID edge_id, PathIDs& path_ids,
	    NodeIDs& search_stack, std::vector<char>& collected) const = 0;

	virtual void getPathIDs(EdgeID edge_id, Query const& query,
	    PathIDs& path_ids, NodeIDs& search_stack,
	    std::vector<char>& collected) const = 0;

	virtual void init(uint numberOfEdges, uint numberOfPathEdges) = 0;

	virtual void preAddEdge() = 0;

	virtual void addPathIntervall(PathIntervall path_intervall) = 0;

	virtual void postAddEdge(uint offset) = 0;

	virtual void finish() = 0;

	virtual void clearCaches() const = 0;

	void pushIfNotYetCollected(
	    PathIDs& path_ids, PathID path_id, std::vector<char>& collected) const
	{
		if (!collected[path_id]) {
			collected[path_id] = 1;
			path_ids.push_back(path_id);
		}
	}
};
} // namespace pf
