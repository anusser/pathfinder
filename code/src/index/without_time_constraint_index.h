#pragma once

#include "IIndex.h"

namespace pf
{
// Inverse mapping from edge to path ids via an offset array
// which ignores time constraint
class WithoutTimeConstraintIndex : public IIndex
{
private:
	std::vector<PathID> edge_to_pathsID_array;
	std::vector<std::size_t> edge_to_paths_offsets;

public:
	WithoutTimeConstraintIndex()
	{
	}

	bool isContainedInPath(EdgeID edge_id) const
	{
		auto begin_index = edge_to_paths_offsets[edge_id];
		auto end_index = edge_to_paths_offsets[edge_id + 1];
		return begin_index != end_index;
	}

	TimeIntervall getTimeIntervall(EdgeID edge_id) const
	{
		Unused(edge_id);
		return c::FULL_INTERVALL;
	}

	PathIntervalls getPathIntervalls(EdgeID edge_id) const
	{
		Unused(edge_id);
		return PathIntervalls();
	}

	void getPathIDsOnlySpatial(EdgeID edge_id, PathIDs& path_ids,
	    NodeIDs& search_stack, std::vector<char>& collected) const
	{
		Unused(search_stack);
		Unused(collected);
		auto begin =
		    edge_to_pathsID_array.begin() + edge_to_paths_offsets[edge_id];
		auto end =
		    edge_to_pathsID_array.begin() + edge_to_paths_offsets[edge_id + 1];
		path_ids.insert(path_ids.end(), begin, end);
	}

	void getPathIDs(EdgeID edge_id, Query const& query, PathIDs& path_ids,
	    NodeIDs& search_stack, std::vector<char>& collected) const
	{
		Unused(query);
		getPathIDsOnlySpatial(edge_id, path_ids, search_stack, collected);
	}

	void init(uint numberOfEdges, uint numberOfPathEdges)
	{
		edge_to_paths_offsets.reserve(numberOfEdges + 1);
		edge_to_pathsID_array.reserve(numberOfPathEdges);
		edge_to_paths_offsets.push_back(0);
	}

	void preAddEdge()
	{
	}

	void addPathIntervall(PathIntervall path_intervall)
	{
		edge_to_pathsID_array.push_back(path_intervall.path_id);
	}

	void postAddEdge(uint offset)
	{
		edge_to_paths_offsets.push_back(offset);
	}

	void finish()
	{
	}

	void clearCaches() const
	{
	}
};

} // namespace pf
