#include "indices.h"
#include "IIndex.h"
#include "intervall_tree_index.h"
#include "vector_index.h"
#include "vector_index_on_disk.h"
#include "without_time_constraint_index.h"

namespace pf
{

std::unique_ptr<IIndex> createIndex(
    InvertedIndicesVariant inverted_indices_variant)
{
	switch (inverted_indices_variant) {
	case InvertedIndicesVariant::IntervallTree:
		return std::unique_ptr<IIndex>(new IntervallTreeIndex());
	case InvertedIndicesVariant::Vector:
		return std::unique_ptr<IIndex>(new VectorIndex());
	case InvertedIndicesVariant::VectorOnDisk:
		return std::unique_ptr<IIndex>(new VectorIndexOnDisk());
	case InvertedIndicesVariant::WithoutTimeConstraint:
		return std::unique_ptr<IIndex>(new WithoutTimeConstraintIndex());
	}

	return nullptr;
}

std::string invertedIndexVariantToString(
    InvertedIndicesVariant inverted_indices_variant)
{
	switch (inverted_indices_variant) {
	case InvertedIndicesVariant::IntervallTree:
		return "Intervall tree";
	case InvertedIndicesVariant::Vector:
		return "Vector";
	case InvertedIndicesVariant::VectorOnDisk:
		return "VectorOnDisk";
	case InvertedIndicesVariant::WithoutTimeConstraint:
		return "Without time constraint";
	}

	return "NULL";
}

} // namespace pf
