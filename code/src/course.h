#pragma once

#include "basic_types.h"

#include <cmath>

namespace pf
{

struct Course {
	double lat;
	double lon;

	Course(double lat, double lon) : lat(lat), lon(lon)
	{
		normalize();
	};

	Course(Node const& source, Node const& target)
	    : lat(target.coordinate.lat - source.coordinate.lat)
	    , lon(target.coordinate.lon - source.coordinate.lon)
	{
		normalize();
	};

	// This is just a dot product
	double calculateSimilarity(Course const& course)
	{
		return lat * course.lat + lon * course.lon;
	};

private:
	void normalize()
	{
		double length = sqrt(lat * lat + lon * lon);
		lat /= length;
		lon /= length;
	};
};
} // namespace pf
