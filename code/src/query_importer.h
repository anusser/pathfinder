#pragma once

#include "query.h"

#include <cstdint>
#include <sstream>
#include <string>
#include <vector>

namespace pf
{

class QueryImporter
{
public:
	QueryImporter(Graph const& graph, Queries& queries);

	// Returns true iff parsing was successful.
	bool importQueriesForPerformanceTest(std::string graph_filename,
	    const uint number_of_queries, double factorForBBSize);

	bool importQueries(std::string file_name);

private:
	bool already_parsed = false;

	Graph const& graph;
	Queries& queries;

	void readHeader(std::ifstream& f);
	void readQueries(std::ifstream& f);
	void readQuery(Query& query, std::string const& line);

	template <typename Out>
	void split(const std::string& s, char delim, Out result);
	std::vector<std::string> split(const std::string& s, char delim);
};

} // namespace pf
