#pragma once

#include "basic_types.h"
#include "edge_range.h"

#include <string>
#include <vector>

namespace pf
{

namespace unit_tests
{
void testGraph();
} // namespace unit_tests

// Forward declare to avoid circular dependency
class Path;

class Graph
{
public:
	Graph() = default;
	void init(std::string const& filename,
	    bool resort_IDs_for_spatial_locality = true);

	std::size_t getNumberOfNodes() const;
	std::size_t getNumberOfEdges() const;

	// The nodes are sorted (ascending) wrt their ids.
	Nodes const& getAllNodes() const;
	// Note: The returned edges don't have to be in any particular order.
	Edges const& getAllEdges() const;

	Node const& getNode(NodeID node_id) const;
	Edge const& getEdge(EdgeID edge_id) const;

	// This can be used to iterate over the edges using a range-based loop
	EdgeRange getOutEdgesOf(NodeID node_id) const;
	EdgeRange getInEdgesOf(NodeID node_id) const;
	// The following is just a convenience function that uses the first two
	EdgeRange getEdgesOf(NodeID node_id, Direction direction) const;

	// Only iterate over the up/down edges.
	EdgeRange getUpEdgesOf(NodeID node_id, Direction direction) const;
	EdgeRange getDownEdgesOf(NodeID node_id, Direction direction) const;
	EdgeRange getNonObsoleteDownEdgesOf(
	    NodeID node_id, Direction direction) const;
	EdgeRange getTreeDownEdgesOf(NodeID node_id, Direction direction) const;
	EdgeRange getNonObsoleteNonTreeDownEdgesOf(
	    NodeID node_id, Direction direction) const;

	// Gives the number of edges of a certain node
	std::size_t getNumberOfOutEdges(NodeID node_id) const;
	std::size_t getNumberOfInEdges(NodeID node_id) const;
	// The following are just convenience functions that uses the first two
	std::size_t getNumberOfEdges(NodeID node_id, Direction direction) const;
	std::size_t getNumberOfEdges(NodeID node_id) const;

	bool isUpEdge(EdgeID edge_id, Direction direction) const;
	bool isUpEdge(Edge const& edge, Direction direction) const;
	bool isDownEdge(EdgeID edge_id, Direction direction) const;
	NodeID getLowerNode(EdgeID edge_id) const;

	// Calculates all the edges for which child_edge1 or child_edge2 is edge_id.
	EdgeIDs getParents(EdgeID edge_id) const;

	// Get the node which is shortcut by this edge. Returns NO_NID if the passed
	// edge isn't a shortcut.
	NodeID getShortcutNode(EdgeID edge_id) const;
	// Returns the level at which the edge was created during the CH
	// construction. This equals the level of the shortcut node. If the edge
	// isn't a shortcut (and thus no shortcut level exists) it returns NO_LEVEL.
	Level getShortcutLevel(EdgeID edge_id) const;

	// Unpacks the shortcut and returns all the child edges (recursively).
	Path unpack(EdgeID edge_id) const;

	// This just returns a vector of node ids sorted (ascending) by the level.
	NodeIDs getContractionOrder() const;
	// Returns a possible order in which the edges were created during the
	// construction of the CH. If something should be "pushed up" or "pushed
	// down" in the CH, then this order can be used.
	EdgeIDs getEdgeCreationOrder() const;

	EdgeIDs getNonShortcutOutEdgesOf(NodeID node_id) const;

	bool isSortedWRTEdgesStatus() const;

	void sortWRTEdgeStatus(
	    std::vector<bool>& is_obsolete, std::vector<bool>& is_tree_edge);

private:
	Nodes nodes;

	Edges out_edges;
	Edges in_edges;
	// The edges are sorted primarily by increasing source id (out)/target id
	// (in); and secondarily by decreasing target level (out)/source level (in).
	std::vector<std::size_t> out_offsets;
	std::vector<std::size_t> in_offsets;

	// At position i of this vector you find the first index (of out_edges)
	// after the last up edge of the node with the id i. This information is
	// used to create edge iterators that just traverse all the up/down edges.
	std::vector<std::size_t> out_up_edges_end;
	// Similar as out_up_edges_end just for in edges.
	std::vector<std::size_t> in_up_edges_end;

	std::vector<std::size_t> out_non_obsolete_down_edges_end;

	std::vector<std::size_t> in_non_obsolete_down_edges_end;

	std::vector<std::size_t> out_tree_down_edges_end;

	std::vector<std::size_t> in_tree_down_edges_end;

	std::vector<std::size_t> id_to_index;

	bool sortedWRTEdgeStatus = false;

	// Builds the graph from the parser input
	void build(std::vector<Node>&& nodes_from_parser,
	    std::vector<Edge>&& edges_from_parser,
	    bool resort_IDs_for_spatial_locality);

	void resortIDsForSpatialLocality(
	    std::vector<Node> const& nodes, std::vector<Edge>& edges) const;

	// Checks the graph for consistency
	void check();

	void checkAfterSort() const;

	void setIdToIndexVector();

	friend void unit_tests::testGraph();

	Level getMaxLevel(std::vector<Node> const& nodes) const;
};

} // namespace pf
