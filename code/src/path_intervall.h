#pragma once

#include "basic_types.h"
#include "paths.h"
#include "timeIntervall.h"

#include <ostream>
#include <vector>

namespace pf
{
struct TimeIntervall;

struct PathIntervall {
	PathID path_id;
	TimeIntervall time_intervall;

	PathIntervall()
	{
	}

	PathIntervall(const PathID path_id, TimeIntervall const time_intervall)
	    : path_id(path_id), time_intervall(time_intervall)
	{
	}

	bool operator<(const PathIntervall& rhs) const
	{
		if (path_id < rhs.path_id) {
			return true;
		} else if (path_id > rhs.path_id) {
			return false;
		} else {
			return time_intervall < rhs.time_intervall;
		}
	}

	bool operator==(const PathIntervall& rhs) const
	{
		return path_id == rhs.path_id && time_intervall == rhs.time_intervall;
	}
};

using PathIntervalls = std::vector<PathIntervall>;

} // namespace pf
