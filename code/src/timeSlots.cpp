#include "timeSlots.h"
#include "bounding_box.h"

namespace pf
{

TimeSlots::TimeSlots() : TimeSlots(false)
{
}

TimeSlots::TimeSlots(bool valueForAll)
{
	if (valueForAll) {
		slots.set();

	} else {
		slots.reset();
	}
}

TimeSlots::TimeSlots(TimeIntervall const& time_intervall) : TimeSlots(false)
{
	expand(time_intervall);
}

TimeSlots::TimeSlots(std::string slots) : slots(slots)
{
}

bool TimeSlots::operator==(TimeSlots const& slotMask) const
{
	return this->slots == slotMask.slots;
}

void TimeSlots::expand(TimeIntervall const& time_intervall)
{
	if (!time_intervall.isValid()) {
		return;
	}

	if (time_intervall.max_time - time_intervall.min_time >= secondsPerWeek) {
		slots.set();
		return;
	}

	int min_slot_index = getIndex(time_intervall.min_time);
	int max_slot_index = getIndex(time_intervall.max_time);

	if (min_slot_index <= max_slot_index) {
		for (int slot_index = min_slot_index; slot_index <= max_slot_index;
		     slot_index++) {
			slots[slot_index] = true;
		}
	} else {
		// time slots represent periodic time intervalls, therefore we have
		// to handle this case

		// front
		for (int slot_index = 0; slot_index <= max_slot_index; slot_index++) {
			slots[slot_index] = true;
		}
		// back
		for (int slot_index = min_slot_index; slot_index <= nofslots;
		     slot_index++) {
			slots[slot_index] = true;
		}
	}
}

void TimeSlots::expand(TimeSlots const& slotMask)
{
	this->slots |= slotMask.slots;
}

void TimeSlots::intersect(TimeSlots const& slotMask)
{
	this->slots &= slotMask.slots;
}

bool TimeSlots::isValid() const
{
	return this->slots.any();
}

bool TimeSlots::isFull() const
{
	return this->slots.all();
}

int TimeSlots::getIndex(TimeType time)
{
	TimeType weekTime = time % secondsPerWeek;
	return weekTime / secondsPerSlot;
}

std::ostream& operator<<(std::ostream& stream, TimeSlots const& slotMask)
{
	stream << "Start";
	for (uint i = 0; i < slotMask.slots.size(); i++) {
		stream << slotMask.slots[i];
	}
	stream << "End";
	return stream;
}

} // namespace pf
