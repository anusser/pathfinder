#include "edge_range.h"

namespace pf
{

ReverseEdgeRange EdgeRange::reverse() const
{
	return ReverseEdgeRange(begin_it, end_it);
}

EdgeRange ReverseEdgeRange::reverse() const
{
	return EdgeRange(end_it.base(), begin_it.base());
}

} // namespace pf
