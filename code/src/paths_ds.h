#pragma once

#include "basic_types.h"
#include "paths.h"

#include <stack>
#include <stxxl/vector>
#include <unordered_map>

namespace pf
{

namespace unit_tests
{
void testPathsDS();
} // namespace unit_tests

class PathsDS
{
public:
	PathsDS();
	PathsDS& operator=(PathsDS const& paths_ds) = default;
	PathsDS(const PathsDS&) = default;

	virtual ~PathsDS()
	{
		offset_and_time_at_last_node_vector.clear();
		time_at_node_and_next_edge_vector.clear();
	}

	PathID getNrOfPaths() const;
	bool empty() const;

	void addPath(Path const& path);
	Path getPath(PathID path_id) const;

	size_t getPathLength(PathID path_id) const;

private:
	using Offset = size_t;

	// we pack these together because we have almost as many edges as nodes
	struct TimeAtNodeAndNextEdge {
		TimeType time_at_node;
		EdgeID edge_id;

		TimeAtNodeAndNextEdge() = default;

		TimeAtNodeAndNextEdge(TimeType time_at_node, EdgeID edge_id)
		    : time_at_node(time_at_node), edge_id(edge_id)
		{
		}
	};

	// to compensate the off by one relationship between nodes and edes we
	// store the time of last node with the offset
	struct OffsetAndTimeAtLastNode {
		Offset offset;
		TimeType time_at_last_node;

		OffsetAndTimeAtLastNode() = default;

		OffsetAndTimeAtLastNode(Offset offset, TimeType time_at_last_node)
		    : offset(offset), time_at_last_node(time_at_last_node)
		{
		}
	};

#if defined(PATHS_ON_DISK)
	using OffsetAndTimeAtLastNodeVector =
	    stxxl::VECTOR_GENERATOR<OffsetAndTimeAtLastNode>::result;
	using TimeAtNodeAndNextEdgeVector =
	    stxxl::VECTOR_GENERATOR<TimeAtNodeAndNextEdge>::result;
#else
	using OffsetAndTimeAtLastNodeVector = std::vector<OffsetAndTimeAtLastNode>;
	using TimeAtNodeAndNextEdgeVector = std::vector<TimeAtNodeAndNextEdge>;
#endif

	OffsetAndTimeAtLastNodeVector offset_and_time_at_last_node_vector;
	TimeAtNodeAndNextEdgeVector time_at_node_and_next_edge_vector;
};

} // namespace pf
