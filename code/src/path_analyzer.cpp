#include "path_analyzer.h"

#include "defs.h"
#include "statistics_util.h"

#include <algorithm>
#include <cstdint>
#include <limits>
#include <type_traits>
#include <vector>

namespace pf
{

PathAnalyzer::PathAnalyzer(Graph const& graph) : graph(graph), geometry(graph)
{
}

double PathAnalyzer::getLengthByNodesInKm(EdgeID const& edge_id) const
{
	Edge const& edge = graph.getEdge(edge_id);
	if (edge.isShortcut()) {
		return getLengthByNodesInKm(edge.child_edge1) +
		       getLengthByNodesInKm(edge.child_edge2);
	} else {
		Node const& src = graph.getNode(graph.getEdge(edge_id).source);
		Node const& tgt = graph.getNode(graph.getEdge(edge_id).target);
		return geometry.calcGeoDist(src.coordinate, tgt.coordinate);
	}
}

double PathAnalyzer::getLengthByNodesInKm(Path const& path) const
{
	double length = 0;
	for (EdgeID edge_id : path.getEdges()) {
		length += getLengthByNodesInKm(edge_id);
	}
	return length;
}

void PathAnalyzer::analyze(PathsDS const& paths_ds)
{
	std::vector<size_t> path_sizes;

	std::vector<double> path_lengths;

	long maxEndTime = std::numeric_limits<long>::min();

	std::vector<TimeType> startTimes;
	std::vector<TimeType> edgeTimes;

	uint pathsWithHopLengthZero = 0;

	for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); path_id++) {
		Path path = paths_ds.getPath(path_id);

		if (!checkPath(path)) {
			continue;
		}

		path_sizes.push_back(path.getEdges().size());

		double path_length = getLengthByNodesInKm(path);
		path_lengths.push_back(path_length);

		if (path.getEdges().size() >= 1) {

			TimeType start = path.getTimeIntervallForEdge(0).min_time;
			TimeType end =
			    path.getTimeIntervallForEdge(path.getEdges().size() - 1)
			        .max_time;

			if (start != c::INVALID_TIME && end != c::INVALID_TIME) {

				if (end >= maxEndTime) {
					maxEndTime = end;
				}

				startTimes.push_back(start);
			}
		} else {
			pathsWithHopLengthZero++;
		}

		for (EdgeIndex edge_index = 0; edge_index < path.getEdges().size();
		     edge_index++) {
			TimeIntervall time_intervall =
			    path.getTimeIntervallForEdge(edge_index);
			if (time_intervall.isValid()) {
				edgeTimes.push_back(
				    time_intervall.max_time - time_intervall.min_time);
			}
		}
	}

	// some start times are 0, it is likely that this is some artifical value
	// therefore we assume that the biggest difference is between 0 and the
	// smallest reasonable value and search for this
	std::sort(startTimes.begin(), startTimes.end());
	TimeType biggestDifference = std::numeric_limits<TimeType>::min();
	int index = -1;

	TimeType previousStartTime = startTimes.at(0);
	for (uint i = 1; i < startTimes.size(); i++) {
		TimeType diff = startTimes.at(i) - previousStartTime;
		if (diff > biggestDifference) {
			biggestDifference = diff;
			index = i;
		}
		previousStartTime = startTimes.at(i);
	}

	Print("minNofEdges");
	Print(statistics_util::calcMin(path_sizes));
	Print("maxNofEdges");
	Print(statistics_util::calcMax(path_sizes));
	Print("AveragePathSize:");
	Print(statistics_util::calcAverage(path_sizes));
	Print("pathSizeStandardDeviation");
	Print(statistics_util::calcStandardDeviaton(path_sizes));

	Print("min_path_length");
	Print(statistics_util::calcMin(path_lengths));
	Print("max_path_length");
	Print(statistics_util::calcMax(path_lengths));
	Print("average path length");
	Print(statistics_util::calcAverage(path_lengths));
	Print("standard variation path length");
	Print(statistics_util::calcStandardDeviaton(path_lengths));

	Print("pathsWithHopLengthZero:");
	Print(pathsWithHopLengthZero);
	Print("medianEdgeTime:");
	Print(statistics_util::median(edgeTimes));
	Print("maxEndTime:");
	Print(maxEndTime);

	Print("More reasonable start time at index");
	Print(index);
	Print("More reasonable start time is");
	Print(startTimes.at(index));
	Print("Start time before this index is");
	Print(startTimes.at(index - 1));
}

bool PathAnalyzer::checkPath(Path const& path) const
{
	TimeType previousMin = std::numeric_limits<TimeType>::min();
	TimeType previousMax = std::numeric_limits<TimeType>::min();
	for (EdgeIndex edge_index = 0; edge_index < path.getEdges().size();
	     edge_index++) {
		TimeIntervall time_intervall = path.getTimeIntervallForEdge(edge_index);
		if (time_intervall.min_time > time_intervall.max_time) {
			return false;
		}

		if (previousMin > time_intervall.min_time) {
			return false;
		}

		if (previousMax > time_intervall.max_time) {
			return false;
		}

		previousMin = time_intervall.min_time;
		previousMax = time_intervall.max_time;
	}
	return true;
}

} // namespace pf
