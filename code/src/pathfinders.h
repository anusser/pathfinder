#pragma once

#include "IPathfinder.h"
#include "naive_ch_pathfinder.h"
#include "naive_pathfinder.h"
#include "pathfinder.h"

namespace pf
{

enum class PathfinderType { STD, NAIVE, NAIVE_CH };

PathfinderType toPathfinderType(std::string const& type)
{
	if (type == "std") {
		return PathfinderType::STD;
	} else if (type == "naive") {
		return PathfinderType::NAIVE;
	} else if (type == "naive_ch") {
		return PathfinderType::NAIVE_CH;
	} else {
		std::cerr << "Unknown pathfinder type: " << type << "\n";
	}

	return PathfinderType::STD;
}

std::unique_ptr<IPathfinder> createPathfinder(
    PathfinderType pathfinder_type, PathfinderDS& ds, uint nr_of_threads)
{
	switch (pathfinder_type) {
	case PathfinderType::STD:
		return std::unique_ptr<IPathfinder>(new Pathfinder(ds, nr_of_threads));
	case PathfinderType::NAIVE:
		return std::unique_ptr<IPathfinder>(new NaivePathfinder(ds));
	case PathfinderType::NAIVE_CH:
		return std::unique_ptr<IPathfinder>(
		    new NaiveCHPathfinder(ds, nr_of_threads));
	}

	return nullptr;
}
} // namespace pf
