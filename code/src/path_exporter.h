#pragma once

#include "paths.h"
#include "paths_ds.h"

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

namespace pf
{

class PathExporter
{
public:
	PathExporter();

	void exportPathsForPerformanceTest(PathsDS const& paths_ds,
	    std::string graph_filename, uint distance) const;

	void exportPaths(const PathsDS& paths_ds, std::string file_name) const;

	void exportPathsBin(const PathsDS& paths_ds, std::string file_name) const;

private:
	void writeHeader(PathsDS const& paths_ds, std::ofstream& f) const;
	void writePaths(PathsDS const& paths_ds, std::ofstream& f) const;

	void writeBinHeader(PathsDS const& paths_ds, std::ofstream& f) const;
	void writeBinPaths(PathsDS const& paths_ds, std::ofstream& f) const;
};

} // namespace pf
