#include "defs.h"
#include "exporter.h"
#include "flags.h"
#include "graph.h"
#include "naive_ch_pathfinder.h"
#include "naive_pathfinder.h"
#include "path_generator.h"
#include "path_parser.h"
#include "pathfinder.h"
#include "pathfinder_ds.h"
#include "random.h"
#include "stxxl_init.h"
#include "time_tracker.h"
#include <getopt.h>

#include <sstream>
#include <string>

using namespace pf;

void printUsage()
{
	std::cout
	    << "Usage: ./pathfinder [ARGUMENTS]\n"
	    << "Mandatory arguments are:\n"
	    << "  -i, --graph_filename <path>        Read graph from <path>\n"
	    << "  -e, --export_filename <out>        Export result to <path>\n"
	    << "Mutual exclusive arguments are:\n"
	    << "  -p, --path_filename <path>        Read paths from <path>\n"
	    << "  -g, --number_of_paths <number>        Generate <number> random "
	       "paths \n"
	    << "Optional arguments are:\n"
	    << "  -r, --query_rectangle <min_coordinate>:<max_coordinate>\n where "
	       "<min_coordinate> and <max_coordinate> give the\n"
	       "coordinates of the query bounding box and have to be\n"
	       "of the form:\n"
	       "<lat>,<lon> (e.g., 64.244740,17.385652) \n"
	    << "  -t, --time_interval <min_time>,<max_time>\n where "
	       "<min_time> and <max_time> define the query time interval and must "
	       "be given in unix time, e.g.:\n"
	       "1184695221,1184695217 \n"
	    << "  -s, --time_slot <bitvector>       <bitvector> defines query time "
	       "slots within a week and must be given as a 64-bit string, e.g.:\n"
	       "1111111111111111111111111111111100000000000000000000000000000000"
	       "for the first half of the week."
	    << std::endl;
}

Coordinate toCoordinate(std::string const& coordinate_string)
{
	std::stringstream stream(coordinate_string);
	std::string line;

	std::getline(stream, line, ',');
	auto lat = std::stod(line);
	std::getline(stream, line, ',');
	auto lon = std::stod(line);

	return Coordinate(lat, lon);
}

BoundingBox toBoundingBox(std::string const& bounding_box_string)
{
	std::stringstream stream(bounding_box_string);
	std::string line;
	std::getline(stream, line, ':');
	auto min_coordinate = toCoordinate(line);
	std::getline(stream, line, ':');
	auto max_coordinate = toCoordinate(line);
	return BoundingBox(min_coordinate, max_coordinate);
}

TimeIntervall toTimeInterval(std::string const& time_interval_string)
{
	std::stringstream stream(time_interval_string);
	std::string line;
	std::getline(stream, line, ',');
	auto min_time = std::stod(line);
	std::getline(stream, line, ',');
	auto max_time = std::stod(line);
	return TimeIntervall(min_time, max_time);
}

Query askUserForQuery()
{
	std::string bounding_box_string;

	std::cout << "Enter the coordinates of a query box."
	          << "They have to be of the form:"
	          << "<min_lat>,<min_lon>:<max_lat>,<max_lon>"
	          << " (or \"quit\"):" << std::endl;
	std::getline(std::cin, bounding_box_string);

	if (bounding_box_string == "quit") {
		return BoundingBox();
	}

	BoundingBox bb = toBoundingBox(bounding_box_string);
	Query query = Query(bb);

	std::string time_interval_string;

	std::cout << "Enter the start and end of a time intervall query."
	          << "They have to be of the form:"
	          << "<min_time>,<max_time>"
	          << " (or \"skip\"):" << std::endl;
	std::getline(std::cin, time_interval_string);

	if (time_interval_string != "skip") {
		query.time_intervall = toTimeInterval(time_interval_string);
	} else {
		query.time_intervall = c::FULL_INTERVALL;
	}

	std::string time_slots_string;

	std::cout << "Enter the slots of the time slot query."
	          << "They must be given as 64-bit string."
	          << " (or \"skip\"):" << std::endl;
	std::getline(std::cin, time_slots_string);

	if (time_slots_string != "skip") {
		query.time_slots = TimeSlots(time_slots_string);
	} else {
		query.time_slots = TimeSlots(true);
	}

	return query;
}

bool askForExport()
{
	std::string export_string;
	std::cout << "Do you want to export the result (default: n)? (y/n) ";
	std::getline(std::cin, export_string);

	if (export_string == "y" || export_string == "yes") {
		return true;
	}

	return false;
}

void exportResults(std::string const& export_filename,
    PathfinderDS const& pathfinder_ds, BoundingBox query_box,
    PathIDs const& intersection_path_ids)
{
	TimeTracker time_tracker;
	Print("Unpack paths...");
	time_tracker.start();

	ExportData data;
	ExportColor blue{0, 0, 255, 255};
	ExportColor black{0, 0, 0, 255};
	ExportColor red{255, 0, 0, 255};

	data.filename = export_filename;
	data.boxes = {query_box};
	data.box_colors = {blue};

	Flags<bool> is_intersection_path(
	    pathfinder_ds.getPathsDS().getNrOfPaths(), false);
	for (auto intersection_path_id : intersection_path_ids) {
		is_intersection_path.set(intersection_path_id, true);
	}

	long intersectionPathsCount = 0;
	for (uint i = 0; i < is_intersection_path.size(); i++) {
		if (is_intersection_path[i]) {
			intersectionPathsCount++;
		}
	}

	Print("Retrieved " + std::to_string(intersectionPathsCount) + " paths");

	Graph const& graph = pathfinder_ds.getGraph();

	for (PathID path_id = 0;
	     path_id < (PathID)pathfinder_ds.getPathsDS().getNrOfPaths();
	     ++path_id) {

		if (is_intersection_path[path_id]) {
			Path unpacked =
			    pathfinder_ds.getPathsDS().getPath(path_id).unpack(graph);
			data.paths_ds.addPath(unpacked);
			data.path_colors.emplace_back(red);
		}
	}
	for (PathID path_id = 0;
	     path_id < (PathID)pathfinder_ds.getPathsDS().getNrOfPaths();
	     ++path_id) {

		if (!is_intersection_path[path_id]) {
			Path unpacked =
			    pathfinder_ds.getPathsDS().getPath(path_id).unpack(graph);
			data.paths_ds.addPath(unpacked);
			data.path_colors.emplace_back(black);
		}
	}

	time_tracker.stop();
	Print("Took " << time_tracker.getLastMeasurment() << " seconds");

	Exporter exporter(pathfinder_ds.getGraph());

	Print("Write simplestGraphRendererFile...");
	time_tracker.start();
	exporter.exportSimplestGraphRendererFile(data);
	time_tracker.stop();
	Print("Took " << time_tracker.getLastMeasurment() << " seconds");
}

int main(int argc, char* argv[])
{
	initStxxl();

	std::string graph_filename;
	PathDataSource path_data_source(PathDataSource::None);
	std::string path_filename;
	std::string export_filename;
	std::size_t number_of_random_paths = 0;
	BoundingBox query_box;
	TimeIntervall query_interval;
	TimeSlots query_slots;
	const uint nr_of_threads = 4;

	/*
	 * Getopt argument parsing.
	 */

	const struct option longopts[] = {
	    {"help", no_argument, 0, 'h'},
	    {"graph_filename", required_argument, 0, 'i'},
	    {"path_filename", required_argument, 0, 'p'},
	    {"number_of_generated_paths", required_argument, 0, 'g'},
	    {"query_rectangle", required_argument, 0, 'r'},
	    {"query_interval", required_argument, 0, 't'},
	    {"query_slots", required_argument, 0, 's'},
	    {"export_filename", required_argument, 0, 'e'},
	};

	int index(0);
	int iarg(0);
	opterr = 1;

	while ((iarg = getopt_long(
	            argc, argv, "hi:p:g:r:t:s:e:", longopts, &index)) != -1) {
		switch (iarg) {
		case 'h':
			printUsage();
			return 0;
			break;
		case 'i':
			graph_filename = optarg;
			break;
		case 'p':
			path_data_source = PathDataSource::ReadInMatched;
			path_filename = optarg;
			break;
		case 'g': {
			if (path_data_source != PathDataSource::None) {
				std::cerr
				    << "The path data source has to be unique! Exiting.\n";
				std::cerr << "Use ./pathfinder --help to print the usage.\n";
				return 1;
			}
			path_data_source = PathDataSource::Generate;

			size_t idx = 0; // index of first "non digit"
			number_of_random_paths = std::stoi(optarg, &idx);
			if ('\0' != optarg[idx] || number_of_random_paths <= 0) {
				std::cerr << "Invalid path count: '" << optarg << "'\n";
				return 1;
			}
		} break;
		case 'r': {
			query_box = toBoundingBox(optarg);
		} break;
		case 't': {
			query_interval = toTimeInterval(optarg);
		} break;
		case 's': {
			std::string bit_vector_string = optarg;
			query_slots = TimeSlots(bit_vector_string);
		} break;
		case 'e': {
			export_filename = optarg;
		} break;
		default:
			printUsage();
			return 1;
			break;
		}
	}

	if (graph_filename == "") {
		std::cerr << "No input file specified! Exiting.\n";
		std::cerr << "Use ./pathfinder --help to print the usage.\n";
		return 1;
	}

	Random random;
	TimeTracker time_tracker;

	Print("Read and init graph...");
	time_tracker.start();
	Graph graph;

	graph.init(graph_filename);

	time_tracker.stop();
	Print("Took " << time_tracker.getLastMeasurment() << " seconds");

	PathsDS paths_ds;

	switch (path_data_source) {
	case PathDataSource::Generate: {
		Print("Generate " << number_of_random_paths << " random paths...");
		time_tracker.start();
		PathGenerator path_generator(graph, random, nr_of_threads);
		paths_ds =
		    path_generator.getRandomShortestPaths(number_of_random_paths, 50);
		time_tracker.stop();
		Print("Took " << time_tracker.getLastMeasurment() << " seconds");
		// the paths are already compressed
		break;
	}
	case PathDataSource::ReadInMatched: {
		Print("Read paths...");
		time_tracker.start();
		PathsDS raw_paths_ds;
		PathParser path_parser(graph, raw_paths_ds);
		bool success = path_parser.parse(path_filename);

		if (!success) {
			throw Exception(
			    "Parsing of the following path file failed:\n" + path_filename);
		}
		time_tracker.stop();
		Print("Took " << time_tracker.getLastMeasurment() << " seconds");

		paths_ds = partitioner_util::partitionPaths(graph, raw_paths_ds);
		break;
	}
	case PathDataSource::Import:
		std::cerr << "Not valid in this context. Exiting.\n";
		std::cerr << "Use ./performance_test --help to print the usage.\n";
		return 1;
	case PathDataSource::None:
		std::cerr << "No path data source specified! Exiting.\n";
		std::cerr << "Use ./performance_test --help to print the usage.\n";
		return 1;
	default:
		assert(false);
	}

	Print("Setup search data structures...");
	//	time_tracker.start();
	PathfinderDS pathfinder_ds(graph, nr_of_threads, std::move(paths_ds));
	Pathfinder pathfinder(pathfinder_ds, nr_of_threads);
	//	time_tracker.stop();
	//	Print("Took " << time_tracker.getLastMeasurment() << " seconds");

	Print("Execute pathfinder algorithm...");
	PathIDs intersection_path_ids;

	Query query(query_box);
	if (query.bounding_box.isValid()) {
		if (query_interval.isValid()) {
			query.time_intervall = query_interval;
		} else {
			query.time_intervall = c::FULL_INTERVALL;
		}

		if (query_slots.isValid()) {
			query.time_slots = query_slots;
		} else {
			query.time_slots = TimeSlots(true);
		}

		time_tracker.start();
		intersection_path_ids = pathfinder.run(query);
		time_tracker.stop();
		Print("Took " << time_tracker.getLastMeasurment() << " seconds");
		Print("Details of the time measurements:");
		pathfinder.printTimes();

		Print("Export the results...");
		exportResults(export_filename, pathfinder_ds, query.bounding_box,
		    intersection_path_ids);
	} else {
		Print("");
		Print("In this mode you can enter queries interactively.");
		Print("The result of the last query is exported.");
		Print("");

		while (true) {
			auto input_query = askUserForQuery();

			if (!input_query.bounding_box.isValid()) {
				break;
			}
			query = input_query;

			time_tracker.start();
			intersection_path_ids = pathfinder.run(query);
			time_tracker.stop();
			Print("Took " << time_tracker.getLastMeasurment() << " seconds");
			Print("Details of the time measurements:");
			pathfinder.printTimes();

			bool export_result = askForExport();
			if (export_result) {
				Print("Export the results...");
				exportResults(export_filename, pathfinder_ds, query_box,
				    intersection_path_ids);
			}
			Print("");
		}
	}

	return EXIT_SUCCESS;
}
