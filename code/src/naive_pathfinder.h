#pragma once

#include "IPathfinder.h"
#include "basic_types.h"
#include "flags.h"
#include "geometry.h"
#include "pathfinder_ds.h"
#include "paths.h"
#include "time_tracker.h"

namespace pf
{

namespace unit_tests
{
void testPathfinder();
} // namespace unit_tests

class NaivePathfinder : public IPathfinder
{
private:
	using Result = CHTraversal::Result;
	using ExitValue = CHTraversal::ExitValue;

	PathfinderDS const& ds;
	Graph const& graph;
	Geometry geometry;

public:
	NaivePathfinder(PathfinderDS const& ds)
	    : ds(ds), graph(ds.getGraph()), geometry(ds.getGraph())
	{
	}

	PathIDs run(Query const& query)
	{
		PathIDs path_ids;

		PathID number_of_paths = ds.getPathsDS().getNrOfPaths();
		for (PathID path_id = 0; path_id < number_of_paths; ++path_id) {
			Path const path = ds.getPath(path_id);

			for (EdgeIndex edge_index = 0; edge_index < path.getEdges().size();
			     edge_index++) {

				Path edge_path = graph.unpack(path.getEdge(edge_index));

				for (EdgeID base_edge_id : edge_path.getEdges()) {

					if (geometry.intersect(query.bounding_box, base_edge_id)) {
						// ATTENTION: We check the geometry intersection for the
						// base edges, but the time for the retrieved edges
						TimeIntervall time_intervall =
						    path.getTimeIntervallForEdge(edge_index);
						time_intervall.intersect(query.time_intervall);
						TimeSlots time_slots(time_intervall);
						time_slots.intersect(query.time_slots);

						if (time_intervall.isValid() && time_slots.isValid()) {
							path_ids.push_back(path_id);
						}
					}
				}
			}
		}

		// remove duplicates
		std::sort(path_ids.begin(), path_ids.end());
		auto new_end = std::unique(path_ids.begin(), path_ids.end());
		path_ids.erase(new_end, path_ids.end());

		return path_ids;
	}
};

} // namespace pf
