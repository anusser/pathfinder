#pragma once

#include "defs.h"
#include "dijkstra.h"
#include "graph.h"
#include "paths.h"

#include <memory>
#include <unordered_map>
#include <vector>

namespace pf
{

namespace unit_tests
{
void testShortestPathPartitioner();
} // namespace unit_tests

class ShortestPathPartitioner
{
public:
	ShortestPathPartitioner(Graph const& graph) : graph(graph){};
	virtual ~ShortestPathPartitioner(){};

	virtual Path run(Path const& path) = 0;
	uint getNofShopaPartsForLastPath()
	{
		return nof_shopa_parts_sum;
	};

protected:
	Graph const& graph;
	// The number of shortest path parts the last path was divided in
	uint nof_shopa_parts_sum = 0;
};

//
// IncrementalPartitioner
//

class IncrementalPartitioner : public ShortestPathPartitioner
{
public:
	IncrementalPartitioner(Graph const& graph)
	    : ShortestPathPartitioner(graph){};
	~IncrementalPartitioner(){};
	virtual Path run(Path const& path) override;

private:
	using Index = Path::EdgeIndex;

	EdgeID getCommonParent(EdgeID edgeID1, EdgeID edgeID2);
	EdgeID getCommonParent(
	    EdgeRange edges, NodeID target, EdgeID edge1, EdgeID edge2);
};

} // namespace pf
