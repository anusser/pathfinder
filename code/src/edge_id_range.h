#pragma once

#include "basic_types.h"

namespace pf
{

namespace unit_tests
{
void testEdgeRange();
} // namespace unit_tests

class ReverseEdgeIDRange;

class EdgeIDRange
{
public:
	using iterator = EdgeIDs::const_iterator;

	EdgeIDRange(iterator const& begin, iterator const& end)
	    : begin_it(begin), end_it(end)
	{
	}

	iterator begin() const
	{
		return begin_it;
	};

	iterator end() const
	{
		return end_it;
	};

	ReverseEdgeIDRange reverse() const;

private:
	iterator begin_it;
	iterator end_it;

	friend void unit_tests::testEdgeRange();
};

class ReverseEdgeIDRange
{
public:
	using iterator = EdgeIDs::const_iterator;
	using reverse_iterator = EdgeIDs::const_reverse_iterator;

	ReverseEdgeIDRange(iterator const& begin, iterator const& end)
	    : begin_it(end), end_it(begin)
	{
	}

	reverse_iterator begin() const
	{
		return begin_it;
	};

	reverse_iterator end() const
	{
		return end_it;
	};

	EdgeIDRange reverse() const;

private:
	reverse_iterator begin_it;
	reverse_iterator end_it;
};

} // namespace pf
