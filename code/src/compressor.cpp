#include "defs.h"
#include "graph.h"
#include "partitioner_util.h"
#include "path_analyzer.h"
#include "path_exporter.h"
#include "path_importer.h"
#include "path_parser.h"
#include "paths_ds.h"
#include "plot_data_writer.h"
#include "statistics_util.h"
#include "stxxl_init.h"
#include "time_tracker.h"

#include <cmath>
#include <fstream>
#include <getopt.h>
#include <math.h>
#include <string>

using namespace pf;

void printUsage()
{
	std::cout
	    << "Usage: ./compressor [ARGUMENTS]\n"
	    << "  -c, --compress        Compress paths by partitioning them, this "
	       "loses some time information\n"
	    << "  -b, --binary        Export in binary format\n"
	    << "  -i, --graph_filename <path>        Read graph from <path>\n"
	    << "  -p, --path_filename <path>        Read paths from <path>\n"
	    << "  -e, --export_filename <path>       Export paths to <path>\n";
}

std::vector<double> calcEdgeCompressionRatios(PathsSizes const& paths_sizes)
{
	std::vector<double> edge_compression_rates;
	for (PathSizes path_sizes : paths_sizes) {
		if (path_sizes.compressed_size != 0) {
			edge_compression_rates.push_back(
			    (double)path_sizes.original_size / path_sizes.compressed_size);
		}
	}
	return edge_compression_rates;
}

int main(int argc, char* argv[])
{
	initStxxl();

	std::string graph_filename;
	std::string paths_filename;
	std::string export_filename;
	bool compress_by_partition = false;
	bool export_as_binary = false;

	/*
	 * Getopt argument parsing.
	 */

	const struct option longopts[] = {
	    {"help", no_argument, 0, 'h'},
	    {"compress", no_argument, 0, 'c'},
	    {"binary", no_argument, 0, 'b'},
	    {"graph_filename", required_argument, 0, 'i'},
	    {"path_filename", required_argument, 0, 'p'},
	    {"export_filename", required_argument, 0, 'e'},
	};

	int index(0);
	int iarg(0);
	opterr = 1;

	while (
	    (iarg = getopt_long(argc, argv, "hcbi:p:e:", longopts, &index)) != -1) {
		switch (iarg) {
		case 'h':
			printUsage();
			return 0;
			break;
		case 'i':
			graph_filename = optarg;
			break;
		case 'p':
			paths_filename = optarg;
			break;
		case 'e':
			export_filename = optarg;
			break;
		case 'c':
			compress_by_partition = true;
			break;
		case 'b':
			export_as_binary = true;
			break;
		default:
			printUsage();
			return 1;
			break;
		}
	}

	Print("Read and init graph...");
	Graph graph;
	graph.init(graph_filename);

	Print("Read paths...");
	PathsDS paths_ds;
	PathParser path_parser(graph, paths_ds);
	bool success = path_parser.parse(paths_filename);

	if (!success) {
		throw Exception(
		    "Parsing of the following path file failed:\n" + paths_filename);
	}

	Print("Analyze paths...");
	PathAnalyzer path_analyzer(graph);
	path_analyzer.analyze(paths_ds);

	PathsDS paths_ds_to_export;

	if (compress_by_partition) {
		PathsSizes paths_sizes;
		paths_sizes.reserve(paths_ds.getNrOfPaths());

		Print("Partition paths...");
		TimeTracker time_tracker;
		time_tracker.start();

		// compress the paths
		PathsDS partitioned_paths_ds =
		    partitioner_util::partitionPaths(graph, paths_ds);

		PlotDataWriter plot_data_writer;
		plot_data_writer.writePathSizes(paths_sizes, paths_filename);

		std::vector<double> edge_compression_rates =
		    calcEdgeCompressionRatios(paths_sizes);
		Print("The average edge compression ratio is");
		Print(statistics_util::calcAverage(edge_compression_rates));
		Print("The standard deviation edge compression ratio is ");
		Print(statistics_util::calcVariance(edge_compression_rates));
		Print("The max edge compression ratio is");
		Print(statistics_util::calcMax(edge_compression_rates));

		paths_ds_to_export = std::move(partitioned_paths_ds);
	} else {
		paths_ds_to_export = std::move(paths_ds);
	}

	Print("Analyze exported paths ...");
	path_analyzer.analyze(paths_ds_to_export);

	Print("Paths to be exported:");
	Print(paths_ds_to_export.getNrOfPaths());

	Print("Export partitioned paths...");
	PathExporter pe;

	if (export_as_binary) {
		pe.exportPathsBin(paths_ds_to_export, export_filename);
	} else {
		pe.exportPaths(paths_ds_to_export, export_filename);
	}

	return EXIT_SUCCESS;
}
