#pragma once

#include "basic_types.h"
#include "bounding_box.h"
#include "geometry_types.h"
#include "graph.h"

namespace pf
{

namespace unit_tests
{
void testGeometry();
} // namespace unit_tests

class Geometry
{
public:
	Geometry(Graph const& graph);

	Vertex getVertex(NodeID node_id) const;
	Segment getSegment(EdgeID edge_id) const;
	bool nodeInBox(Vertex const& vertex, BoundingBox const& box) const;
	Orientation calcOrientation(
	    Segment const& segment, Vertex const& vertex) const;

	Vertices getVertices(BoundingBox const& box) const;
	Segments getSegments(BoundingBox const& box) const;

	bool intersect(BoundingBox const& query_box, EdgeID edge_id) const;
	bool intersect(BoundingBox const& query_box, Path const& path) const;
	double calcGeoDist(Coordinate coordinate1, Coordinate coordinate2) const;

private:
	static constexpr double R = 6371; // earth radius in kilometers

	Graph const& graph;
	double square(double x) const;
	double pythagoras(double a, double b) const;
	double calcGeoDist(
	    double lon1, double lat1, double lon2, double lat2) const;
};

} // namespace pf
