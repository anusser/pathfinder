#include "paths_as_list.h"
#include "shortest_path_partitioner.h"

#include <algorithm>
#include <cstdint>
#include <queue>

namespace pf
{

Path IncrementalPartitioner::run(Path const& path)
{
	PathAsList path_as_list(path);

	if (path_as_list.edges.size() < 2) {
		// nothing to be compressed here
		return path_as_list.makeVectorPath(graph);
	}

	while (!path_as_list.iteratorIsAtEnd()) {

		if (path_as_list.iteratorIsAtBegin()) {
			++path_as_list.iterator;
		}

		EdgeID prev = path_as_list.getPreviosEdgeID();
		EdgeID cur = path_as_list.getCurrentEdgeID();
		EdgeID commonParent = getCommonParent(prev, cur);

		if (commonParent != c::NO_EID) {
			path_as_list.lastTwoEdgesToShortcut(graph, commonParent);
			if (path_as_list.edges.size() < 2) {
				// nothing to compress anymore
				break;
			}
		} else {
			++path_as_list.iterator;
		}
	}

	return path_as_list.makeVectorPath(graph);
}

EdgeID IncrementalPartitioner::getCommonParent(EdgeID edgeID1, EdgeID edgeID2)
{
	Edge const& edge1 = graph.getEdge(edgeID1);
	Edge const& edge2 = graph.getEdge(edgeID2);

	// source and target for possible shortcut
	NodeID sourceID = edge1.source;
	NodeID targetID = edge2.target;
	Node const& source = graph.getNode(sourceID);
	Node const& target = graph.getNode(targetID);

	if (source.level == target.level) {
		// this means there can not exist a shortcut
		return c::NO_EID;
	} else if (source.level < target.level) {
		EdgeRange sh_candidates =
		    graph.getUpEdgesOf(sourceID, Direction::Forward);
		return getCommonParent(sh_candidates, targetID, edgeID1, edgeID2);
	} else {
		EdgeRange sh_candidates =
		    graph.getDownEdgesOf(sourceID, Direction::Forward);
		return getCommonParent(sh_candidates, targetID, edgeID1, edgeID2);
	}
}

EdgeID IncrementalPartitioner::getCommonParent(
    EdgeRange edges, NodeID target, EdgeID edge1, EdgeID edge2)
{
	EdgeID commonEdgeID = c::NO_EID;

	for (Edge const& edge : edges) {
		if (edge.target == target) {
			if (edge.child_edge1 == edge1 && edge.child_edge2 == edge2) {
				commonEdgeID = edge.id;
				break;
			}
		}
	}

	return commonEdgeID;
}

} // namespace pf
