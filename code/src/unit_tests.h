#pragma once

#include "defs.h"

namespace pf
{

namespace unit_tests
{
void testAll(unsigned int seed);
void testAll();

void testStxxl();

void testIndexVariants();

#if defined(PATHS_ON_DISK)
const uint nr_of_threads = 1;
#else
const uint nr_of_threads = 2;
#endif

const std::size_t number_of_paths = 100;

const double max_path_distance = 50;

const std::size_t number_of_queries = 100;

const std::size_t max_number_of_queries_to_find_example = 100000;

const std::size_t number_of_simple_tests = 100;
} // namespace unit_tests

} // namespace pf
