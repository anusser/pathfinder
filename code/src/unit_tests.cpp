#include "unit_tests.h"

#include "ch_traversal.h"
#include "defs.h"
#include "dijkstra.h"
#include "exporter.h"
#include "flags.h"
#include "geometry.h"
#include "graph.h"
#include "index/indices.h"
#include "interval_tree.h"
#include "merge_sorted_unique.h"
#include "naive_ch_pathfinder.h"
#include "naive_pathfinder.h"
#include "parser.h"
#include "partitioner_util.h"
#include "path_generator.h"
#include "path_parser.h"
#include "pathfinder.h"
#include "pathfinder_ds.h"
#include "paths.h"
#include "random.h"
#include "set_util.h"
#include "stxxl_init.h"

#include <algorithm>
#include <list>
#include <memory>
#include <string>
#include <unordered_set>

#include <stxxl/vector>

//
// helper functions/macros
//

#define Test(x)                                                                \
	do {                                                                       \
		if (!(x)) {                                                            \
			std::cout << "\n";                                                 \
			std::cout << "TEST_FAILED!\n";                                     \
			std::cout << "File: " << __FILE__ << "\n";                         \
			std::cout << "Line: " << __LINE__ << "\n";                         \
			std::cout << "Function: " << __func__ << "\n";                     \
			std::cout << "Test: " << #x << "\n";                               \
			std::cout << "\n";                                                 \
			std::cout << std::flush;                                           \
			std::abort();                                                      \
		}                                                                      \
	} while (0)

namespace
{

void printTestHeader(std::string const& test_name)
{
	std::string line(8 + test_name.length(), '-');

	Print("Testing " << test_name);
	Print(line);
}

void printTestFooter(std::string const& test_name)
{
	std::string line(16 + test_name.length(), '=');

	Print("");
	Print(line);
	Print(test_name << " test successful");
	Print(line);
	Print("");
}

} // namespace

//
// Tests
//

namespace pf
{

static Random random;

void unit_tests::testAll(unsigned int seed)
{
	random.seed(seed);
	testAll();
}

void unit_tests::testAll()
{
	initStxxl();

	Print("Executing all unit tests");
	Print("========================");
	Print("");
	Print("Using random seed value: " << random.getSeed());
	Print("");

	unit_tests::testStxxl();
	unit_tests::testIntervalTree();
	unit_tests::testMergeSortedUnique();
	unit_tests::testExporter();
	unit_tests::testPathParser();
	unit_tests::testRTree();
	unit_tests::testSimpleFlags();
	unit_tests::testFlags();
	unit_tests::testParser();

	unit_tests::testGraph();
	unit_tests::testPaths();
	unit_tests::testPathsDS();

	unit_tests::testPathGenerator();
	unit_tests::testRandom();
	unit_tests::testEdgeRange();
	unit_tests::testDijkstra();
	unit_tests::testNodeRange();
	unit_tests::testShortestPathPartitioner();
	unit_tests::testAmbiguousShopas();

	unit_tests::testPathfinderDS();
	unit_tests::testEdgeCandidateFinder();
	unit_tests::testPathfinder(1);
	unit_tests::testPathfinder(nr_of_threads);
	unit_tests::testConservativeFilter();
	unit_tests::testEdgeSorting();
	unit_tests::testIndexVariants();
	unit_tests::testCHTraversal();
}

void unit_tests::testStxxl()
{
	printTestHeader("Stxxl");

	typedef stxxl::VECTOR_GENERATOR<int>::result vector;
	vector my_vector;

	size_t nofIntegers = 1E6;
	Print("Vector size on disk:");
	Print(nofIntegers);

	Random random;

	for (uint i = 0; i < nofIntegers; i++) {
		my_vector.push_back(random.getInt(0, nofIntegers));
	}

	std::sort(my_vector.begin(), my_vector.end());

	Print("First element:");
	Print(my_vector.front());
	int last_elem = my_vector.front();
	for (uint i = 1; i < my_vector.size(); i++) {
		int cur_elem = my_vector[i];
		Test(last_elem <= cur_elem);
		last_elem = cur_elem;
	}
	Print("Last element:");
	Print(my_vector.back());

	my_vector.clear();

	printTestFooter("Stxxl");
}

void unit_tests::testParser()
{
	printTestHeader("Parser");

	Nodes nodes;
	Edges edges;

	Parser parser(nodes, edges);
	bool success = parser.parse("../../data/test.graph");

	Test(success);

	Test(nodes.size() == 14951);
	Test(edges.size() == 54903);

	Test(nodes.front().id == 0);
	Test(nodes.front().osm_id == 27393353);
	Test(nodes.front().coordinate.lat == 48.9535070);
	Test(nodes.front().coordinate.lon == 8.8727831);
	Test(nodes.front().elevation == 285);
	Test(nodes.front().level == 0);
	Test(nodes.back().osm_id == 486562412);

	Test(edges.back().id == (EdgeID)edges.size() - 1);
	Test(edges.back().source == 14949);
	Test(edges.back().target == 5575);
	Test(edges.back().length == 60);
	Test(edges.back().type == 12);
	Test(edges.back().speed == 30);
	Test(edges.back().child_edge1 == -1);
	Test(edges.back().child_edge2 == -1);

	printTestFooter("Parser");
}

void unit_tests::testGraph()
{
	printTestHeader("Graph");

	Graph graph;
	graph.init("../../data/test.graph", false);

	Test(graph.getNumberOfNodes() == 14951);
	Test(graph.getNumberOfEdges() == 54903);

	std::size_t count = 0;
	for (auto const& edge : graph.getOutEdgesOf(0)) {
		Test(edge.target == 6588 || edge.target == 11947);
		++count;
	}
	Test(count == 2);

	count = 0;
	for (auto const& edge : graph.getOutEdgesOf(14873).reverse()) {
		Test(edge.target == 14871 || edge.target == 14872 ||
		     edge.target == 14875);
		++count;
	}
	Test(count == 3);

	count = 0;
	for (auto const& edge : graph.getInEdgesOf(14932)) {
		Test(edge.source == 14930 || edge.source == 14933 ||
		     edge.source == 14934 || edge.source == 14935 ||
		     edge.source == 14937);
		Test(edge.source == edge.getHead(Direction::Backward));
		++count;
	}
	Test(count == 5);

	Test(graph.getNode(42).osm_id == 28287688);
	Test(graph.getEdge(19).target == 11072);

	Test(graph.getNumberOfOutEdges(14944) == 4);
	Test(graph.getNumberOfOutEdges(14948) == 4);

	Test(graph.getNumberOfInEdges(14944) == 4);

	//
	// Check if the edges reported by getParents are correct
	//

	for (std::size_t i = 0; i < 1000; ++i) {
		auto edge_id = random.getEdge(graph);
		auto function_parents = graph.getParents(edge_id);

		EdgeIDs real_parents;
		for (auto const& edge : graph.getAllEdges()) {
			if (edge.child_edge1 == edge_id || edge.child_edge2 == edge_id) {
				real_parents.push_back(edge.id);
			}
		}

		std::sort(function_parents.begin(), function_parents.end());
		std::sort(real_parents.begin(), real_parents.end());

		Test(function_parents == real_parents);
	}

	//
	// Check up an down edge iterators
	//

	for (std::size_t i = 0; i < 1000; ++i) {
		auto node_id = random.getNode(graph);

		for (auto direction : {Direction::Forward, Direction::Backward}) {
			std::size_t edge_count = 0;

			for (auto const& edge : graph.getUpEdgesOf(node_id, direction)) {
				Test(edge.getTail(direction) == node_id);
				++edge_count;
			}

			for (auto const& edge : graph.getDownEdgesOf(node_id, direction)) {
				Test(edge.getTail(direction) == node_id);
				++edge_count;
			}

			Test(graph.getNumberOfEdges(node_id, direction) == edge_count);
		}
	}

	//
	// Check for edges that are not shortest paths
	//

	std::size_t edge_count = 0;
	std::size_t shortcut_count = 0;

	CHDijkstra dijkstra(graph);

	for (auto const& edge : graph.out_edges) {
		dijkstra.run(edge.source, edge.target);
		auto const& result = dijkstra.getResult();

		if (result.path_length != edge.length) {
			if (edge.isShortcut()) {
				++shortcut_count;
			} else {
				++edge_count;
			}
		}
	}

	Print("There are " << edge_count << " normal edges and " << shortcut_count
	                   << " shortcuts which are no shortest paths.");

	printTestFooter("Graph");
}

void unit_tests::testPaths()
{
	printTestHeader("Paths");

	Graph graph;
	graph.init("../../data/test.graph", false);

	// The edges 0 and 1 do not match so an exception should be thrown.
	try {
		Path path;
		path.push(graph, 0);
		path.push(graph, 1);
		Test(false);
	} catch (Exception const& e) {
		Test(true);
	}

	// The edges 8 and 13 match so no exception should be thrown.
	try {
		Path path;
		path.push(graph, 8);
		path.push(graph, 13);
		Test(true);
	} catch (Exception const& e) {
		Test(false);
	}

	Path path;
	path.push(graph, 8);
	path.push(graph, 13);
	path.push(graph, 16);
	Test(path.length() == 3);
	Test(path.getSource(graph) == 2);
	Test(path.getTarget(graph) == 11665);
	path.pop();
	Test(path.getEdges().size() == 2);
	for (auto edge_id : path.getEdges()) {
		Test(edge_id == 8 || edge_id == 13);
	}
	path.clear();
	Test(path.getEdges().size() == 0);

	Path time_path;
	time_path.push(graph, 8);
	time_path.setTimeIntervallForEdge(0, TimeIntervall(10001, 10002));
	time_path.push(graph, 13);
	time_path.setTimeIntervallForEdge(1, TimeIntervall(10002, 10004));

	Path compressed_time_path;
	compressed_time_path.push(graph, 9);
	compressed_time_path.setTimeIntervallForEdge(
	    0, TimeIntervall(10002, 10003));

	Test(graph.getEdge(9).child_edge1 == 8);
	Test(graph.getEdge(9).child_edge2 == 13);

	compressed_time_path.setTimeIntervallsForCompressed(graph, time_path);
	Test(compressed_time_path.getTimeIntervallForEdge(
	         compressed_time_path.getEdges().size() - 1) ==
	     TimeIntervall(10001, 10004));

	Test(compressed_time_path.isCompressedOf(graph, time_path));

	// Test different compressions of the same path
	Path compressed_path_1;
	compressed_path_1.push(graph, 8);
	compressed_path_1.push(graph, 13);
	compressed_path_1.push(graph, 18);

	Path compressed_path_2;
	compressed_path_2.push(graph, 9);
	compressed_path_2.push(graph, 14);
	compressed_path_2.push(graph, 11);

	Test(compressed_path_1.unpack(graph) == compressed_path_2.unpack(graph));

	Test(!compressed_path_1.isCompressedOf(graph, compressed_path_2));
	Test(!compressed_path_2.isCompressedOf(graph, compressed_path_1));

	printTestFooter("Paths");
}

void unit_tests::testPathsDS()
{
	printTestHeader("PathsDS");

	Graph graph;
	graph.init("../../data/test.graph", false);

	Path path;
	path.push(graph, 8);
	path.push(graph, 13);
	path.push(graph, 16);

	Path time_path;
	time_path.push(graph, 8);
	time_path.setTimeIntervallForEdge(0, TimeIntervall(10001, 10002));
	time_path.push(graph, 13);
	time_path.setTimeIntervallForEdge(1, TimeIntervall(10002, 10004));

	PathsDS path_ds;
	path_ds.addPath(path);
	path_ds.addPath(time_path);

	Path retrieved_path = path_ds.getPath(0);
	Test(path.length() == retrieved_path.length());
	for (EdgeIndex edge_index = 0; edge_index < path.length(); edge_index++) {
		Test(path.getEdge(edge_index) == retrieved_path.getEdge(edge_index));
		Test(path.getTimeAtNodes().at(edge_index) ==
		     retrieved_path.getTimeAtNodes().at(edge_index));
	}
	Test(path.getEndTime() == retrieved_path.getEndTime());

	Path retrieved_time_path = path_ds.getPath(1);
	Test(time_path.length() == retrieved_time_path.length());
	for (EdgeIndex edge_index = 0; edge_index < time_path.length();
	     edge_index++) {
		Test(time_path.getEdge(edge_index) ==
		     retrieved_time_path.getEdge(edge_index));
		Test(time_path.getTimeAtNodes().at(edge_index) ==
		     retrieved_time_path.getTimeAtNodes().at(edge_index));
	}
	Test(time_path.getEndTime() == retrieved_time_path.getEndTime());

	printTestFooter("PathsDS");
}

void unit_tests::testPathParser()
{
	printTestHeader("PathParser");

	Graph graph;
	graph.init("../../data/test.graph");

	PathsDS paths_ds;
	PathParser path_parser(graph, paths_ds);
	bool success = path_parser.parse("../../data/test_paths.json");

	Test(success);

	Test(paths_ds.getNrOfPaths() == 2);

	std::vector<Path> paths;
	for (PathID path_id = 0; path_id < paths_ds.getNrOfPaths(); path_id++) {
		paths.push_back(paths_ds.getPath(path_id));
	}

	Test(paths[0].getEdges().size() == 3);
	Test(paths[1].getEdges().size() == 1);

	Test(paths[0].getTimeIntervallForEdge(0).min_time == 1000000000);
	Test(paths[0].getTimeIntervallForEdge(0).max_time == 1000000003);
	Test(paths[0].getTimeIntervallForEdge(1).min_time == 1000000003);
	Test(paths[0].getTimeIntervallForEdge(1).max_time == 1000000006);
	Test(paths[0].getTimeIntervallForEdge(2).min_time == 1000000006);

	Test(paths[1].getTimeIntervallForEdge(0).min_time == 1000000000);
	Test(paths[1].getTimeIntervallForEdge(0).max_time == 1000000005);

	paths.clear();

	printTestFooter("PathParser");
}

void unit_tests::testPathGenerator()
{
	printTestHeader("PathGenerator");

	Graph graph;
	graph.init("../../data/test.graph");

	PathGenerator path_generator(graph, random, nr_of_threads);
	for (std::size_t i = 0; i < number_of_paths; ++i) {
		auto path_types = path_generator.getRandomPathTypes(10);
		auto path = path_generator.getStitchedPath(path_types);
	}

	printTestFooter("PathGenerator");
}

void unit_tests::testRandom()
{
	printTestHeader("Random");

	Graph graph;
	graph.init("../../data/test.graph");

	for (std::size_t i = 0; i < 20000; ++i) {
		auto node_id = random.getNode(graph);
		Test(node_id >= 0 && node_id < (NodeID)graph.getNumberOfNodes());

		auto edge_id = random.getEdge(graph);
		Test(edge_id >= 0 && edge_id < (EdgeID)graph.getNumberOfEdges());

		auto out_edge_id = random.getNonShortcutOutgoingEdge(graph, node_id);
		bool found = false;
		for (auto const& edge_id : graph.getNonShortcutOutEdgesOf(node_id)) {
			if (edge_id == out_edge_id) {
				found = true;
			}
		}

		Test(found ||
		     (!found && graph.getNonShortcutOutEdgesOf(node_id).size() == 0 &&
		         out_edge_id == c::NO_EID));

		int rand_int = random.getInt(23, 42);
		Test(rand_int >= 23 && rand_int < 42);

		float rand_float = random.getFloat(1.234, 5.6789);
		Test(rand_float >= 1.234 && rand_float <= 5.6789);
	}

	printTestFooter("Random");
}

void unit_tests::testEdgeRange()
{
	printTestHeader("EdgeRange");

	Graph graph;
	graph.init("../../data/test.graph");

	for (std::size_t i = 0; i < 1000; ++i) {
		auto node_id = random.getNode(graph);

		std::vector<EdgeID> ids;

		for (auto const& edge : graph.getOutEdgesOf(node_id)) {
			ids.push_back(edge.id);
		}

		for (auto const& edge : graph.getOutEdgesOf(node_id).reverse()) {
			ids.push_back(edge.id);
		}

		for (std::size_t i = 0; i < ids.size(); ++i) {
			Test(ids[i] == ids[ids.size() - i - 1]);
		}
	}

	printTestFooter("EdgeRange");
}

void unit_tests::testDijkstra()
{
	printTestHeader("Dijkstra");

	Graph graph;
	graph.init("../../data/test.graph");

	Dijkstra dijkstra(graph);
	CHDijkstra ch_dijkstra(graph);

	for (std::size_t i = 0; i < 2000; ++i) {
		auto source = random.getNode(graph);
		auto target = random.getNode(graph);

		dijkstra.run(source, target);
		ch_dijkstra.run(source, target);

		auto const& result = dijkstra.getResult();
		auto const& ch_result = ch_dijkstra.getResult();

		Test(result.path_found == ch_result.path_found);

		if (!result.path_found) {
			continue;
		}

		Length path_length = 0;
		for (EdgeID edge_id : result.path.getEdges()) {
			path_length += graph.getEdge(edge_id).length;
		}
		Test(result.path_length == path_length);

		Test(result.path_length == ch_result.path_length);
	}

	printTestFooter("Dijkstra");
}

void unit_tests::testShortestPathPartitioner()
{
	printTestHeader("ShortestPathPartitioner");

	Graph graph;
	graph.init("../../data/test.graph");

	auto is_valid = [&](Path const& path1, Path const& path2) {
		Test(path1.getSource(graph) == path1.getSource(graph));
		Test(path2.getTarget(graph) == path2.getTarget(graph));

		Length length_path1 = 0;
		for (EdgeID edge_id : path1.getEdges()) {
			length_path1 += graph.getEdge(edge_id).length;
		}

		Length length_path2 = 0;
		for (EdgeID edge_id : path2.getEdges()) {
			length_path2 += graph.getEdge(edge_id).length;
		}

		Test(length_path1 == length_path1);
		Test(path1.unpack(graph) == path2.unpack(graph));
	};

	PathGenerator path_generator(graph, random, nr_of_threads);

	std::unique_ptr<ShortestPathPartitioner> incremental_partitioner =
	    partitioner_util::createPartitioner(
	        graph, PartitionerType::INCREMENTAL);

	for (std::size_t i = 0; i < number_of_paths; ++i) {
		auto path_types = PathGenerator::PathTypes(
		    10, PathGenerator::PathType::RandomShortest);
		auto path = path_generator.getStitchedPath(path_types);

		auto incremental_path = incremental_partitioner->run(path);

		is_valid(path, incremental_path);
	}

	printTestFooter("ShortestPathPartitioner");
}

void unit_tests::testNodeRange()
{
	printTestHeader("NodeRange");

	Graph graph;
	PathGenerator path_generator(graph, random, nr_of_threads);

	graph.init("../../data/test.graph");

	std::vector<NodeID> node_range_nodes;
	for (std::size_t i = 0; i < number_of_paths; ++i) {
		// Produce a random path
		auto path = path_generator.getRandomPath();

		if (path.length() == 0) {
			continue;
		}

		//
		// NodeRange over full path
		//

		// Get the nodes via a NodeRange loop
		node_range_nodes.clear();
		for (auto node_id : path.getNodeRange(graph)) {
			node_range_nodes.push_back(node_id);
		}

		Test(node_range_nodes.size() == path.length() + 1);

		// Compare with the actual nodes
		std::size_t j = 0;
		Test(node_range_nodes[j++] == path.getSource(graph));
		for (EdgeID edge_id : path.getEdges()) {
			auto const& edge = graph.getEdge(edge_id);
			Test(edge.target == node_range_nodes[j++]);
		}

		//
		// NodeRange over part of the path
		//

		std::size_t begin = random.getInt(0, path.length());
		std::size_t end = random.getInt(begin, path.length());

		// Get the nodes via a NodeRange loop
		node_range_nodes.clear();
		for (auto node_id : path.getNodeRange(graph, begin, end)) {
			node_range_nodes.push_back(node_id);
		}

		// Compare with the actual nodes
		std::size_t node_index = 0;
		for (std::size_t edge_index = begin; edge_index <= end; ++edge_index) {
			if (edge_index == path.length()) {
				Test(node_range_nodes[node_index] == path.getTarget(graph));
			} else {
				auto const& edges = path.getEdges();
				auto const& edge = graph.getEdge(edges[edge_index]);
				Test(node_range_nodes[node_index] == edge.source);
			}

			++node_index;
		}

		//
		// ReverseNodeRange over full path
		//

		// Get the nodes via a ReverseNodeRange loop
		node_range_nodes.clear();
		for (auto node_id : path.getNodeRange(graph).reverse(graph)) {
			node_range_nodes.push_back(node_id);
		}

		Test(node_range_nodes.size() == path.length() + 1);

		// Compare with the actual nodes
		j = path.length();
		Test(node_range_nodes[j--] == path.getSource(graph));
		for (EdgeID edge_id : path.getEdges()) {
			auto const& edge = graph.getEdge(edge_id);
			Test(edge.target == node_range_nodes[j--]);
		}

		//
		// ReverseNodeRange over part of the path
		//

		begin = random.getInt(0, path.length());
		end = random.getInt(begin, path.length());

		// Get the nodes via a ReverseNodeRange loop
		node_range_nodes.clear();
		for (auto node_id :
		    path.getNodeRange(graph, begin, end).reverse(graph)) {
			node_range_nodes.push_back(node_id);
		}

		// Compare with the actual nodes
		node_index = node_range_nodes.size() - 1;
		for (std::size_t edge_index = begin; edge_index <= end; ++edge_index) {
			if (edge_index == path.length()) {
				Test(node_range_nodes[node_index] == path.getTarget(graph));
			} else {
				auto const& edges = path.getEdges();
				auto const& edge = graph.getEdge(edges[edge_index]);
				Test(node_range_nodes[node_index] == edge.source);
			}

			--node_index;
		}
	}

	printTestFooter("NodeRange");
}

void unit_tests::testPathfinderDS()
{
	printTestHeader("PathfinderDS");

	Graph graph;
	graph.init("../../data/test.graph");

	PathGenerator path_generator(graph, random, nr_of_threads);
	PathsDS paths_ds = path_generator.getRandomPaths(number_of_paths);

	PathsDS compressed_paths_ds =
	    partitioner_util::partitionPaths(graph, paths_ds);

	PathfinderDS pathfinder_ds(
	    graph, nr_of_threads, std::move(compressed_paths_ds));

	Print("Test getEdgeBox");
	for (std::size_t i = 0; i < number_of_simple_tests; ++i) {
		auto random_edge_id = random.getEdge(graph);
		auto path = graph.unpack(random_edge_id);

		auto const& ds_box = pathfinder_ds.getEdgeBox(random_edge_id);
		BoundingBox path_box(graph, path);

		Test(ds_box == path_box);
	}

	Print("Test getRNodeBox");
	for (std::size_t i = 0; i < number_of_simple_tests; ++i) {
		auto random_node_id = random.getNode(graph);

		auto const& ds_box = pathfinder_ds.getRNodeBox(random_node_id);

		// set initial value in case random_node doesn't have down edges
		auto const& random_node = graph.getNode(random_node_id);
		BoundingBox manual_box(random_node.coordinate);
		// push down
		auto push_down_function = [&](Edge const& edge, Direction direction) {
			Unused(direction);
			if (pathfinder_ds.is_obsolete[edge.id]) {
				return CHTraversal::Result::Ignore;
			}
			manual_box.expand(pathfinder_ds.getEdgeBox(edge.id));

			return CHTraversal::Result::Expand;
		};
		pathfinder_ds.traverse().down(
		    std::move(push_down_function), random_node_id);

		Test(ds_box == manual_box);
	}

	Print("Build temporary path vector");
	std::vector<Path> paths;
	for (PathID path_id = 0;
	     path_id < pathfinder_ds.getPathsDS().getNrOfPaths(); path_id++) {
		Path path = pathfinder_ds.getPath(path_id);
		paths.push_back(path);
	}

	Print("Test isContainedInPath");
	for (std::size_t i = 0; i < number_of_simple_tests; ++i) {
		auto random_edge_id = random.getEdge(graph);

		bool is_contained = false;
		for (Path const& path : paths) {
			for (EdgeID edge_id : path.getEdges()) {
				if (random_edge_id == edge_id) {
					is_contained = true;
				}
			}
		}

		Test(is_contained == pathfinder_ds.isContainedInPath(random_edge_id));
	}

	Print("Test getPathIDs");
	for (std::size_t i = 0; i < number_of_simple_tests; ++i) {
		auto random_edge_id = random.getEdge(graph);

		PathIntervalls manual_path_intervalls;

		for (PathID path_id = 0; path_id < (PathID)paths.size(); ++path_id) {
			Path const& path = paths[path_id];
			for (EdgeIndex edge_index = 0; edge_index < path.getEdges().size();
			     edge_index++) {
				if (random_edge_id == path.getEdge(edge_index)) {
					PathIntervall path_intervall(
					    path_id, path.getTimeIntervallForEdge(edge_index));
					manual_path_intervalls.push_back(path_intervall);
				}
			}
		}

		auto ds_path_intervalls =
		    pathfinder_ds.getPathIntervalls(random_edge_id);

		std::sort(manual_path_intervalls.begin(), manual_path_intervalls.end());
		std::sort(ds_path_intervalls.begin(), ds_path_intervalls.end());
		Test(ds_path_intervalls == manual_path_intervalls);
	}

	Print("Test initTopNodes");
	std::vector<bool> seen_edges(graph.getNumberOfEdges(), false);
	std::vector<bool> seen_nodes(graph.getNumberOfNodes(), false);

	auto mark_edges = [&](Edge const& edge, Direction direction) {
		if (pathfinder_ds.is_obsolete[edge.id]) {
			return CHTraversal::Result::Ignore;
		}
		seen_edges[edge.id] = true;
		Test(seen_nodes[edge.getTail(direction)]);
		seen_nodes[edge.getHead(direction)] = true;
		return CHTraversal::Result::Expand;
	};

	BoundingBox all_bounding_box({-90., -180.}, {90., 180.});
	auto const& top_nodes = pathfinder_ds.getTopNodes(all_bounding_box);
	for (auto node_id : top_nodes) {
		seen_nodes[node_id] = true;
		pathfinder_ds.traverse().down(mark_edges, node_id);
	}
	for (EdgeID edge_id = 0; edge_id < (EdgeID)seen_edges.size(); ++edge_id) {
		if (pathfinder_ds.is_obsolete[edge_id]) {
			Test(!seen_edges[edge_id]);
		} else {
			Test(seen_edges[edge_id]);
		}
	}
	for (auto seen : seen_nodes) {
		Test(seen);
	}

	printTestFooter("PathfinderDS");
}

void unit_tests::testCHTraversal()
{
	printTestHeader("CHTraversal");

	Graph graph;
	graph.init("../../data/test.graph");

	CHTraversal ch_traversal(graph);

	std::size_t edge_count;

	// test up
	edge_count = 0;
	auto push_up_function = [&](Edge const& edge, Direction direction) {
		++edge_count;
		Test(graph.isUpEdge(edge.id, direction));
	};
	ch_traversal.up(std::move(push_up_function));
	Test(edge_count == graph.getNumberOfEdges());

	// TODO: test up with a node_id

	// test down
	edge_count = 0;
	auto push_down_function = [&](Edge const& edge, Direction direction) {
		++edge_count;
		Test(graph.isDownEdge(edge.id, direction));
	};
	ch_traversal.down(std::move(push_down_function));
	Test(edge_count == graph.getNumberOfEdges());

	// TODO: test down with a node_id

	// test toParents
	edge_count = 0;
	auto push_to_parents_function = [&](EdgeID parent_id, EdgeID edge_id) {
		++edge_count;

		auto parents = graph.getParents(edge_id);
		auto it = std::find(parents.begin(), parents.end(), parent_id);
		Test(it != parents.end());
	};
	ch_traversal.toParents(std::move(push_to_parents_function));

	std::size_t number_of_children = 0;
	for (auto const& edge : graph.getAllEdges()) {
		if (edge.isShortcut()) {
			number_of_children += 2;
		}
	}

	Test(edge_count == number_of_children);

	printTestFooter("CHTraversal");
}

void unit_tests::checkTimeIntervallDifference(
    Pathfinder& pathfinder, Graph const& graph)
{
	Print("Issue up to "
	      << max_number_of_queries_to_find_example
	      << " queries with and without time intervall constraint to find an "
	         "example where it makes a difference.");
	bool found_example = false;
	std::size_t i = 0;
	while (!found_example && i < max_number_of_queries_to_find_example) {
		auto random_box = random.getBox(graph);
		auto random_intervall = random.getIntervall();
		auto path_ids = pathfinder.run(
		    Query(random_box, random_intervall, TimeSlots(true)));
		auto path_ids_ignoring_intervall = pathfinder.run(Query(random_box));

		std::sort(path_ids.begin(), path_ids.end());
		std::sort(path_ids_ignoring_intervall.begin(),
		    path_ids_ignoring_intervall.end());

		if (!set_util::checkEquality(
		        path_ids, path_ids_ignoring_intervall, false)) {
			found_example = true;
		}
		++i;
	}

	if (found_example) {
		Print("It took " << i << " queries to find an example.");
	} else {
		Print("Attention: It was not possible with "
		      << max_number_of_queries_to_find_example
		      << " queries to find an example. As this is extremely unlikely, "
		         "this strongly indicates an error. If you think the code is "
		         "alright, try it with more queries.");
		Test(false);
	}
}

void unit_tests::checkTimeSlotDifference(
    Pathfinder& pathfinder, Graph const& graph)
{
	Print("Issue up to " << max_number_of_queries_to_find_example
	                     << " queries with and without time slot constraint to "
	                        "find an example where it makes a difference.");
	bool found_example = false;
	std::size_t i = 0;
	while (!found_example && i < max_number_of_queries_to_find_example) {

		auto random_box = random.getBox(graph);
		auto random_slots = random.getTimeSlots();
		auto path_ids =
		    pathfinder.run(Query(random_box, c::FULL_INTERVALL, random_slots));
		auto path_ids_ignoring_slots = pathfinder.run(
		    Query(random_box, c::FULL_INTERVALL, TimeSlots(true)));

		std::sort(path_ids.begin(), path_ids.end());
		std::sort(
		    path_ids_ignoring_slots.begin(), path_ids_ignoring_slots.end());

		if (!set_util::checkEquality(
		        path_ids, path_ids_ignoring_slots, false)) {
			found_example = true;
		}

		++i;
	}

	if (found_example) {
		Print("It took " << i << " queries to find an example.");
	} else {
		Print("Attention: It was not possible with "
		      << max_number_of_queries_to_find_example
		      << " queries to find an example. As this is extremely unlikely, "
		         "this strongly indicates an error. If you think the code is "
		         "alright, try it with more queries.");
		Test(false);
	}
}

void unit_tests::testEdgeCandidateFinder()
{
	printTestHeader("EdgeCandidateFinder");

	testEdgeCandidateFinder(true);
	testEdgeCandidateFinder(false);

	printTestFooter("EdgeCandidateFinder");
}

void unit_tests::testEdgeCandidateFinder(bool sortWRTEdgeStatus)
{
	Print("Test sortedWRTEdgeStatus: " << sortWRTEdgeStatus);

	Graph graph;
	graph.init("../../data/test.graph");

	PathGenerator path_generator(graph, random, nr_of_threads);

	PathsDS paths_ds = path_generator.getRandomShortestPaths(
	    number_of_paths, max_path_distance);

	PathsDS compressed_paths_ds =
	    partitioner_util::partitionPaths(graph, paths_ds);

	Print("Init pathfinder data structures");
	PathfinderDS pathfinder_ds(graph, nr_of_threads,
	    std::move(compressed_paths_ds), InvertedIndicesVariant::Vector,
	    sortWRTEdgeStatus);

	EdgeIDs edge_ids;
	EdgeCandidateFinder edge_candidate_finder(pathfinder_ds, nr_of_threads);

	// test if output of find edge candidates is correct
	Print("Check findEdgeCandidates " << number_of_queries << " times");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto top_nodes = pathfinder_ds.getTopNodes(random_box);
		edge_candidate_finder.findEdgeCandidates(
		    Query(random_box), top_nodes, edge_ids);

		EdgeIDs edge_ids_naive;
		for (auto const& edge : graph.getAllEdges()) {
			if (!pathfinder_ds.isContainedInPath(edge.id)) {
				continue;
			}
			auto edge_box = pathfinder_ds.getEdgeBox(edge.id);
			edge_box.intersect(random_box);

			if (edge_box.isValid()) {
				edge_ids_naive.push_back(edge.id);
			}
		}

		std::sort(edge_ids.begin(), edge_ids.end());
		std::sort(edge_ids_naive.begin(), edge_ids_naive.end());

		Test(set_util::checkEquality(edge_ids, edge_ids_naive));
	}
	Print("");
}

void unit_tests::testPathfinder(uint nr_of_threads_to_test)
{
	printTestHeader("Pathfinder");

	Graph graph;
	graph.init("../../data/test.graph");

	PathGenerator path_generator(graph, random, nr_of_threads_to_test);
	// only works with already partitioned random paths
	PathsDS paths_ds = path_generator.getRandomShortestPaths(
	    number_of_paths, max_path_distance);

	PathsDS compressed_paths_ds =
	    partitioner_util::partitionPaths(graph, paths_ds);

	Print("Init pathfinder data structures");
	PathfinderDS pathfinder_ds(
	    graph, nr_of_threads_to_test, std::move(compressed_paths_ds));

	Pathfinder pathfinder(pathfinder_ds, nr_of_threads_to_test);
	NaivePathfinder naive_pathfinder(pathfinder_ds);
	NaiveCHPathfinder naive_ch_pathfinder(pathfinder_ds, nr_of_threads_to_test);

	Print("Issue " << number_of_queries << " queries");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto path_ids = pathfinder.run(Query(random_box));
		auto path_ids_naive = naive_pathfinder.run(Query(random_box));

		std::sort(path_ids.begin(), path_ids.end());
		std::sort(path_ids_naive.begin(), path_ids_naive.end());

		Test(set_util::checkEquality(path_ids, path_ids_naive));
	}

	Print("Issue " << number_of_queries
	               << " queries with time intervall constraint");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto random_intervall = random.getIntervall();
		auto path_ids = pathfinder.run(
		    Query(random_box, random_intervall, TimeSlots(true)));
		auto path_ids_naive = naive_pathfinder.run(
		    Query(random_box, random_intervall, TimeSlots(true)));

		std::sort(path_ids.begin(), path_ids.end());
		std::sort(path_ids_naive.begin(), path_ids_naive.end());

		Test(set_util::checkEquality(path_ids, path_ids_naive));
	}

	checkTimeIntervallDifference(pathfinder, graph);

	Print(
	    "Issue " << number_of_queries << " queries with time slot constraint");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto random_slots = random.getTimeSlots();
		auto path_ids =
		    pathfinder.run(Query(random_box, c::FULL_INTERVALL, random_slots));
		auto path_ids_naive = naive_pathfinder.run(
		    Query(random_box, c::FULL_INTERVALL, random_slots));

		std::sort(path_ids.begin(), path_ids.end());
		std::sort(path_ids_naive.begin(), path_ids_naive.end());

		Test(set_util::checkEquality(path_ids, path_ids_naive));
	}
	checkTimeSlotDifference(pathfinder, graph);

	Print("Issue " << number_of_queries << " queries with naive CH pathfinder");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto path_ids = naive_ch_pathfinder.run(Query(random_box));
		auto path_ids_naive = naive_pathfinder.run(Query(random_box));

		std::sort(path_ids.begin(), path_ids.end());
		std::sort(path_ids_naive.begin(), path_ids_naive.end());

		Test(set_util::checkEquality(path_ids, path_ids_naive));
	}

	Print("Test variants");

	printTestFooter("Pathfinder");
}

void unit_tests::checkConservativeFilterDifference(
    Pathfinder& uncompressed_pathfinder, Pathfinder& compressed_pathfinder,
    Graph const& graph)
{
	Print("Issue up to " << max_number_of_queries_to_find_example
	                     << " queries to find an example where time loss makes "
	                        "a difference in precision.");
	bool found_example = false;
	std::size_t i = 0;
	while (!found_example && i < max_number_of_queries_to_find_example) {

		auto random_box = random.getBox(graph);
		auto random_intervall = random.getIntervall();
		auto random_slots = random.getTimeSlots();
		auto query = Query(random_box, random_intervall, random_slots);

		auto uncompressed_path_ids = uncompressed_pathfinder.run(query);
		auto compressed_path_ids = compressed_pathfinder.run(query);

		std::sort(uncompressed_path_ids.begin(), uncompressed_path_ids.end());
		std::sort(compressed_path_ids.begin(), compressed_path_ids.end());

		// as we lose some time information, sometimes more is reported
		Test(set_util::checkSubset(uncompressed_path_ids, compressed_path_ids));

		if (!set_util::checkEquality(
		        uncompressed_path_ids, compressed_path_ids, false)) {
			found_example = true;
		}

		++i;
	}

	if (found_example) {
		Print("It took " << i << " queries to find an example.");
	} else {
		Print("Attention: It was not possible with "
		      << max_number_of_queries_to_find_example
		      << " queries to find an example. As this is extremely unlikely, "
		         "this strongly indicates an error. If you think the code is "
		         "alright, try it with more queries.");
		Test(false);
	}
}

void unit_tests::testConservativeFilter()
{
	printTestHeader("ConservativeFilter");

	testConservativeFilterGenerated();
	testConservativeFilterReal();

	printTestFooter("ConservativeFilter");
}

void unit_tests::testConservativeFilterGenerated()
{
	Print("Test with generated data");
	Graph graph;
	graph.init("../../data/test.graph");

	PathGenerator path_generator(graph, random, nr_of_threads);

	PathsDS paths_ds = path_generator.getRandomPaths(number_of_paths);

	testConservativeFilter(graph, std::move(paths_ds));
	Print("");
}

void unit_tests::testConservativeFilterReal()
{
	Print("Test with real data");
	Graph graph;
	graph.init("../../data/saarland-180101-maxspeed-distance-dm-all-ch.ftxt");

	PathsDS paths_ds;
	PathParser path_parser(graph, paths_ds);
	path_parser.parse("../../data/paths.0_2147483647.json");

	testConservativeFilter(graph, std::move(paths_ds));
	Print("");
}

void unit_tests::testConservativeFilter(Graph& graph, PathsDS&& paths_ds)
{
	PathsDS compressed_paths_ds =
	    partitioner_util::partitionPaths(graph, paths_ds);

	Print("Init pathfinder data structures");
	PathfinderDS compressed_pathfinder_ds(graph, nr_of_threads,
	    std::move(compressed_paths_ds), InvertedIndicesVariant::Vector, false);
	PathfinderDS uncompressed_pathfinder_ds(graph, nr_of_threads,
	    std::move(paths_ds), InvertedIndicesVariant::Vector, false);

	Pathfinder uncompressed_pathfinder(
	    uncompressed_pathfinder_ds, nr_of_threads);
	Pathfinder compressed_pathfinder(compressed_pathfinder_ds, nr_of_threads);

	Print("Issue " << number_of_queries << " queries");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto random_intervall = random.getIntervall();
		auto random_slots = random.getTimeSlots();
		auto query = Query(random_box, random_intervall, random_slots);

		auto uncompressed_path_ids = uncompressed_pathfinder.run(query);
		auto compressed_path_ids = compressed_pathfinder.run(query);

		std::sort(uncompressed_path_ids.begin(), uncompressed_path_ids.end());
		std::sort(compressed_path_ids.begin(), compressed_path_ids.end());

		// as we lose some time information, sometimes more is reported
		Test(set_util::checkSubset(uncompressed_path_ids, compressed_path_ids));
	}

	checkConservativeFilterDifference(
	    uncompressed_pathfinder, compressed_pathfinder, graph);
}

void unit_tests::testIndexVariants()
{
	printTestHeader("IndexVariants");

	Graph graph;
	graph.init("../../data/test.graph");

	PathGenerator path_generator(graph, random, nr_of_threads);
	// only works with already partitioned random paths
	PathsDS paths_ds = path_generator.getRandomShortestPaths(
	    number_of_paths, max_path_distance);

	std::vector<InvertedIndicesVariant> inverted_index_variants_to_test;
	inverted_index_variants_to_test.push_back(
	    InvertedIndicesVariant::IntervallTree);
	inverted_index_variants_to_test.push_back(InvertedIndicesVariant::Vector);
	inverted_index_variants_to_test.push_back(
	    InvertedIndicesVariant::VectorOnDisk);

	std::list<Graph> graph_for_variant;
	std::list<Pathfinder> pathfinder_for_variant;
	// this exists only that pathfinder_ds stays in scope
	std::list<PathfinderDS> pathfinder_ds_for_variant;

	for (InvertedIndicesVariant inverted_index_variant :
	    inverted_index_variants_to_test) {
		Print("Build inverted index variant:");
		Print(invertedIndexVariantToString(inverted_index_variant));
		Print("Init pathfinder data structures");
		// graph gets modified, thats why we need a copy
		graph_for_variant.push_back(Graph(graph));
		pathfinder_ds_for_variant.push_back(
		    PathfinderDS(graph_for_variant.back(), nr_of_threads,
		        PathsDS(paths_ds), inverted_index_variant));
		Pathfinder pathfinder(pathfinder_ds_for_variant.back(), nr_of_threads);
		pathfinder_for_variant.push_back(pathfinder);
		Print("");
	}

	Print("Issue " << number_of_queries
	               << " queries with time intervall constraint");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		auto random_box = random.getBox(graph);
		auto random_intervall = random.getIntervall();

		std::vector<PathIDs> path_ids_for_variant;
		for (Pathfinder& pathfinder : pathfinder_for_variant) {
			auto path_ids = pathfinder.run(
			    Query(random_box, random_intervall, TimeSlots(true)));
			std::sort(path_ids.begin(), path_ids.end());

			// check equality with previous results
			for (PathIDs const& path_ids_to_compare_to : path_ids_for_variant) {
				Test(set_util::checkEquality(path_ids, path_ids_to_compare_to));
			}

			path_ids_for_variant.push_back(path_ids);
		}
	}

	printTestFooter("IndexVariants");
}

void unit_tests::testEdgeSorting()
{
	printTestHeader("EdgeSorting");

	Graph graph;
	graph.init("../../data/test.graph");

	PathGenerator path_generator(graph, random, nr_of_threads);
	PathsDS paths_ds = path_generator.getRandomShortestPaths(
	    number_of_paths, max_path_distance);

	Print("Init pathfinder data structures");
	Graph graph_sorted(graph);
	PathfinderDS pathfinder_ds_sorted(graph_sorted, nr_of_threads,
	    PathsDS(paths_ds), InvertedIndicesVariant::Vector, true);

	Graph graph_unsorted(graph);
	PathfinderDS pathfinder_ds_unsorted(graph_unsorted, nr_of_threads,
	    PathsDS(paths_ds), InvertedIndicesVariant::Vector, false);

	Pathfinder pathfinder_sorted(pathfinder_ds_sorted, nr_of_threads);
	NaiveCHPathfinder pathfinder_unsorted(
	    pathfinder_ds_unsorted, nr_of_threads);

	Print("Issue " << number_of_queries
	               << " queries with time intervall constraint");
	for (std::size_t i = 0; i < number_of_queries; ++i) {
		BoundingBox random_box = random.getBox(graph);
		TimeIntervall random_intervall = random.getIntervall();

		PathIDs path_ids_sorted = pathfinder_sorted.run(
		    Query(random_box, random_intervall, TimeSlots(true)));
		PathIDs path_ids_unsorted = pathfinder_unsorted.run(
		    Query(random_box, random_intervall, TimeSlots(true)));
		std::sort(path_ids_sorted.begin(), path_ids_sorted.end());
		std::sort(path_ids_unsorted.begin(), path_ids_unsorted.end());

		Test(set_util::checkEquality(path_ids_sorted, path_ids_unsorted));
	}

	printTestFooter("EdgeSorting");
}

void unit_tests::testSimpleFlags()
{
	printTestHeader("SimpleFlags");

	enum class TestFlags { Default, Value1, Value2 };
	std::size_t size = 10;

	SimpleFlags<TestFlags> flags(size, TestFlags::Default);
	std::vector<std::size_t> changed_flags;

	flags.set(0, TestFlags::Default, changed_flags);
	Test(changed_flags.empty());

	flags.set(3, TestFlags::Value2, changed_flags);
	flags.set(5, TestFlags::Value1, changed_flags);
	flags.set(7, TestFlags::Value1, changed_flags);
	Test(flags[3] == TestFlags::Value2);
	Test(flags[5] == TestFlags::Value1);
	Test(flags[7] == TestFlags::Value1);

	flags.set(7, TestFlags::Value2, changed_flags);
	Test(changed_flags.size() == 3);

	flags.reset(changed_flags);
	Test(changed_flags.empty());
	for (auto flag : flags.flags) {
		Test(flag == TestFlags::Default);
	}

	printTestFooter("SimpleFlags");
}

void unit_tests::testFlags()
{
	printTestHeader("Flags");

	enum class TestFlags { Default, Value1, Value2 };
	std::size_t size = 10;

	Flags<TestFlags> flags(size, TestFlags::Default);

	flags.set(0, TestFlags::Default);
	Test(flags.changed_flags.empty());

	flags.set(3, TestFlags::Value2);
	flags.set(5, TestFlags::Value1);
	flags.set(7, TestFlags::Value1);
	Test(flags[3] == TestFlags::Value2);
	Test(flags[5] == TestFlags::Value1);
	Test(flags[7] == TestFlags::Value1);

	flags.set(7, TestFlags::Value2);
	Test(flags.changed_flags.size() == 3);

	flags.reset();
	Test(flags.changed_flags.empty());
	for (auto flag : flags.flags) {
		Test(flag == TestFlags::Default);
	}

	printTestFooter("Flags");
}

void unit_tests::testExporter()
{
	printTestHeader("Exporter");

	Graph graph;
	graph.init("../../data/test.graph");

	PathGenerator path_generator(graph, random, nr_of_threads);
	PathsDS paths_ds = path_generator.getRandomPaths(number_of_paths);

	PathsDS compressed_paths_ds =
	    partitioner_util::partitionPaths(graph, paths_ds);

	Print("Init pathfinder data structures");
	PathfinderDS pathfinder_ds(
	    graph, nr_of_threads, std::move(compressed_paths_ds));
	Pathfinder pathfinder(pathfinder_ds, nr_of_threads);

	Print("Run pathfinder query");
	auto random_box = random.getBox(graph);
	auto path_ids = pathfinder.run(Query(random_box));

	Print("Export result");
	ExportData data;
	ExportColor blue{0, 0, 255, 255};
	ExportColor black{0, 0, 0, 255};

	data.filename = "exporter_testgraph.sg";
	data.boxes = {random_box};
	data.box_colors = {blue};
	for (auto path_id : path_ids) {
		Path unpacked =
		    pathfinder_ds.getPathsDS().getPath(path_id).unpack(graph);
		data.paths_ds.addPath(unpacked);
	}
	data.path_colors = ExportColors(data.paths_ds.getNrOfPaths(), black);

	Exporter exporter(graph);
	exporter.exportSimplestGraphRendererFile(data);

	printTestFooter("Exporter");
}

void unit_tests::testRTree()
{
	printTestHeader("RTree");

	RTree<int> r_tree;

	r_tree.emplace(BoundingBox({-2., 0.}, {-1., 1.}), 0);
	r_tree.emplace(BoundingBox({-1., 0.}, {0., 1.}), 1);
	r_tree.emplace(BoundingBox({0., 0.}, {1., 1.}), 2);
	r_tree.emplace(BoundingBox({1., 0.}, {2., 1.}), 3);
	r_tree.build();

	auto values = r_tree.search(BoundingBox({-.5, -.5}, {.5, .5}));

	Test(values.size() == 2);
	Test((values[0] == 1 && values[1] == 2) ||
	     (values[0] == 2 && values[1] == 1));

	printTestFooter("RTree");
}

void unit_tests::testAmbiguousShopas()
{
	printTestHeader("AmbiguousShopas");

	Graph graph;
	graph.init("../../data/test.graph", false);

	Path path1;
	path1.push(graph, 44888);
	path1.push(graph, 44890);
	path1.push(graph, 44888);
	path1.push(graph, 44891);

	Path path2;
	path2.push(graph, 44888);
	path2.push(graph, 44891);

	Test(graph.getEdge(44890).length == 0);
	Test(graph.getEdge(44888).length == 0);

	Test(path1.getLength(graph) == path2.getLength(graph));

	printTestFooter("AmbiguousShopas");
}

void unit_tests::testIntervalTree()
{
	printTestHeader("IntervalTree");

	// Test 1
	{
		IntervalTree<float, int> interval_tree;
		NodeIDs search_stack;

		interval_tree.emplace(-1, 1, 1);
		interval_tree.emplace(3, 4, 2);
		interval_tree.emplace(-1.5, -0.5, 3);
		interval_tree.emplace(2.5, 4.5, 4);
		interval_tree.emplace(-3, -2, 5);
		interval_tree.emplace(0, 2, 6);
		interval_tree.emplace(-3, 4, 7);

		interval_tree.build();

		auto result = interval_tree.search(-0.1, 0.1, search_stack);
		Test(result.size() == 3);
		std::sort(result.begin(), result.end());
		Test(result[0].value == 1);
		Test(result[1].value == 6);
		Test(result[2].value == 7);

		result = interval_tree.search(-2.5, -1.25, search_stack);
		std::sort(result.begin(), result.end());
		Test(result.size() == 3);
		Test(result[0].value == 3);
		Test(result[1].value == 5);
		Test(result[2].value == 7);

		result = interval_tree.search(4.25, 4.75, search_stack);
		std::sort(result.begin(), result.end());
		Test(result.size() == 1);
		Test(result[0].value == 4);
	}

	// Test 2
	{
		std::size_t number_of_intervals = 10000;

		IntervalTree<int, int> interval_tree;
		NodeIDs search_stack;

		TimeIntervalls time_intervals(number_of_intervals);
		for (std::size_t i = 0; i < number_of_intervals; ++i) {
			time_intervals[i] = random.getIntervall();
			interval_tree.emplace(
			    time_intervals[i].min_time, time_intervals[i].max_time, i);
		}

		interval_tree.build();

		for (std::size_t i = 0; i < number_of_queries; ++i) {
			auto query_interval = random.getIntervall();
			auto result_tree = interval_tree.search(
			    query_interval.min_time, query_interval.max_time, search_stack);

			std::vector<int> result_tree_ids;
			for (auto result : result_tree) {
				result_tree_ids.push_back(result.value);
			}

			std::sort(result_tree_ids.begin(), result_tree_ids.end());

			std::vector<int> result_naive;
			for (std::size_t j = 0; j < time_intervals.size(); ++j) {
				TimeIntervall result_interval = query_interval;
				result_interval.intersect(time_intervals[j]);
				if (result_interval.isValid()) {
					result_naive.push_back(j);
				}
			}

			Test(result_tree_ids == result_naive);
		}
	}

	printTestFooter("IntervalTree");
}

void unit_tests::testMergeSortedUnique()
{
	printTestHeader("MergeSortedUnique");

	std::vector<std::vector<int>> sorted_vectors;
	for (int i = 0; i < 10; i++) {
		std::vector<int> sorted_vector;
		for (int i = 0; i < 1000; i++) {
			sorted_vector.push_back(random.getInt(0, 100000));
		}
		std::sort(sorted_vector.begin(), sorted_vector.end());
		sorted_vectors.push_back(sorted_vector);
	}

	std::vector<int> naively_sorted =
	    merge_sorted_unique::naive(sorted_vectors);

	std::vector<int> in_place_sorted =
	    merge_sorted_unique::inplace(sorted_vectors);

	Test(naively_sorted.size() == in_place_sorted.size());
	for (uint i = 0; i < naively_sorted.size(); i++) {
		Test(naively_sorted.at(i) == in_place_sorted.at(i));
	}

	printTestFooter("MergeSortedUnique");
}

} // namespace pf

// just in case anyone does anything stupid with this file...
#undef Test
