#pragma once

#include <cstdint>
#include <limits>
#include <ostream>
#include <vector>

namespace pf
{

using NodeID = int32_t;
using OsmNodeId = uint64_t;
using EdgeID = int32_t;
using Length = int32_t;
using NodeIDs = std::vector<NodeID>;
using EdgeIDs = std::vector<EdgeID>;
using Lengths = std::vector<Length>;
using Level = int32_t;

using BBSize = double;
using Distance = int;
using TimeType = int;

namespace c
{
const NodeID NO_NID = -1;
const EdgeID NO_EID = -1;
const Length NO_LENGTH = std::numeric_limits<Length>::max();
const Level NO_LEVEL = -1;
constexpr TimeType INVALID_TIME = -1;
} // namespace c

// path analyzer measurements
// from saarland's matched paths
namespace pam
{
const TimeType reasonable_min_start_time = 1216536275;
const TimeType max_end_time = 1365320623;
const TimeType median_edge_time = 5;
} // namespace pam

enum class Direction : bool { Forward = 0, Backward = 1 };
Direction operator!(Direction direction);

enum class Dimension : bool { Lat = 0, Lon = 1 };

enum class PathDataSource { ReadInMatched, Generate, Import, None };

enum class QueryDataSource { Generate, Import, None };

enum class Substep {
	Get_top_nodes,
	Find_edge_candidates,
	Refine_candidates,
	Sort_edges,
	Get_path_ids,
	Remove_duplicate_path_ids,
	Reset_Collected
};

static const Substep allSubsteps[] = {Substep::Get_top_nodes,
    Substep::Find_edge_candidates, Substep::Refine_candidates,
    Substep::Sort_edges, Substep::Get_path_ids,
    Substep::Remove_duplicate_path_ids, Substep::Reset_Collected};

std::string getTextForSubstep(Substep substep);

enum class StxxlStatValue {
	Nof_bytes_read_from_disk, // in megabytes
	Read_time                 // in seconds
};

static const StxxlStatValue allStxxlStatValues[] = {
    StxxlStatValue::Nof_bytes_read_from_disk, StxxlStatValue::Read_time};

std::string getTextForStxxlStatValue(StxxlStatValue stxxlStatValue);

struct Coordinate {
	using FloatType = double;
	static constexpr FloatType invalid_value =
	    std::numeric_limits<FloatType>::lowest();

	FloatType lat;
	FloatType lon;

	Coordinate() : lat(invalid_value), lon(invalid_value)
	{
	}

	Coordinate(FloatType lat, FloatType lon) : lat(lat), lon(lon)
	{
	}

	bool operator==(Coordinate const& coordinate) const
	{
		return lat == coordinate.lat && lon == coordinate.lon;
	}

	bool isValid() const
	{
		return lat != invalid_value && lon != invalid_value;
	}

	void setInvalid()
	{
		lat = invalid_value;
		lon = invalid_value;
	}

	void minMerge(Coordinate const& other)
	{
		lat = std::min(lat, other.lat);
		lon = std::min(lon, other.lon);
	}

	void maxMerge(Coordinate const& other)
	{
		lat = std::max(lat, other.lat);
		lon = std::max(lon, other.lon);
	}
};

std::ostream& operator<<(std::ostream& stream, Coordinate const& coordinate);

struct Node {
	NodeID id;
	OsmNodeId osm_id;
	Coordinate coordinate;
	int elevation;
	Level level;
	double lowerTimeLimit = std::numeric_limits<double>::max();
	double upperTimeLimit = std::numeric_limits<double>::min();

	Node() : id(c::NO_NID)
	{
	}
};

std::ostream& operator<<(std::ostream& stream, Node const& node);

// TODO by splitting into data and routing edge we could probably speed-up
// everything significantly.
struct Edge {
	EdgeID id;
	NodeID source;
	NodeID target;
	Length length;
	int type;
	int speed;
	EdgeID child_edge1;
	EdgeID child_edge2;

	Edge() : id(c::NO_EID)
	{
	}

	NodeID getTail(Direction direction) const
	{
		return direction == Direction::Forward ? source : target;
	}

	NodeID getHead(Direction direction) const
	{
		return direction == Direction::Forward ? target : source;
	}

	bool isShortcut() const
	{
		return child_edge1 != c::NO_EID;
	}
};

std::ostream& operator<<(std::ostream& stream, Edge const& edge);

using Nodes = std::vector<Node>;
using Edges = std::vector<Edge>;

using TimeType = int;
static constexpr TimeType invalid_value = -1;

struct GeoMeasurement {
	Coordinate coordinate;
	TimeType timestamp;
};

using GeoMeasurements = std::vector<GeoMeasurement>;

struct OsmEdge {
	OsmNodeId src;
	OsmNodeId tgt;
};

struct Factor {

	double neStartPos;
	double neStopPos;
	int smplStart;
	int smplStop;

	std::vector<OsmEdge> resPath;
};

using Factors = std::vector<Factor>;

struct PathSizes {

	uint original_size;
	uint compressed_size;

	PathSizes(uint original_size, uint compressed_size)
	    : original_size(original_size), compressed_size(compressed_size)
	{
	}
};

std::ostream& operator<<(std::ostream& stream, PathSizes const& path_sizes);

using PathsSizes = std::vector<PathSizes>;

} // namespace pf
