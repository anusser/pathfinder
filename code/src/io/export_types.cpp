#include "export_types.h"

namespace pf
{

std::ostream& operator<<(std::ostream& stream, ExportColor const& color)
{
	stream << color.r << " " << color.g << " " << color.b << " " << color.a;
	return stream;
}

} // namespace pf
