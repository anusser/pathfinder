#pragma once

#include "../bounding_box.h"
#include "../paths.h"
#include "../paths_ds.h"

#include <cstdint>
#include <limits>
#include <ostream>
#include <vector>

namespace pf
{
// r = red, g = green, b = blue, a = alpha (opacity)
struct ExportColor {
	int r;
	int g;
	int b;
	int a;
};
using ExportColors = std::vector<ExportColor>;

std::ostream& operator<<(std::ostream& stream, ExportColor const& color);

// The colors vectors have to have the same size as the vectors for the objects.
struct ExportData {
	std::string filename;

	BoundingBoxes boxes;
	PathsDS paths_ds;

	ExportColors box_colors;
	ExportColors path_colors;

	ExportData() = default;
	ExportData(const ExportData&) = default;
	ExportData& operator=(const ExportData&) = default;

	virtual ~ExportData()
	{
	}
};

} // namespace pf
