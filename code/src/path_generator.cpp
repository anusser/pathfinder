#include "path_generator.h"

#include "defs.h"
#include "dijkstra.h"
#include "geometry.h"

#include <algorithm>
#include <limits>
#include <omp.h>
#include <tuple>
#include <unordered_set>
#include <vector>

namespace pf
{

PathGenerator::PathGenerator(
    Graph const& graph, Random& random, uint nr_of_threads)
    : graph(graph)
    , random(random)
    , geometry(graph)
    , nr_of_threads(nr_of_threads)
{
	for (uint i = 0; i < nr_of_threads; i++) {
		_thread_data.push_back(ThreadData(graph, random));
	}
}

Path PathGenerator::getPath(PathType path_type, NodeID source)
{
	if (source == c::NO_NID) {
		source = random.getNode(graph);
	}

	switch (path_type) {
	case PathType::RandomWalk:
		return getRandomWalkPath(source);
	case PathType::RandomShortest:
		return getRandomShortestPath(source);
	case PathType::RandomCourse:
		return getRandomCoursePath(source);
	default:
		throw Exception("Unknown PathType provided to PathGenerator::getPath");
	}
}

PathsDS PathGenerator::getPaths(PathType path_type, std::size_t number)
{
	PathsDS paths_ds;
	for (std::size_t i = 0; i < number; ++i) {
		auto path = getPath(path_type);
		paths_ds.addPath(path);
	}

	return paths_ds;
}

Path PathGenerator::getStitchedPath(PathTypes const& path_types)
{
	Path path;
	auto source = c::NO_NID;

	for (auto path_type : path_types) {
		auto new_path = getPath(path_type, source);
		path.append(graph, new_path);

		source = path.getTarget(graph);
	}
	setRandomTimeIntervalls(path, random);
	return path;
}

PathsDS PathGenerator::getStitchedPaths(
    PathTypes const& path_types, std::size_t number)
{
	PathsDS paths_ds;
	for (std::size_t i = 0; i < number; ++i) {
		auto path = getStitchedPath(path_types);
		paths_ds.addPath(path);
	}

	return paths_ds;
}

Path PathGenerator::getRandomPath()
{
	std::size_t const number_of_stitched_paths = 3;
	return getStitchedPath(getRandomPathTypes(number_of_stitched_paths));
}

PathsDS PathGenerator::getRandomPaths(std::size_t number)
{
	PathsDS paths_ds;
	for (std::size_t i = 0; i < number; ++i) {
		auto path = getRandomPath();
		paths_ds.addPath(path);
	}

	return paths_ds;
}

PathsDS PathGenerator::getRandomShortestPaths(
    std::size_t number, double max_distance)
{
	TimeTracker time_tracker;
	Print("Generation of paths with distance requirement ...");
	time_tracker.start();

#pragma omp parallel for num_threads(nr_of_threads) schedule(dynamic)
	for (std::size_t i = 0; i < number; ++i) {
		auto path = getRandomShortestPath(max_distance);
		setRandomTimeIntervalls(path, _myThreadData().random);
		_myThreadData().paths_ds.addPath(path);
	}

	// merge thread results
	PathsDS paths_ds;
	for (ThreadData& td : _thread_data) {
		for (PathID path_id = 0; path_id < td.paths_ds.getNrOfPaths();
		     path_id++) {
			// could be way more efficient
			Path path = td.paths_ds.getPath(path_id);
			paths_ds.addPath(path);
		}
	}

	time_tracker.stop();

	Print("Took " << time_tracker.getLastMeasurment() << " seconds");

	return paths_ds;
}

PathsDS PathGenerator::getRandomShortestArtificiallyStitchedPaths(
    std::size_t number, double max_distance)
{
	TimeTracker time_tracker;
	Print("Generation of paths with distance requirement ...");
	time_tracker.start();

#pragma omp parallel for num_threads(nr_of_threads) schedule(dynamic)
	for (std::size_t i = 0; i < number; ++i) {
		auto ch_rep_shopa = getRandomShortestPathEfficiently(max_distance);
		Path artificiallyStitchedPath =
		    getArtificiallyStitchedPath(ch_rep_shopa);
		_myThreadData().paths_ds.addPath(artificiallyStitchedPath);
	}

	// merge thread results
	PathsDS paths_ds;
	for (ThreadData& td : _thread_data) {
		for (PathID path_id = 0; path_id < td.paths_ds.getNrOfPaths();
		     path_id++) {
			// could be way more efficient
			Path path = td.paths_ds.getPath(path_id);
			paths_ds.addPath(path);
		}
	}

	time_tracker.stop();

	Print("Took " << time_tracker.getLastMeasurment() << " seconds");

	return paths_ds;
}

Path PathGenerator::getArtificiallyStitchedPath(Path const& ch_rep_shopa)
{
	Path unpacked = ch_rep_shopa.unpack(graph);
	setRandomTimeIntervalls(unpacked, _myThreadData().random);

	// matched data has 11.088 on average
	uint nof_subpaths = _myThreadData().random.getInt(8, 14 + 1);

	std::vector<Path> sub_paths;

	// sample random positions where to split the path in subpaths
	std::vector<size_t> splits;
	splits.push_back(unpacked.getEdges().size()); // end node
	while (splits.size() < nof_subpaths &&
	       splits.size() < unpacked.getEdges().size()) {
		size_t split_candidate =
		    _myThreadData().random.getInt(1, unpacked.getEdges().size());
		bool alreadyExists = std::find(splits.begin(), splits.end(),
		                         split_candidate) != splits.end();
		if (!alreadyExists) {
			splits.push_back(split_candidate);
		}
	}
	std::sort(splits.begin(), splits.end());
	std::reverse(splits.begin(), splits.end());

	uint start = 0;
	while (!splits.empty()) {
		size_t end = splits.back();
		splits.pop_back();
		Path sub_path = unpacked.getSubPathCopy(start, end);
		sub_paths.push_back(sub_path);
		start = end;
	}

	Path artificially_stitched_path;
	for (Path const& sub_path : sub_paths) {
		Path partitioned_sub_path = _myThreadData().partitioner->run(sub_path);
		artificially_stitched_path.append(graph, partitioned_sub_path);
	}

	return artificially_stitched_path;
}

PathsDS PathGenerator::getDistanceBasedStitchedPaths(
    std::size_t number, double max_distance)
{
	TimeTracker time_tracker;
	Print("Generation of stitched paths with distance requirement ...");
	time_tracker.start();

#pragma omp parallel for num_threads(nr_of_threads) schedule(dynamic)
	for (std::size_t i = 0; i < number; ++i) {

		// matched data has 11.088 on average
		int nof_subpaths = random.getInt(8, 14 + 1);

		Path distanceBasedStitchedPath =
		    getDistanceBasedStitchedPath(nof_subpaths, max_distance);
		_myThreadData().paths_ds.addPath(distanceBasedStitchedPath);
	}

	// merge thread results
	PathsDS paths_ds;
	for (ThreadData& td : _thread_data) {
		for (PathID path_id = 0; path_id < td.paths_ds.getNrOfPaths();
		     path_id++) {
			// could be way more efficient
			Path path = td.paths_ds.getPath(path_id);
			paths_ds.addPath(path);
		}
	}

	time_tracker.stop();

	Print("Took " << time_tracker.getLastMeasurment() << " seconds");

	return paths_ds;
}

Path PathGenerator::getDistanceBasedStitchedPath(
    uint nof_subpaths, double distance)
{
	std::vector<Path> sub_paths;

	while (sub_paths.size() < nof_subpaths) {
		sub_paths = getDistanceBasedStitchedSubPaths(nof_subpaths, distance);
	}

	// stitch paths together
	Path distanceBasedStitchedPath;
	for (Path const& sub_path : sub_paths) {
		Path re_partitioned_sub_path =
		    _myThreadData().partitioner->run(sub_path);
		distanceBasedStitchedPath.append(graph, re_partitioned_sub_path);
	}

	return distanceBasedStitchedPath;
}

std::vector<Path> PathGenerator::getDistanceBasedStitchedSubPaths(
    uint nof_subpaths, double distance)
{
	std::vector<Path> sub_paths;
	double distance_for_subpath = distance / nof_subpaths;

	for (uint i = 0; i < nof_subpaths; i++) {

		NodeID node = c::NO_NID;
		if (sub_paths.empty()) {
			// first subpath
			node = random.getNode(graph);
		} else {
			Path last_sub_path = sub_paths.back();
			node = graph.getEdge(last_sub_path.getEdges().back()).target;
		}

		Path partitioned_sub_path =
		    getRandomShortestCoursedPath(node, distance_for_subpath, 100);
		if (partitioned_sub_path.empty()) {
			// start generation all over again
			return sub_paths;
		}
		// unpack for setting of time intervalls
		Path sub_path = partitioned_sub_path.unpack(graph);
		if (sub_paths.empty()) {
			// first subpath
			setRandomTimeIntervalls(sub_path, _myThreadData().random);
		} else {
			Path last_sub_path = sub_paths.back();
			setRandomTimeIntervalls(
			    sub_path, last_sub_path.getEndTime(), _myThreadData().random);
		}
		sub_paths.push_back(sub_path);
	}
	return sub_paths;
}

auto PathGenerator::getRandomPathType() -> PathType
{
	return (PathType)random.getInt(0, number_of_path_types);
}

auto PathGenerator::getRandomPathTypes(std::size_t number) -> PathTypes
{
	PathTypes path_types;
	for (std::size_t i = 0; i < number; ++i) {
		path_types.push_back(getRandomPathType());
	}

	return path_types;
}

Path PathGenerator::getRandomWalkPath(NodeID source)
{
	Path path;
	std::unordered_set<NodeID> already_seen;

	auto node_id = source;
	already_seen.insert(node_id);

	// Do a random walk not repeating any previously seen node
	while (random.getFloat(0, 1) > p_stop) {
		auto edge_id = random.getNonShortcutOutgoingEdge(graph, node_id);
		// stop if there is no outgoing edge
		if (edge_id == c::NO_EID) {
			break;
		}

		auto target = graph.getEdge(edge_id).target;

		bool not_seen_yet;
		std::tie(std::ignore, not_seen_yet) = already_seen.insert(target);
		if (not_seen_yet) {
			path.push(graph, edge_id);
			node_id = target;
		}
	}

	return path;
}

Path PathGenerator::getRandomShortestPath(NodeID source)
{
	auto target = random.getNode(graph);

	CHDijkstra dijkstra(graph);
	dijkstra.run(source, target);

	return dijkstra.getResult().path.unpack(graph);
}

Path PathGenerator::getRandomShortestPath(double maxDistance)
{
	NodeID source = random.getNode(graph);
	return getRandomShortestPath(source, maxDistance);
}

Path PathGenerator::getRandomShortestPath(NodeID source, double maxDistance)
{
	CHDijkstra& dijkstra = _myThreadData().ch_dijkstra;

	do {
		NodeID target;
		double dist;

		do {
			target = random.getNode(graph);
			dist = geometry.calcGeoDist(graph.getNode(source).coordinate,
			    graph.getNode(target).coordinate);
		} while (dist > maxDistance);

		dijkstra.run(source, target);

	} while (!dijkstra.getResult().path_found);

	return std::move(dijkstra.stealResult().path);
}

Path PathGenerator::getRandomShortestPathEfficiently(double maxDistance)
{
	CHDijkstra& dijkstra = _myThreadData().ch_dijkstra;

	do {
		NodeID source;
		NodeID target;
		double dist;

		do {
			source = random.getNode(graph);
			target = random.getNode(graph);
			dist = geometry.calcGeoDist(graph.getNode(source).coordinate,
			    graph.getNode(target).coordinate);
		} while (dist > maxDistance);

		dijkstra.run(source, target);

	} while (!dijkstra.getResult().path_found);

	return std::move(dijkstra.stealResult().path);
}

Path PathGenerator::getRandomShortestCoursedPath(
    NodeID source, double distance, int nofAttempts)
{
	for (int i = 0; i < nofAttempts; i++) {
		Path path = getRandomShortestCoursedPath(source, distance);
		if (!path.getEdges().empty()) {
			return path;
		}
	}

	Path emptyPath;
	return emptyPath;
}

Path PathGenerator::getRandomShortestCoursedPath(NodeID source, double distance)
{
	// in the first step we get a course path to sample some realistic
	// destination
	Path coursePath = getRandomCoursePath(source, distance);
	NodeID tgt;

	// require a min distance
	if (coursePath.getEdges().empty()) {
		Path emptyPath;
		return emptyPath;
	} else {
		tgt = graph.getEdge(coursePath.getEdges().back()).target;
		if (geometry.calcGeoDist(graph.getNode(source).coordinate,
		        graph.getNode(tgt).coordinate) <= distance / 2) {
			Path emptyPath;
			return emptyPath;
		}
	}

	// now we get a shortest path for it
	CHDijkstra& dijkstra = _myThreadData().ch_dijkstra;
	dijkstra.run(source, tgt);
	return std::move(dijkstra.stealResult().path);
}

Path PathGenerator::getRandomCoursePath(NodeID source, double max_distance)
{
	Path path;
	Course course = random.getCourse();
	std::unordered_set<NodeID> already_seen;

	auto node_id = source;

	while (geometry.calcGeoDist(graph.getNode(source).coordinate,
	           graph.getNode(node_id).coordinate) < max_distance) {
		if (already_seen.count(node_id) > 0 ||
		    graph.getNonShortcutOutEdgesOf(node_id).size() == 0) {
			break;
		}
		already_seen.insert(node_id);

		EdgeID best_edge = c::NO_EID;
		float best_similarity = std::numeric_limits<float>::lowest();

		for (EdgeID const& edge_id : graph.getNonShortcutOutEdgesOf(node_id)) {
			Edge edge = graph.getEdge(edge_id);
			assert(!graph.getEdge(edge_id).isShortcut());
			auto const& source = graph.getNode(edge.source);
			auto const& target = graph.getNode(edge.target);

			Course edge_course(source, target);
			auto similarity = course.calculateSimilarity(edge_course);

			if (similarity > best_similarity) {
				best_edge = edge.id;
				best_similarity = similarity;
			}
		}

		path.push(graph, best_edge);
		node_id = graph.getEdge(best_edge).target;
	}
	return path;
}

void PathGenerator::setPStop(float p_stop)
{
	assert(p_stop > 0 && p_stop <= 1);

	this->p_stop = p_stop;
}

void PathGenerator::setRandomTimeIntervalls(Path& path, Random& random)
{
	TimeType start =
	    random.getInt(pam::reasonable_min_start_time, pam::max_end_time);
	setRandomTimeIntervalls(path, start, random);
}

void PathGenerator::setRandomTimeIntervalls(
    Path& path, TimeType start, Random& random)
{
	TimeType end;

	for (EdgeIndex edge_index = 0; edge_index < path.getEdges().size();
	     edge_index++) {

		// set generation bounds get the median on average
		TimeType lowerGenerationBound = 1;
		TimeType upperGenerationBound = pam::median_edge_time * 2 - 1;
		TimeType timestep =
		    random.getInt(lowerGenerationBound, upperGenerationBound + 1);

		end = start + timestep;
		path.setTimeIntervallForEdge(edge_index, TimeIntervall(start, end));
		start = end;
	}
}

auto PathGenerator::_myThreadData() -> ThreadData&
{
	return _thread_data[omp_get_thread_num()];
}

} // namespace pf
