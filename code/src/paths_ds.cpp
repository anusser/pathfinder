#include "paths_ds.h"

#include "defs.h"

namespace pf
{

PathsDS::PathsDS()
{
	OffsetAndTimeAtLastNode zero_offset =
	    OffsetAndTimeAtLastNode(0, c::INVALID_TIME);
	offset_and_time_at_last_node_vector.push_back(zero_offset);
}

PathID PathsDS::getNrOfPaths() const
{
	return offset_and_time_at_last_node_vector.size() - 1;
}

bool PathsDS::empty() const
{
	return getNrOfPaths() == 0;
}

void PathsDS::addPath(Path const& path)
{
	Offset last_offset = offset_and_time_at_last_node_vector.back().offset;
	assert(last_offset == time_at_node_and_next_edge_vector.size());

	for (EdgeIndex edge_index = 0; edge_index < path.length(); edge_index++) {
		TimeType time_at_node = path.getTimeAtNodes().at(edge_index);
		EdgeID edge_id = path.getEdge(edge_index);
		time_at_node_and_next_edge_vector.push_back(
		    TimeAtNodeAndNextEdge(time_at_node, edge_id));
	}

	Offset offset = last_offset + path.length();
	TimeType time_at_last_node = path.getTimeAtNodes().at(path.length());
	offset_and_time_at_last_node_vector.push_back(
	    OffsetAndTimeAtLastNode(offset, time_at_last_node));
}

Path PathsDS::getPath(PathID path_id) const
{
	Offset start = offset_and_time_at_last_node_vector.at(path_id).offset;
	Offset end = offset_and_time_at_last_node_vector.at(path_id + 1).offset;
	Path path;
	std::size_t node_index;
	for (Offset i = start; i < end; i++) {
		TimeAtNodeAndNextEdge time_at_node_and_next_edge =
		    time_at_node_and_next_edge_vector.at(i);
		node_index = path.length();
		path.setTimeAtNode(node_index, time_at_node_and_next_edge.time_at_node);
		path.push(time_at_node_and_next_edge.edge_id);
	}

	node_index = path.length();
	path.setTimeAtNode(node_index,
	    offset_and_time_at_last_node_vector.at(path_id + 1).time_at_last_node);

	return path;
}

size_t PathsDS::getPathLength(PathID path_id) const
{
	Offset start = offset_and_time_at_last_node_vector.at(path_id).offset;
	Offset end = offset_and_time_at_last_node_vector.at(path_id + 1).offset;
	return end - start;
}

} // namespace pf
