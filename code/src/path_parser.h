#pragma once

#include "basic_types.h"
#include "path_analyzer.h"
#include "paths.h"
#include "paths_ds.h"

#include <string>
#include <unordered_map>

namespace pf
{

namespace unit_tests
{
void testPathParser();
} // namespace unit_tests

class PathParser
{
public:
	PathParser(Graph const& graph, PathsDS& paths_ds);

	// Returns true iff parsing was successful.
	bool parse(const std::string& filename);

private:
	bool already_parsed = false;

	std::unordered_map<OsmNodeId, NodeID> osmIdsToGraphIds;

	Graph const& graph;
	PathsDS& paths_ds;
	PathAnalyzer path_analyzer;

	size_t nof_paths_omitted = 0;

	// Helper functions for parsing the paths file.
	void initOsmIdsToGraphIds();
	void readOpenBracket(std::ifstream& f);
	void readTraces(std::ifstream& f);
	void readTrace(std::ifstream& f);
	Factor readFactorHeader(const std::string line);
	void readOsmEdgeIfPossible(
	    const std::string line, std::vector<OsmEdge>& path);
	OsmEdge readOsmEdge(const std::string line);
	std::vector<GeoMeasurement> readGeoMeasurements(const std::string line);
	bool checkForNoLastOsmEdge(const std::string line);
	bool checkForResPathEnd(const std::string line);
	bool checkForTraceEnd(const std::string line);
	template <typename Out>
	void split(const std::string& s, char delim, Out result);
	std::vector<std::string> split(const std::string& s, char delim);

	friend void unit_tests::testPathParser();
};

} // namespace pf
