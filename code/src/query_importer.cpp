#include "query_importer.h"

#include "defs.h"
#include "query.h"
#include "time_tracker.h"

#include <fstream>

namespace pf
{

QueryImporter::QueryImporter(Graph const& graph, Queries& queries)
    : graph(graph), queries(queries)
{
}

bool QueryImporter::importQueriesForPerformanceTest(
    const std::string graph_filename, uint number_of_queries,
    double factorForBBSize)
{
	std::string directory_name =
	    graph_filename + "_" + std::to_string(number_of_queries) + "_queries";
	std::string file_name =
	    directory_name + "/" + std::to_string(factorForBBSize);
	return importQueries(file_name);
}

bool QueryImporter::importQueries(std::string file_name)
{
	assert(!already_parsed);
	already_parsed = true;

	TimeTracker time_tracker;

	Print("Import queries ...");
	time_tracker.start();

	bool success = true;
	std::ifstream f;
	f.open(file_name.c_str());

	if (f.is_open()) {
		try {
			readHeader(f);
			readQueries(f);
		} catch (Exception e) {
			throw e;
			success = false;
		}

		f.close();
	} else {
		success = false;
	}

	time_tracker.stop();
	Print("Took " << time_tracker.getLastMeasurment() << " seconds");

	return success;
}

void QueryImporter::readHeader(std::ifstream& f)
{
	std::string line;
	std::getline(f, line);
	uint number_of_queries = std::stoi(line);
	queries.resize(number_of_queries);

	std::getline(f, line); // eat empty line
}

void QueryImporter::readQueries(std::ifstream& f)
{
	for (Query& query : queries) {
		std::string line;
		std::getline(f, line);
		readQuery(query, line);
	}
}

void QueryImporter::readQuery(Query& query, std::string const& line)
{
	std::vector<std::string> data = split(line, ' ');
	assert(data.size() == 7);

	Coordinate min;
	min.lat = std::stod(data[0]);
	min.lon = std::stod(data[1]);
	Coordinate max;
	max.lat = std::stod(data[2]);
	max.lon = std::stod(data[3]);
	BoundingBox bb(min, max);
	query.bounding_box = bb;

	TimeIntervall time_intervall;
	time_intervall.min_time = std::stoi(data[4]);
	time_intervall.max_time = std::stoi(data[5]);
	query.time_intervall = time_intervall;

	TimeSlots time_slots(data[6]);
	query.time_slots = time_slots;
}

template <typename Out>
void QueryImporter::split(const std::string& s, char delim, Out result)
{
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		*(result++) = item;
	}
}

std::vector<std::string> QueryImporter::split(const std::string& s, char delim)
{
	std::vector<std::string> elems;
	split(s, delim, std::back_inserter(elems));
	return elems;
}

} // namespace pf
