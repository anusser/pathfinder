#pragma once

#include "basic_types.h"
#include "paths.h"

#include <bitset>
#include <ostream>
#include <vector>

namespace pf
{

struct TimeSlots {
	const static int nofslots = 64;
	const static int secondsPerWeek = 60 * 60 * 24 * 7;
	constexpr static int secondsPerSlot = secondsPerWeek / nofslots;

	std::bitset<nofslots> slots;

	// the coordinates are set invalid by their default constructors
	TimeSlots();
	TimeSlots(bool valueForAll);
	TimeSlots(TimeIntervall const& time_intervall);
	TimeSlots(std::string slots);

	bool operator==(TimeSlots const& box) const;

	void expand(TimeIntervall const& time_intervall);
	void expand(TimeSlots const& slotMask);
	void intersect(TimeSlots const& slotMask);

	bool isValid() const;
	bool isFull() const;

	int getIndex(TimeType time);
};

std::ostream& operator<<(std::ostream& stream, TimeSlots const& slotMask);

using TimeSlotsVector = std::vector<TimeSlots>;

} // namespace pf
