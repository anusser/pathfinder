#pragma once

#include "bounding_box.h"
#include "course.h"
#include "graph.h"
#include "timeIntervall.h"
#include "timeSlots.h"

#include <random>

namespace pf
{

namespace unit_tests
{
void testRandom();
} // namespace unit_tests

class Random
{
public:
	Random();
	Random(unsigned int seed);

	void seed(unsigned int seed);
	unsigned int getSeed() const;

	// all of the following are chosen uniformly at random

	// returns NO_NID iff the graph has no nodes
	NodeID getNode(Graph const& graph);
	// returns NO_NID iff the graph has no edges
	EdgeID getEdge(Graph const& graph);
	// returns NO_EID iff node_id has no outgoing edges
	EdgeID getNonShortcutOutgoingEdge(Graph const& graph, NodeID node_id);

	BoundingBox getBox(Graph const& graph);
	BoundingBox getBox(Graph const& graph, double width, double height);
	TimeIntervall getIntervall();
	TimeSlots getSingleTimeSlot();
	TimeSlots getTimeSlots();
	Course getCourse();

	// this gives an integer in the semi-open range [lower_bound, upper_bound)
	int getInt(int lower_bound, int upper_bound);
	float getFloat(float lower_bound, float upper_bound);

private:
	unsigned int seed_value;
	std::default_random_engine generator;

	friend void unit_tests::testRandom();
};

} // namespace pf
