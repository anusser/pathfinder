#pragma once

#include "IPathfinder.h"
#include "basic_types.h"
#include "flags.h"
#include "geometry.h"
#include "pathfinder_ds.h"
#include "paths.h"
#include "time_tracker.h"

namespace pf
{

namespace unit_tests
{
void testPathfinder(uint nr_of_threads_to_test);
} // namespace unit_tests

class NaiveCHPathfinder : public IPathfinder
{
private:
	using Result = CHTraversal::Result;
	using ExitValue = CHTraversal::ExitValue;

	PathfinderDS& ds;
	Graph const& graph;
	Geometry geometry;

	EdgeIDs cut_edges;

	std::size_t nof_original_cut_edges;

	struct ThreadData {
		EdgeIDs cut_edges;
	};
	std::vector<ThreadData> _thread_data;

public:
	NaiveCHPathfinder(PathfinderDS& ds, int nr_of_threads)
	    : ds(ds)
	    , graph(ds.getGraph())
	    , geometry(ds.getGraph())
	    , _thread_data(nr_of_threads)
	{
	}

	auto _myThreadData() -> ThreadData&
	{
		return _thread_data[omp_get_thread_num()];
	}

	PathIDs run(Query const& query)
	{
		// check all non shortcut edges for cut with query box

#pragma omp parallel num_threads(_thread_data.size())
		{
			_myThreadData().cut_edges.clear();
		}

#pragma omp parallel for num_threads(_thread_data.size())
		for (uint edge_id = 0; edge_id < graph.getNumberOfEdges(); edge_id++) {
			auto const& edge = graph.getEdge(edge_id);
			if (edge.isShortcut()) {
				continue;
			}
			if (geometry.intersect(query.bounding_box, edge.id)) {
				_myThreadData().cut_edges.push_back(edge.id);
			}
		}

		cut_edges.clear();
		for (ThreadData const& td : _thread_data) {
			cut_edges.insert(
			    cut_edges.end(), td.cut_edges.begin(), td.cut_edges.end());
		}
		nof_original_cut_edges = cut_edges.size();

		add_as_cut_edge_traversal(cut_edges);

		// get path ids for cut edges (incl. shortcuts)
		ds.getPathIDs(cut_edges, query);

		auto path_ids = ds.removeDuplicatePathIDs();

		ds.resetCollected();

		return path_ids;
	}

	auto add_as_cut_edge_traversal(EdgeIDs const& start_edge_ids) -> ExitValue
	{
		using PQElement = std::pair<decltype(Node::level), EdgeID>;
		using PQ = std::priority_queue<PQElement, std::vector<PQElement>,
		    std::greater<PQElement>>;

		PQ pq;

		for (auto start_edge_id : start_edge_ids) {
			auto shortcut_node_id = graph.getShortcutNode(start_edge_id);
			if (shortcut_node_id == c::NO_NID) {
				pq.emplace(c::NO_LEVEL, start_edge_id);
			} else {
				// looks like dead code
				pq.emplace(
				    graph.getNode(shortcut_node_id).level, start_edge_id);
			}
		}

		while (!pq.empty()) {
			auto edge_id = pq.top().second;

			// pop edge and all duplicates
			while (!pq.empty() && pq.top().second == edge_id) {
				pq.pop();
			}

			for (auto parent_id : graph.getParents(edge_id)) {
				auto result = add_as_cut_edge(parent_id);

				switch (result) {
				case Result::StopSearch:
					return ExitValue::Stopped;
				case Result::Expand:
					pq.emplace(graph.getShortcutLevel(parent_id), parent_id);
					break;
				case Result::Ignore:
					break;
				}
			}
		}

		return ExitValue::NotStopped;
	}

	// push this cut information to shortcuts
	auto add_as_cut_edge(EdgeID edge_id) -> Result
	{
		cut_edges.push_back(edge_id);
		return Result::Expand;
	}

	PathIDs run(BoundingBox const& box)
	{
		return run(Query(box));
	}

	std::size_t getNofEdgesConsidered()
	{
		return nof_original_cut_edges;
	}
};

} // namespace pf
