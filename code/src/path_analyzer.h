#pragma once

#include "geometry.h"
#include "paths.h"
#include "paths_ds.h"

namespace pf
{

class PathAnalyzer
{

public:
	PathAnalyzer(Graph const& graph);

	void analyze(PathsDS const& paths_ds);
	bool checkPath(Path const& path) const;

private:
	double getLengthByNodesInKm(EdgeID const& edge_id) const;
	double getLengthByNodesInKm(Path const& path) const;

	Graph const& graph;
	Geometry geometry;
};

} // namespace pf
