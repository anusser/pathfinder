#include "query.h"

namespace pf
{

Query::Query()
{
}

Query::Query(BoundingBox bounding_box)
    : bounding_box(bounding_box)
    , time_intervall(c::FULL_INTERVALL)
    , time_slots(true)
{
}

Query::Query(BoundingBox bounding_box, TimeIntervall time_intervall,
    TimeSlots time_slots)
    : bounding_box(bounding_box)
    , time_intervall(time_intervall)
    , time_slots(time_slots)
{
}

bool Query::isSpatialOnly() const
{
	return time_intervall == c::FULL_INTERVALL && time_slots.isFull();
}

} // namespace pf
