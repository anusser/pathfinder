#include "timeIntervall.h"
#include "bounding_box.h"

namespace pf
{

TimeIntervall::TimeIntervall()
    : min_time(invalid_value), max_time(invalid_value)
{
}

TimeIntervall::TimeIntervall(TimeType time) : min_time(time), max_time(time)
{
}

TimeIntervall::TimeIntervall(TimeType min_time, TimeType max_time)
    : min_time(min_time), max_time(max_time)
{
}

bool TimeIntervall::operator==(TimeIntervall const& intervall) const
{
	return min_time == intervall.min_time && max_time == intervall.max_time;
}

bool TimeIntervall::operator<(const TimeIntervall& rhs) const
{
	if (min_time < rhs.min_time) {
		return true;
	} else if (min_time > rhs.min_time) {
		return false;
	} else {
		return max_time < rhs.max_time;
	}
}

bool TimeIntervall::isValid() const
{
	return min_time != invalid_value && max_time != invalid_value;
}

void TimeIntervall::expand(TimeIntervall const& intervall)
{
	if (!intervall.isValid()) {
		return;
	}
	if (!isValid()) {
		*this = intervall;
		return;
	}

	min_time = std::min(min_time, intervall.min_time);
	max_time = std::max(max_time, intervall.max_time);
}

void TimeIntervall::intersect(TimeIntervall const& intervall)
{
	min_time = std::max(min_time, intervall.min_time);
	max_time = std::min(max_time, intervall.max_time);

	if (min_time > max_time) {
		min_time = invalid_value;
		max_time = invalid_value;
	}
}

std::ostream& operator<<(std::ostream& stream, TimeIntervall const& intervall)
{
	stream << ", min time: " << intervall.min_time
	       << ", max time: " << intervall.max_time;

	return stream;
}

} // namespace pf
