#pragma once

#include "basic_types.h"

#include <ostream>
#include <vector>

namespace pf
{

struct TimeIntervall {

	TimeType min_time;
	TimeType max_time;

	// the coordinates are set invalid by their default constructors
	TimeIntervall();
	TimeIntervall(TimeType time);
	TimeIntervall(TimeType min_time, TimeType max_time);

	bool operator==(TimeIntervall const& box) const;
	bool operator<(const TimeIntervall& rhs) const;

	bool isValid() const;
	void expand(TimeIntervall const& box);
	void intersect(TimeIntervall const& box);
};

namespace c
{
TimeIntervall const FULL_INTERVALL =
    TimeIntervall(std::numeric_limits<TimeType>::lowest(),
        std::numeric_limits<TimeType>::max());
} // namespace c

std::ostream& operator<<(std::ostream& stream, TimeIntervall const& box);

using TimeIntervalls = std::vector<TimeIntervall>;

} // namespace pf
