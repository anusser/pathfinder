#pragma once

#include "query.h"

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

namespace pf
{

class QueryExporter
{
public:
	QueryExporter();

	void exportQueriesForPerformanceTest(Queries const& queries,
	    std::string graph_filename, double factorForBBSize) const;

	void exportQueries(const Queries& queries, std::string file_name) const;

private:
	void writeHeader(Queries const& queries, std::ofstream& f) const;
	void writeQueries(Queries const& queries, std::ofstream& f) const;
};

} // namespace pf
