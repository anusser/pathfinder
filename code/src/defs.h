#pragma once

#include <exception>
#include <iostream>

// empty "real" statement
#define PF_NOP                                                                 \
	do {                                                                       \
	} while (0)

#ifdef NDEBUG
#define Debug(x) PF_NOP
#else
#define Debug(x)                                                               \
	do {                                                                       \
		std::cout << x << std::endl;                                           \
	} while (0)
#endif

#if defined(NVERBOSE) && defined(NDEBUG)
#define Print(x) PF_NOP
#else
#define Print(x)                                                               \
	do {                                                                       \
		std::cout << x << std::endl;                                           \
	} while (0)
#endif

#define Unused(x) ((void)x)

#ifdef NDEBUG
#undef NDEBUG
#include <cassert>
#define NDEBUG
#define debug_assert(...) PF_NOP
#else
#include <cassert>
#define debug_assert(...) assert(__VA_ARGS__)
#endif

class Exception : std::exception
{
public:
	Exception(std::string const& message) : message(std::move(message))
	{
	}

	const char* what() const noexcept
	{
		return message.c_str();
	}

private:
	std::string message;
};
