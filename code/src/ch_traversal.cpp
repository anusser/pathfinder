#include "ch_traversal.h"

#include <utility>

namespace pf
{

CHTraversal::CHTraversal(Graph const& graph)
    : graph(graph)
    , seen(graph.getNumberOfNodes(), false)
    , node_contraction_order(graph.getContractionOrder())
    , edge_creation_order(graph.getEdgeCreationOrder())
{
}

void CHTraversal::resetSeen()
{
	seen.reset();
}

bool CHTraversal::wasSeen(NodeID node_id) const
{
	return seen[node_id];
}

} // namespace pf
