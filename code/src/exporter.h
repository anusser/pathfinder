#pragma once

#include "geometry.h"
#include "graph.h"
#include "io/export_types.h"

#include <cstdint>
#include <string>
#include <unordered_map>
#include <vector>

namespace pf
{

namespace unit_tests
{
void testExporter();
} // namespace unit_tests

class Exporter
{
public:
	Exporter(Graph const& graph);

	void exportSimplestGraphRendererFile(ExportData const& data) const;

private:
	Graph const& graph;
	Geometry geometry;

	using ExportNodeID = NodeID;

	struct CoordinateHash {
		std::size_t operator()(Coordinate const& coordinate) const;
	};
	using NodeMap =
	    std::unordered_map<Coordinate, ExportNodeID, CoordinateHash>;

	NodeMap createNodeMap(ExportData const& data) const;
	void writeSimplestHeader(NodeMap const& node_map, ExportData const& data,
	    std::ofstream& f) const;
	void writeSimplestNodes(NodeMap& node_map, std::ofstream& f) const;
	void writeSimplestEdges(NodeMap const& node_map, ExportData const& data,
	    std::ofstream& f) const;
};

} // namespace pf
