#include "defs.h"
#include "graph.h"
#include "naive_ch_pathfinder.h"
#include "path_exporter.h"
#include "path_generator.h"
#include "path_importer.h"
#include "path_parser.h"
#include "pathfinder.h"
#include "pathfinder_ds.h"
#include "pathfinders.h"
#include "query_exporter.h"
#include "query_importer.h"
#include "random.h"
#include "stxxl_init.h"
#include "time_tracker.h"

#include <getopt.h>
#include <map>
#include <math.h>
#include <string>

// #define NAIVE

using namespace pf;

// measured time
using MTime = double;

// nof average bytes read on disk from stxxl
using NofBytesRead = double;

const int minBbSizeExponent = 1;
const int maxBbSizeExponent = 5;

const uint N_OF_DIGITS = 5;
const uint WIDTH = N_OF_DIGITS + 5;

void printUsage()
{
	std::cout
	    << "Usage: ./performance_test [ARGUMENTS]\n"
	    << "Mandatory arguments are:\n"
	    << "  -i, --graph_filename <path>        Read graph from <path>\n"
	    << "Mutual exclusive arguments are:\n"
	    << "  -q, --number_of_queries <number>        Query <number> times\n"
	    << "  -w, --number_of_queries <number>        Import <number> "
	       "generated "
	       "random queries from archive which were exported before \n"
	    << "Mutual exclusive arguments are:\n"
	    << "  -p, --path_filename <path>        Read paths from <path>\n"
	    << "  -g, --number_of_paths <number>        Generate <number> random "
	       "paths \n"
	    << "  -a, --number_of_paths <number>        Import <number> generated "
	       "random paths from archive which were exported before \n"
	    << "Optional arguments are:\n"
	    << "  -u, --pathfinder type <type>        Measure performance of "
	       "certain pathfinder variant: 'naive', 'naive_ch', 'STD' (default) \n"
	    << "  -m, --measure_intervalls        Measure performance with "
	       "time-intervall constraints \n"
	    << "  -t, --threads <number>     Number of threads to use in the "
	       "calculations (default: 1)\n"
	    << "  -e, --export        Export generated paths and queries \n";
}

BoundingBox getBoundingBoxForGraph(Graph const& graph)
{
	double minLat = std::numeric_limits<double>::max();
	double maxLat = std::numeric_limits<double>::lowest();
	double minLon = std::numeric_limits<double>::max();
	double maxLon = std::numeric_limits<double>::lowest();

	for (auto const& node : graph.getAllNodes()) {
		if (node.coordinate.lat < minLat) {
			minLat = node.coordinate.lat;
		}
		if (node.coordinate.lat > maxLat) {
			maxLat = node.coordinate.lat;
		}
		if (node.coordinate.lon < minLon) {
			minLon = node.coordinate.lon;
		}
		if (node.coordinate.lon > maxLon) {
			maxLon = node.coordinate.lon;
		}
	}

	return BoundingBox(Coordinate(minLat, minLon), Coordinate(maxLat, maxLon));
}

void testQueries(IPathfinder& pathfinder, Queries const& queries,
    Distance distance, BBSize bbSize, std::map<BBSize, MTime>& time_results,
    std::map<Substep, std::map<Distance, std::map<BBSize, MTime>>>&
        substep_time_results,
    std::map<BBSize, double>& nofPath_results,
    std::map<BBSize, Measurement>& on_disk_measurements,
    bool measure_time_intervalls, bool measure_time_slots)
{

	std::size_t intersection_path_count = 0;

	TimeTracker time_tracker;

	std::size_t edges_considered_sum = 0;
	for (Query const& query : queries) {
		time_tracker.start();
		PathIDs intersection_path_ids;

		Query query_to_measure(query.bounding_box);

		if (measure_time_intervalls) {
			query_to_measure.time_intervall = query.time_intervall;
		}
		if (measure_time_slots) {
			query_to_measure.time_slots = query.time_slots;
		}
		intersection_path_ids = pathfinder.run(query_to_measure);

		edges_considered_sum += pathfinder.getNofEdgesConsidered();
		intersection_path_count += intersection_path_ids.size();
		time_tracker.stop();
	}

	time_results.insert(
	    std::pair<BBSize, MTime>(bbSize, time_tracker.calculateMean()));

	Print("Time tracking results for the queries:");
	time_tracker.printSummary();

	std::map<Substep, TimeTracker> substepTimeTrackers =
	    pathfinder.getSubstepTimeTrackers();
	for (std::map<Substep, TimeTracker>::iterator iter =
	         substepTimeTrackers.begin();
	     iter != substepTimeTrackers.end(); ++iter) {
		Substep const& substep = iter->first;
		substep_time_results.at(substep).at(distance).insert(
		    std::pair<BBSize, MTime>(bbSize, iter->second.calculateMean()));
	}
	Print("");
	Print("Results per step in the pathfinder algorithm:");
	pathfinder.printTimes();

	double averageNofIntersectionPaths =
	    (double)intersection_path_count / queries.size();
	Print("");
	Print("Average number of intersection paths: "
	      << averageNofIntersectionPaths);
	Print("Average number of edges considered: "
	      << (double)edges_considered_sum / queries.size());
	nofPath_results.insert(
	    std::pair<BBSize, double>(bbSize, averageNofIntersectionPaths));

	if (pathfinder.getStxxlTracker() != nullptr) {
		on_disk_measurements.insert(std::make_pair(
		    bbSize, pathfinder.getStxxlTracker()->calculateMean()));
		pathfinder.getStxxlTracker()->clear();
	}
}

void testQueries(IPathfinder& pathfinder, Distance distance,
    std::map<BBSize, Queries> const& bbSizeToQueries,
    std::map<BBSize, MTime>& time_results,
    std::map<Substep, std::map<Distance, std::map<BBSize, MTime>>>&
        substep_time_results,
    std::map<BBSize, double>& nofPath_results,
    std::map<BBSize, Measurement>& on_disk_measurement,
    bool measure_time_intervalls, bool measure_time_slots)
{
	// iterate in reverse order because the map is ordered ascending w.r.t. to
	// keys
	// but we want to have it descending in the results tables
	for (std::map<BBSize, Queries>::const_reverse_iterator iter =
	         bbSizeToQueries.rbegin();
	     iter != bbSizeToQueries.rend(); ++iter) {

		BBSize factor = iter->first;
		Queries const& queries = iter->second;

		Print("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
		Print("Starting Experiment with BB size factor: " << factor);

		testQueries(pathfinder, queries, distance, factor, time_results,
		    substep_time_results, nofPath_results, on_disk_measurement,
		    measure_time_intervalls, measure_time_slots);

		Print("End of Experiment with factor " << factor);
	}
}

void printGeneratedPathsTable(std::vector<Distance> const& distances,
    std::map<Distance, std::map<BBSize, MTime>> const& results,
    std::string result_type_name, uint width, uint digits)
{
	std::string format = std::to_string(width) + "." + std::to_string(digits);

	Print("");
	Print("Overview table for " + result_type_name +
	      ": The Columns are the BB sizes and the rows are the distances");

	std::cout << "       |||";
	for (int bbSizeExponent = minBbSizeExponent;
	     bbSizeExponent <= maxBbSizeExponent; bbSizeExponent++) {
		printf(
		    std::string("%" + format + "f|").c_str(), pow(0.5, bbSizeExponent));
	}
	std::cout << "\n";
	std::cout << "-------|||";
	for (int bbSizeExponent = minBbSizeExponent;
	     bbSizeExponent <= maxBbSizeExponent; bbSizeExponent++) {
		for (uint w = 0; w < width; w++) {
			std::cout << "-";
		}
		std::cout << "|";
	}
	std::cout << "\n";

	for (Distance distance : distances) {
		printf("%7.1i|||", distance);
		std::map<BBSize, MTime> const& resultsForDistance =
		    results.at(distance);
		for (auto iter = resultsForDistance.rbegin();
		     iter != resultsForDistance.rend(); ++iter) {
			printf(std::string("%" + format + "f|").c_str(),
			    results.at(distance).at(iter->first));
		}
		std::cout << "\n";
	}
}

PathsDS getGeneratedPaths(bool import_paths, std::string graph_filename,
    std::size_t number_of_paths, int distance, Graph const& graph,
    Random& random, uint nr_of_threads)
{
	if (import_paths) {
		PathsDS paths_ds;
		PathImporter pi(graph, paths_ds);
		pi.importPathsForPerformanceTest(
		    graph_filename, number_of_paths, distance);
		return paths_ds;
	} else {
		PathGenerator path_generator(graph, random, nr_of_threads);
		return path_generator.getRandomShortestArtificiallyStitchedPaths(
		    number_of_paths, distance);
	}
}

Queries getGeneratedQueries(Graph const& graph, std::size_t number_of_queries,
    double factorForBBSize, Random& random)
{
	BoundingBox graphBB = getBoundingBoxForGraph(graph);

	double graph_width =
	    graphBB.max_coordinate.lon - graphBB.min_coordinate.lon;
	double graph_height =
	    graphBB.max_coordinate.lat - graphBB.min_coordinate.lat;

	double bbWidth = graph_width * factorForBBSize;
	double bbHeight = graph_height * factorForBBSize;

	// Create queries before measuring the time
	Queries queries;
	for (std::size_t query_nr = 0; query_nr < number_of_queries; ++query_nr) {
		BoundingBox bounding_box = random.getBox(graph, bbWidth, bbHeight);
		TimeIntervall time_intervall = random.getIntervall();
		TimeSlots random_slots = random.getTimeSlots();
		queries.push_back(Query(bounding_box, time_intervall, random_slots));
	}
	return queries;
}

Queries getQueries(Graph const& graph, std::size_t number_of_queries,
    double factorForBBSize, Random& random, std::string graph_filename,
    bool import_queries)
{
	if (import_queries) {
		Queries queries;
		QueryImporter qi(graph, queries);
		qi.importQueriesForPerformanceTest(
		    graph_filename, number_of_queries, factorForBBSize);
		return queries;
	} else {
		return getGeneratedQueries(
		    graph, number_of_queries, factorForBBSize, random);
	}
}

void initializeSubstepMap(
    std::map<Substep, std::map<Distance, std::map<BBSize, MTime>>>&
        substep_time_results,
    IPathfinder& pathfinder, Distance distance)
{
	std::map<Substep, TimeTracker> substepTimeTrackers =
	    pathfinder.getSubstepTimeTrackers();
	for (std::map<Substep, TimeTracker>::iterator iter =
	         substepTimeTrackers.begin();
	     iter != substepTimeTrackers.end(); ++iter) {
		Substep const& substep = iter->first;
		if (substep_time_results.count(substep) == 0) {
			substep_time_results.insert(
			    std::pair<Substep, std::map<Distance, std::map<BBSize, MTime>>>(
			        substep, std::map<Distance, std::map<BBSize, MTime>>()));
		}
		substep_time_results.at(substep).insert(
		    std::pair<Distance, std::map<BBSize, MTime>>(
		        distance, std::map<BBSize, MTime>()));
	}
}

int testByGeneratedPaths(Graph& graph, PathfinderType pathfinder_type,
    Random& random, std::map<BBSize, Queries> const& bbSizeToQueries,
    std::string graph_filename, std::size_t number_of_paths, uint nr_of_threads,
    bool export_paths, bool importPaths, bool measure_time_intervalls,
    bool measure_time_slots)
{
	stxxl::block_manager* bm = stxxl::block_manager::get_instance();

	std::vector<Distance> distances;
	distances.push_back(25);
	distances.push_back(100);
	distances.push_back(400);
	distances.push_back(100000);

	std::map<Distance, std::map<BBSize, MTime>> time_results;
	std::map<Substep, std::map<Distance, std::map<BBSize, MTime>>>
	    substep_time_results;
	std::map<Distance, std::map<BBSize, double>> nofPath_results;
	std::map<Distance, std::map<BBSize, Measurement>> on_disk_measurements;

	for (uint i = 0; i < distances.size(); i++) {
		int distance = distances.at(i);

		Print("************************************************************"
		      "****"
		      "***********************");
		Print("Starting Experiments with distance: " << distance);

		auto paths = getGeneratedPaths(importPaths, graph_filename,
		    number_of_paths, distance, graph, random, nr_of_threads);

		if (export_paths) {
			PathExporter pe;
			pe.exportPathsForPerformanceTest(paths, graph_filename, distance);
		}

		PathfinderDS pathfinder_ds(graph, nr_of_threads, std::move(paths));
		auto pathfinder =
		    createPathfinder(pathfinder_type, pathfinder_ds, nr_of_threads);

		// initialize time tracker maps
		time_results.insert(std::pair<Distance, std::map<BBSize, MTime>>(
		    distance, std::map<BBSize, MTime>()));
		nofPath_results.insert(std::pair<Distance, std::map<BBSize, double>>(
		    distance, std::map<BBSize, double>()));
		on_disk_measurements.insert(
		    std::pair<Distance, std::map<BBSize, Measurement>>(
		        distance, std::map<BBSize, Measurement>()));
		initializeSubstepMap(substep_time_results, *pathfinder, distance);

		testQueries(*pathfinder, distance, bbSizeToQueries,
		    time_results.at(distance), substep_time_results,
		    nofPath_results.at(distance), on_disk_measurements.at(distance),
		    measure_time_intervalls, measure_time_slots);
	}

	std::cout << "Maximum stxxl allocation on disk in bytes: "
	          << bm->get_maximum_allocation() << std::endl;

	for (std::map<Substep,
	         std::map<Distance, std::map<BBSize, MTime>>>::iterator iter =
	         substep_time_results.begin();
	     iter != substep_time_results.end(); ++iter) {
		printGeneratedPathsTable(distances, iter->second,
		    getTextForSubstep(iter->first) + " time", WIDTH, N_OF_DIGITS);
	}

	printGeneratedPathsTable(
	    distances, time_results, "time", WIDTH, N_OF_DIGITS);
	printGeneratedPathsTable(
	    distances, nofPath_results, "nofPaths", 13, N_OF_DIGITS);
	for (StxxlStatValue stxxlStatValue : allStxxlStatValues) {
		std::map<Distance, std::map<BBSize, double>> values_for_stxxl_stat;
		for (auto iter = on_disk_measurements.begin();
		     iter != on_disk_measurements.end(); ++iter) {
			std::map<BBSize, double> innerMapToRet;

			std::map<BBSize, Measurement> const& subMap = iter->second;
			for (std::map<BBSize, Measurement>::const_iterator inner_iter =
			         subMap.begin();
			     inner_iter != subMap.end(); ++inner_iter) {
				innerMapToRet.insert(std::make_pair(
				    inner_iter->first, inner_iter->second.at(stxxlStatValue)));
			}
			values_for_stxxl_stat.insert(
			    std::make_pair(iter->first, innerMapToRet));
		}
		printGeneratedPathsTable(distances, values_for_stxxl_stat,
		    getTextForStxxlStatValue(stxxlStatValue), 13, N_OF_DIGITS);
	}

	return EXIT_SUCCESS;
}

void printReadPathsTable(std::map<BBSize, double> const& results,
    std::string result_type_name, uint width, uint digits)
{
	std::string format = std::to_string(width) + "." + std::to_string(digits);

	Print("");
	Print("Overview table " + result_type_name +
	      ": The Columns are the BB sizes");

	std::cout << "       |||";
	for (auto iter = results.rbegin(); iter != results.rend(); ++iter) {
		printf(std::string("%" + format + "f|").c_str(), iter->first);
	}
	std::cout << "\n";
	std::cout << "-------|||";
	for (auto iter = results.rbegin(); iter != results.rend(); ++iter) {
		for (uint w = 0; w < width; w++) {
			std::cout << "-";
		}
		std::cout << "|";
	}
	std::cout << "\n";

	std::cout << "-------|||";
	for (auto iter = results.rbegin(); iter != results.rend(); ++iter) {
		printf(std::string("%" + format + "f|").c_str(), iter->second);
	}
	std::cout << "\n";
}

int testByReadingPaths(Graph& graph, PathfinderType pathfinder_type,
    std::map<BBSize, Queries> const& bbSizeToQueries, std::string path_filename,
    uint nr_of_threads, bool measure_time_intervalls, bool measure_time_slots)
{
	PathsDS paths_ds;

	PathParser path_parser(graph, paths_ds);
	Print("Parse paths ...");
	path_parser.parse(path_filename);

	PathsDS compressed_paths_ds =
	    partitioner_util::partitionPaths(graph, paths_ds);

	PathfinderDS pathfinder_ds(
	    graph, nr_of_threads, std::move(compressed_paths_ds));
	auto pathfinder =
	    createPathfinder(pathfinder_type, pathfinder_ds, nr_of_threads);

	std::map<BBSize, MTime> time_results;
	std::map<Substep, std::map<Distance, std::map<BBSize, MTime>>>
	    substep_time_results;
	std::map<BBSize, double> nofPath_results;
	std::map<BBSize, Measurement> measurements_on_disk;

	// TODO: make some cool table datastructure instead of nested maps so we
	// do not need this
	int dummy_dist = -1;

	initializeSubstepMap(substep_time_results, *pathfinder, dummy_dist);

	testQueries(*pathfinder, dummy_dist, bbSizeToQueries, time_results,
	    substep_time_results, nofPath_results, measurements_on_disk,
	    measure_time_intervalls, measure_time_slots);

	for (std::map<Substep,
	         std::map<Distance, std::map<BBSize, MTime>>>::iterator iter =
	         substep_time_results.begin();
	     iter != substep_time_results.end(); ++iter) {
		printReadPathsTable(iter->second.at(dummy_dist),
		    getTextForSubstep(iter->first) + " time", WIDTH, N_OF_DIGITS);
	}

	printReadPathsTable(time_results, "time", WIDTH, N_OF_DIGITS);
	printReadPathsTable(nofPath_results, "nofPaths", 13, N_OF_DIGITS);

	for (StxxlStatValue stxxlStatValue : allStxxlStatValues) {
		std::map<BBSize, double> values_for_stxxl_stat;

		for (std::map<BBSize, Measurement>::const_iterator iter =
		         measurements_on_disk.begin();
		     iter != measurements_on_disk.end(); ++iter) {
			values_for_stxxl_stat.insert(
			    std::make_pair(iter->first, iter->second.at(stxxlStatValue)));
		}

		printReadPathsTable(values_for_stxxl_stat,
		    getTextForStxxlStatValue(stxxlStatValue), 13, N_OF_DIGITS);
	}

	return EXIT_SUCCESS;
}

int main(int argc, char* argv[])
{
	initStxxl();

	std::string graph_filename;
	std::size_t number_of_queries = 0;
	std::size_t number_of_generated_paths = 0;
	std::size_t number_of_archived_paths = 0;
	std::string path_filename;
	PathDataSource path_data_source(PathDataSource::None);
	QueryDataSource query_data_source(QueryDataSource::None);
	PathfinderType pathfinder_type = PathfinderType::STD;
	Unused(pathfinder_type);
	bool measure_time_intervalls(false);
	bool measure_time_slots(false);
	uint nr_of_threads = 1;
	bool export_paths(false);
	bool export_queries(false);

	/*
	 * Getopt argument parsing.
	 */

	const struct option longopts[] = {
	    {"help", no_argument, 0, 'h'},
	    {"graph_filename", required_argument, 0, 'i'},
	    {"path_filename", required_argument, 0, 'p'},
	    {"number_of_generated_paths", required_argument, 0, 'g'},
	    {"number_of_archived_paths", required_argument, 0, 'a'},
	    {"number_of_generated_queries", required_argument, 0, 'q'},
	    {"number_of_archived_queries", required_argument, 0, 'w'},
	    {"pathfinder_type", no_argument, 0, 'u'},
	    {"measure_time_intervalls", no_argument, 0, 'm'},
	    {"measure_time_slots", no_argument, 0, 's'},
	    {0, 0, 0, 0},
	    {"threads", required_argument, 0, 't'},
	    {"export", no_argument, 0, 'e'},
	};

	int index(0);
	int iarg(0);
	opterr = 1;

	while ((iarg = getopt_long(
	            argc, argv, "hi:p:g:a:q:w:u:mst:e", longopts, &index)) != -1) {
		switch (iarg) {
		case 'h':
			printUsage();
			return 0;
			break;
		case 'i':
			graph_filename = optarg;
			break;
		case 'p':
			path_data_source = PathDataSource::ReadInMatched;
			path_filename = optarg;
			break;
		case 'g': {
			if (path_data_source != PathDataSource::None) {
				std::cerr
				    << "The path data source has to be unique! Exiting.\n";
				std::cerr
				    << "Use ./performance_test --help to print the usage.\n";
				return 1;
			}
			path_data_source = PathDataSource::Generate;

			size_t idx = 0; // index of first "non digit"
			number_of_generated_paths = std::stoi(optarg, &idx);
			if ('\0' != optarg[idx] || number_of_generated_paths <= 0) {
				std::cerr << "Invalid path count: '" << optarg << "'\n";
				return 1;
			}
		} break;
		case 'a': {
			if (path_data_source != PathDataSource::None) {
				std::cerr
				    << "The path data source has to be unique! Exiting.\n";
				std::cerr
				    << "Use ./performance_test --help to print the usage.\n";
				return 1;
			}
			path_data_source = PathDataSource::Import;

			size_t idx = 0; // index of first "non digit"
			number_of_archived_paths = std::stoi(optarg, &idx);
			if ('\0' != optarg[idx] || number_of_archived_paths <= 0) {
				std::cerr << "Invalid path count: '" << optarg << "'\n";
				return 1;
			}
		} break;
		case 'q': {
			query_data_source = QueryDataSource::Generate;
			size_t idx = 0; // index of first "non digit"
			number_of_queries = std::stoi(optarg, &idx);
			if ('\0' != optarg[idx] || number_of_queries <= 0) {
				std::cerr << "Invalid query count: '" << optarg << "'\n";
				return 1;
			}
		} break;
		case 'w': {
			if (query_data_source != QueryDataSource::None) {
				std::cerr
				    << "The query data source has to be unique! Exiting.\n";
				std::cerr
				    << "Use ./performance_test --help to print the usage.\n";
				return 1;
			}
			query_data_source = QueryDataSource::Import;
			size_t idx = 0; // index of first "non digit"
			number_of_queries = std::stoi(optarg, &idx);
			if ('\0' != optarg[idx] || number_of_queries <= 0) {
				std::cerr << "Invalid query count: '" << optarg << "'\n";
				return 1;
			}
		} break;
		case 'u':
			pathfinder_type = toPathfinderType(optarg);
			break;
		case 'm':
			measure_time_intervalls = true;
			break;
		case 's':
			measure_time_slots = true;
			break;
		case 't': {
			size_t idx = 0; // index of first "non digit"
			nr_of_threads = std::stoi(optarg, &idx);
			if ('\0' != optarg[idx] || nr_of_threads <= 0) {
				std::cerr << "Invalid thread count: '" << optarg << "'\n";
				return 1;
			}
		} break;
		case 'e': {
			export_paths = true;
			export_queries = true;
		} break;
		default:
			printUsage();
			return 1;
			break;
		}
	}

	if (graph_filename == "") {
		std::cerr << "No input file specified! Exiting.\n";
		std::cerr << "Use ./performance_test --help to print the usage.\n";
		return 1;
	}

	if (number_of_queries == 0) {
		std::cerr << "No query count specified! Exiting.\n";
		std::cerr << "Use ./performance_test --help to print the usage.\n";
		return 1;
	}

	Print("Read and init graph...");
	Graph graph;
	graph.init(graph_filename);

	Print("Conducting performance test...");
	Random random;

	bool import_queries = false;

	switch (query_data_source) {
	case QueryDataSource::Generate:
		import_queries = false;
		break;
	case QueryDataSource::Import:
		import_queries = true;
		break;
	case QueryDataSource::None:
		std::cerr << "No query data source specified! Exiting.\n";
		std::cerr << "Use ./performance_test --help to print the usage.\n";
		return 1;
	default:
		assert(false);
	}

	std::map<BBSize, Queries> bbSizeToQueries;
	for (int bbSizeExponent = minBbSizeExponent;
	     bbSizeExponent <= maxBbSizeExponent; bbSizeExponent++) {
		BBSize factor = pow(0.5, bbSizeExponent);
		Queries queries = getQueries(graph, number_of_queries, factor, random,
		    graph_filename, import_queries);
		bbSizeToQueries.insert(std::pair<BBSize, Queries>(factor, queries));
	}

	if (export_queries) {
		QueryExporter qe;

		for (std::map<double, Queries>::const_iterator iter =
		         bbSizeToQueries.begin();
		     iter != bbSizeToQueries.end(); ++iter) {
			qe.exportQueriesForPerformanceTest(
			    iter->second, graph_filename, iter->first);
		}
	}

	switch (path_data_source) {
	case PathDataSource::Generate:
		return testByGeneratedPaths(graph, pathfinder_type, random,
		    bbSizeToQueries, graph_filename, number_of_generated_paths,
		    nr_of_threads, export_paths, false, measure_time_intervalls,
		    measure_time_slots);
	case PathDataSource::Import:
		return testByGeneratedPaths(graph, pathfinder_type, random,
		    bbSizeToQueries, graph_filename, number_of_archived_paths,
		    nr_of_threads, export_paths, true, measure_time_intervalls,
		    measure_time_slots);
	case PathDataSource::ReadInMatched:
		return testByReadingPaths(graph, pathfinder_type, bbSizeToQueries,
		    path_filename, nr_of_threads, measure_time_intervalls,
		    measure_time_slots);
	case PathDataSource::None:
		std::cerr << "No path data source specified! Exiting.\n";
		std::cerr << "Use ./performance_test --help to print the usage.\n";
		return 1;
	default:
		assert(false);
	}
}
